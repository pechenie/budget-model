package budget.model;

/**
 * Base entity representing balance report
 *
 * @since 15.04.2017
 */
public interface BalanceReport {
    /**
     * Report time
     *
     * @return time
     */
    long time();

    /**
     * Balance amount
     * @return balance
     */
    double amount();
}
