package budget.model.book;

import budget.model.BalanceReport;
import org.jetbrains.annotations.NotNull;

/**
 * Balance reports book implementation
 *
 * @since 18.04.2017
 */
public interface BalanceRecordBook extends RecordBook {
    /**
     * Writes a balance report to a book
     *
     * @param balanceReport balance report to write
     * @return true - if written successfully
     */
    boolean report(@NotNull BalanceReport balanceReport);
}
