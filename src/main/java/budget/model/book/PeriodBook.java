package budget.model.book;

import budget.model.ReferencePeriod;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Reference periods book. Manages the time interval entities
 *
 * @author Kirill Berezin
 */
public interface PeriodBook {
    /**
     * Rewrites the timeline with provided periods. New periods <b>may</b> overlap existing ones.
     * The resolution strategy is upon implementation
     *
     * @param periods periods to write into a book
     * @return true - if rewritten successfully
     */
    boolean rewrite(List<? extends ReferencePeriod> periods);

    /**
     * Appends a period to the end of a book.
     *
     * @param period period to append
     * @return true - if appended successfully
     */
    boolean append(@NotNull ReferencePeriod period);
}
