package budget.model.book;

import budget.model.Operation;
import org.jetbrains.annotations.NotNull;

/**
 * Operations book implementation
 *
 * @since 18.04.2017
 */
public interface OperationRecordBook extends RecordBook {
    /**
     * Executes an operation and writes it to a book
     *
     * @param operation operation to write
     * @return true - if executed and written successfully
     */
    boolean execute(@NotNull Operation operation);
}
