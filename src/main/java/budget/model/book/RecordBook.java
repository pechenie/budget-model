package budget.model.book;

/**
 * Record book is a base interface for book dealing with timed records
 *
 * @since 29.04.2017
 */
public interface RecordBook {
    /**
     * Checks whether there is a record at the specified time
     *
     * @param time time to check
     * @return true - if there is a record
     */
    boolean hasRecord(long time);

    /**
     * Erases the record by the specified time. Does nothing if there is no record at the time
     * @param time time of a record to erase
     */
    void eraseRecord(long time);
}
