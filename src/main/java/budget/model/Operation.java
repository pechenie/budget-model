package budget.model;

/**
 * Base entity representing an operation record.
 *
 * @since 15.04.2017
 */
public interface Operation {
    /**
     * Operation record time
     *
     * @return time
     */
    long time();

    /**
     * Operation cost
     * @return cost
     */
    double cost();
}
