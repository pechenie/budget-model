package budget.model.time;

/**
 * Iterator over points on a virtual timeline.
 *
 * @author Kirill Berezin
 */

public interface TimestampIterator {
    /**
     * Returns the next point on a timeline according to iterator's rules
     *
     * @param current time to start from
     * @return next point on a timeline
     */
    long next(long current);

    /**
     * Returns the first point on a timeline <b>prior</b> to provided timestamp, which is aligned to iterator's rules
     * @param current timestamp to calculated aligned point in the "past"
     * @return the value of aligned point on a timeline, which is guaranteed to be less than or equal to a provided time
     */
    long alignedStart(long current);
}
