package budget.model;

import org.jetbrains.annotations.NotNull;

/**
 * Basic entity for representing a reference period. Responsible for checking inclusiveness of as well as relation
 * to point on a virtual timeline.
 *
 * @since 15.04.2017
 */
public interface ReferencePeriod {
    /**
     * Represents a position of a <b>reference period</b> in relation of a point on a virtual timeline
     */
    enum Relation {
        PERIOD_IS_BEFORE, PERIOD_INCLUDES, PERIOD_IS_AFTER
    }

    /**
     * Period start. May or may not be considered included into a period.
     */
    long start();

    /**
     * Period end. May or may not be considered included into a period.
     */
    long end();

    /**
     * Returns a relation to a specified point on a virtual timeline. <b>Must</b> be consistent with {@link #includes(long)}.
     *
     * @param time time on a virtual timeline
     * @return a relation enum value
     */
    @NotNull
    default Relation relatesTo(long time) {
        if (time < start()) {
            return Relation.PERIOD_IS_AFTER;
        } else if (end() <= time) {
            return Relation.PERIOD_IS_BEFORE;
        } else {
            return Relation.PERIOD_INCLUDES;
        }
    }

    /**
     * Checks whether a point on a virtual timeline is included in the period or not. By default, period's end point is not included.
     * Must</b> be consistent with {@link #relatesTo(long)}
     *
     * @param time time to check
     * @return included or not
     */
    default boolean includes(long time) {
        return start() <= time && time < end();
    }

    /**
     * Available margin value defined for a period
     * @return avilable margin value
     */
    double availableMargin();
}
