package budget.model.tag;

import org.jetbrains.annotations.NotNull;

import java.util.Collection;

/**
 * Simple key-value storage
 *
 * @author Kirill Berezin
 */

public interface Tag {
    /**
     * Key of a tag. Usually associated with a point on a virtual timeline
     *
     * @return key of a tag
     */
    long key();

    /**
     * Checks whether the entry with specified key exists
     * @param key key to check
     * @return true - if exists
     */
    boolean hasEntry(@NotNull String key);

    /**
     * Returns an updated new tag with additionally provided key-value pair. There are no any guarantees that it will be new or an existing instance.
     * @param key key to add
     * @param value value to associate with the key
     * @return an updated tag
     */
    @NotNull
    Tag withEntry(@NotNull String key, @NotNull String value);

    /**
     * Returns an updated tag by removing an entry with the specified key. There are no any guarantees that it will be new or an existing instance.
     * @param key key to remove
     * @return an updated tag
     */
    @NotNull
    Tag removeEntry(@NotNull String key);

    /**
     * Returns a value by a specified key or empty string if there is no such key
     * @param key key to get value for
     * @return value or empty string
     */
    @NotNull
    String valueFor(@NotNull String key);

    /**
     * Convenience method for fetching value as a long
     * @param key key to get long value for
     * @return parsed long value
     * @throws NumberFormatException in case the value can't be parsed as long
     */
    default long longValueFor(String key) {
        return Long.parseLong(valueFor(key));
    }

    /**
     * Returns all keys with non-empty values
     *
     * @return keys with non-empty values
     */
    Collection<String> allKeys();
}
