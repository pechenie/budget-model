package budget.model.tag;

import org.jetbrains.annotations.NotNull;

import java.util.Collection;

/**
 * Low-level tagging model.
 *
 * @author Kirill Berezin
 */

public interface Tags {
    void remove(long time);

    void update(@NotNull Tag tag);

    @NotNull
    Tag tagBy(long time);

    @NotNull
    Collection<? extends Tag> tags();
}
