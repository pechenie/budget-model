package budget.model;

/**
 * Simple general listener for updates of any kind
 *
 * @author Kirill Berezin
 */

public interface UpdateListener {
    /**
     * Called on underlying entities update
     */
    void notifyUpdated();
}
