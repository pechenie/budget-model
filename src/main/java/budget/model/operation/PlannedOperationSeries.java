package budget.model.operation;

import budget.model.Operation;
import org.jetbrains.annotations.NotNull;

import java.util.function.Consumer;

/**
 * Planned operations series
 *
 * @author Kirill Berezin
 */

public interface PlannedOperationSeries {
    /**
     * Name of a series
     *
     * @return name
     */
    @NotNull
    String name();

    /**
     * Traverses the operations within a series one by one in the order of a timeline and passes them to provided consumer
     * @param nextOperationConsumer operations consumer
     */
    void processSeries(@NotNull Consumer<Operation> nextOperationConsumer);

    /**
     * Operations count in the series
     * @return operations count
     */
    int size();
}
