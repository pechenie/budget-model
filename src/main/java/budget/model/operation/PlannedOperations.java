package budget.model.operation;

import org.jetbrains.annotations.NotNull;

import java.util.Collection;

/**
 * Low-level model for operation planning. Operates with an operation series
 *
 * @author Kirill Berezin
 */

public interface PlannedOperations {
    /**
     * Returns a list of all planned operation series
     *
     * @return list of operation series
     */
    @NotNull
    Collection<? extends PlannedOperationSeries> plannedSeries();

    /**
     * Plans new series
     * @param series series to plan
     * @return true - if planned successfully
     */
    boolean plan(@NotNull PlannedOperationSeries series);

    /**
     * Removes the series by its name
     * @param name name of a series
     */
    void delete(@NotNull String name);
}
