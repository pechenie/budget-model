package budget.model.source;

import budget.model.Operation;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

/**
 * Source of operations
 *
 * @since 15.04.2017
 */
public interface Operations {
    /**
     * Retrieves an operation record at specified time
     *
     * @param time time of the record
     * @return operation at time or null
     */
    @Nullable
    Operation operationAt(long time);

    /**
     * Convenience method for calculation operations sum within an interval
     * @param timeFrom interval start
     * @param timeTo interval end
     * @param inclusive is end inclusive or not
     * @return sum of operations, or 0d in case there is no operations in the interval
     */
    double operationSum(long timeFrom, long timeTo, boolean inclusive);

    /**
     * Returns a collection of operations within a specified interval (with inclusiveness flags on edges). Guaranteed to be sorted.
     * @param from time from
     * @param inclusiveFrom inclusive or not
     * @param to time to
     * @param inclusiveTo inclusive or not
     * @return a collection of operations (may be empty)
     */
    @NotNull
    Collection<Operation> range(long from, boolean inclusiveFrom, long to, boolean inclusiveTo);
}

