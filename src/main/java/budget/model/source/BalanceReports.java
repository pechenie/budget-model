package budget.model.source;

import budget.model.BalanceReport;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;

/**
 * Source of balance reports
 *
 * @since 15.04.2017
 */
public interface BalanceReports {
    /**
     * Returns a last known balance report prior to a specified time (inclusive or not)
     *
     * @param time      time to lookup a balance report
     * @param inclusive inclusive lookup or not
     * @return first report prior to specified time <b>or</b> report of 0 balance at {@link Long#MIN_VALUE} time on a virtual timeline
     */
    @NotNull
    BalanceReport lastReportAtTime(long time, boolean inclusive);

    /**
     * Returns a collection of balance reports within a specified interval (with inclusiveness flags on edges). Guaranteed to be sorted.
     *
     * @param from          time from
     * @param inclusiveFrom inclusive or not
     * @param to            time to
     * @param inclusiveTo   inclusive or not
     * @return a collection of balance report (may be empty)
     */
    @NotNull
    Collection<BalanceReport> range(long from, boolean inclusiveFrom, long to, boolean inclusiveTo);
}

