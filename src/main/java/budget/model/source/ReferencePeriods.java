package budget.model.source;

import budget.model.ReferencePeriod;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

/**
 * Source of reference periods.
 * General contract is that the source contain a sequence of non-overlapping reference periods without any gaps in between them.
 *
 * @since 15.04.2017
 */
public interface ReferencePeriods {
    /**
     * Returns a reference period containing specified time
     *
     * @param time point on a virtual timeline
     * @return a reference period or null, if no period contains specified time
     */
    @Nullable
    ReferencePeriod periodAtTime(long time);

    /**
     * A sorted collection of all available periods
     * @return sorted collection
     */
    @NotNull
    Collection<ReferencePeriod> periods();

    /**
     * Returns a range of periods intersecting a specified interval.
     * Last parameters defines if periods containing <code>to</code> included or not
     * <p>
     * Returning set of periods <b>must</b> have a relation of {@link ReferencePeriod.Relation#PERIOD_INCLUDES} or
     * {@link ReferencePeriod.Relation#PERIOD_IS_AFTER} for <code>from</code> time and must have a relation of
     * {@link ReferencePeriod.Relation#PERIOD_IS_BEFORE} (or, if <code>inclusiveTo</code> is <code>true</code>,
     * {@link ReferencePeriod.Relation#PERIOD_INCLUDES}) for <code>to</code> time
     * </p>
     *
     * @param from        interval start
     * @param to          interval end
     * @param inclusiveTo include <code>to</code> or not
     * @return a sorted collection of periods
     */
    @NotNull
    Collection<ReferencePeriod> range(long from, long to, boolean inclusiveTo);

    /**
     * Return a union of all periods in the source
     * @return returns a union. Available margin is always {@link Double#NaN}, in case there are no periods in the source - both
     * start and end must be equal to {@link Long#MIN_VALUE}
     */
    @NotNull
    ReferencePeriod unite();
}

