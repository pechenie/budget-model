package budget.model;

import budget.view.TimedBalance;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.function.Predicate;

/**
 * Main metrics calculation model. Accumulates a knowledge and a logic of provisional balances and real / provisional
 * usable margins calculations at any point in time. Capable of checking special conditions on balances as well as iterating over
 * sequence of record in a period of time.
 *
 * <p>Generally, this is a more low-level journal representation, but it could be served as underlying model for Dashboard as well</p>
 *
 * @since 28.04.2017
 */
public interface TimedMetrics {
    /**
     * Visitor for traversing entries of a low level journal timeline
     */
    interface EntryVisitor {
        /**
         * Called when period's start record occurred
         *
         * @param period  period
         * @param balance balance at period's start
         * @param margin  available margin at period's start
         */
        void onPeriodStart(ReferencePeriod period, double balance, double margin);

        /**
         * Called when balance report record occurred
         *
         * @param report report
         * @param margin available margin at the report's time
         */
        void onBalanceReport(BalanceReport report, double margin);

        /**
         * Called when operation record occurred
         * @param operation operation
         * @param estimatedBalance estimated balance after operation
         * @param margin available margin at the operation's time
         */
        void onOperation(Operation operation, double estimatedBalance, double margin);

        /**
         * Called when period's end record occurred
         * @param period period
         * @param balance balance at period's end
         * @param margin available margin at period's end
         */
        void onPeriodEnd(ReferencePeriod period, double balance, double margin);
    }

    /**
     * Traverse entries within a time interval (both sides inclusive)
     * @param visitor visitor to traverse with
     * @param from time from
     * @param to time to
     */
    void traverse(EntryVisitor visitor, long from, long to);

    /**
     * Returns an available margin value for a time in a period
     * @param time time
     * @param period period
     * @return available margin value
     */
    double marginForTimeInPeriod(long time, @Nullable ReferencePeriod period);

    /**
     * Returns minimal available margin for an interval from <code>time</code> to period's end
     * @param time time
     * @param period period
     * @return minimal available margin
     */
    double minUsableMarginInPeriod(long time, @Nullable ReferencePeriod period);

    /**
     * Estimates a balance value at the time provided
     * @param time time
     * @return estimated balance value
     */
    double estimatedBalanceAtTime(long time);

    /**
     * Provides a minimal balance value within an interval
     * @param from time from
     * @param to time to
     * @return minimal balance value
     */
    @NotNull
    TimedBalance minBalanceAtRange(long from, long to);

    /**
     * Returns a first record matching the criteria within an interval (or null, if none matches)
     * @param from time from
     * @param to time to
     * @param criteria criteria to check
     * @return balance and time, or null
     */
    @Nullable
    TimedBalance firstBalanceWithCriteria(long from, long to, Predicate<Double> criteria);
}
