package budget.model;

/**
 * Time-tracking entity for avoiding records collision.
 *
 * @since 29.04.2017
 */
public interface Schedule {
    /**
     * Checks where the specified time is busy (already occupied by some underlying record) or not
     *
     * @param time time to check
     * @return busy or not
     */
    boolean busy(long time);
}
