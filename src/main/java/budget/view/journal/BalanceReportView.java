package budget.view.journal;

import org.jetbrains.annotations.NotNull;

/**
 * Balance reports records view. Represented by additional balance.
 *
 * @since 18.04.2017
 */
public interface BalanceReportView extends GroupItem {
    @Override
    default <T> T visitBy(@NotNull GroupItemVisitor<T> visitor) {
        return visitor.onBalanceReport(this);
    }
}
