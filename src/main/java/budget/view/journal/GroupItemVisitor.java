package budget.view.journal;

import org.jetbrains.annotations.NotNull;

/**
 * A visitor for traversing group items within a period group view. Produces a result of type T
 *
 * @since 18.04.2017
 */
public interface GroupItemVisitor<T> {
    /**
     * Called if a visited item is an operation view
     *
     * @param item operation view item
     * @return a result
     */
    T onOperation(@NotNull OperationView item);

    /**
     * Called if a visited item is a balance report view
     * @param item balance report view item
     * @return a result
     */
    T onBalanceReport(@NotNull BalanceReportView item);
}
