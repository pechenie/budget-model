package budget.view.journal;

import org.jetbrains.annotations.NotNull;

/**
 * View of the journal. Represents a list of period group views as elements. Provides an ability to lookup a group by time
 *
 * @since 16.04.2017
 */
public interface JournalView {
    /**
     * Returns an index of a group for a given time. Return 0 if all groups are after that time and (size - 1) if all are before.
     *
     * @param time time to lookup
     * @return group index
     */
    int groupIndexAtTime(long time);

    /**
     * Size of a list
     * @return size of a list
     */
    int groupsCount();

    /**
     * Returns group by its index
     * @param index index of a group
     * @return period group view
     * @throws IndexOutOfBoundsException if index is out of bounds
     */
    @NotNull
    PeriodGroupView groupAt(int index);
}

