package budget.view.journal;

import budget.view.TimedBalanceWithMargin;
import org.jetbrains.annotations.NotNull;

/**
 * Base interface for record items in the journal.
 *
 * @since 18.04.2017
 */
public interface GroupItem {
    /**
     * Returns base metrics at this record time
     * @return time + balance + margin
     */
    TimedBalanceWithMargin state();

    /**
     * Group items are visitable. Return any value visitor would like to produce
     * @param visitor visitor to visit with
     * @param <T> return value type
     * @return visit result
     */
    <T> T visitBy(@NotNull GroupItemVisitor<T> visitor);
}
