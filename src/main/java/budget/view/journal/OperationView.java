package budget.view.journal;

import org.jetbrains.annotations.NotNull;

/**
 * Operation records view. Represented by additional cost.
 *
 * @since 18.04.2017
 */
public interface OperationView extends GroupItem {
    /**
     * Cost of an operation
     *
     * @return cost
     */
    double cost();

    @Override
    default <T> T visitBy(@NotNull GroupItemVisitor<T> visitor) {
        return visitor.onOperation(this);
    }
}
