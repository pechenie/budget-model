package budget.view.journal.properties;

import org.jetbrains.annotations.NotNull;

/**
 * Base interface for item properties
 *
 * @author Kirill Berezin
 */

public interface ItemProperties {
    /**
     * Item's name
     *
     * @return name
     */
    @NotNull
    String name();

    /**
     * Item properties are visitable. Returns any result visitor produces
     * @param visitor visitor
     * @param <T> any type visitor produces
     * @return result
     */
    <T> T visitBy(ItemPropertiesVisitor<T> visitor);
}
