package budget.view.journal.properties;

/**
 * A typed visitor for item properties
 *
 * @author Kirill Berezin
 */

public interface ItemPropertiesVisitor<T> {
    /**
     * Called if visited item properties is balance report
     *
     * @param properties balance report properties
     * @return result
     */
    T visit(BalanceReportProperties properties);

    /**
     * Called if visited item properties is executed operation properties
     * @param properties executed operation properties
     * @return result
     */
    T visit(ExecutedOperationProperties properties);

    /**
     * Called if visited item properties is planned operation properties
     * @param properties planned operation properties
     * @return result
     */
    T visit(PlannedOperationProperties properties);
}
