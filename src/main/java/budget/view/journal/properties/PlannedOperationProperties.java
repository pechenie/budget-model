package budget.view.journal.properties;

/**
 * Properties of a planned operation
 *
 * @author Kirill Berezin
 */

public interface PlannedOperationProperties extends ItemProperties {
    /**
     * Time of the first record in chain
     *
     * @return time of the first record in chain
     */
    long chainStartTime();

    @Override
    default <T> T visitBy(ItemPropertiesVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
