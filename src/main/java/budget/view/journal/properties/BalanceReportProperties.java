package budget.view.journal.properties;

/**
 * Properties of a balance report
 *
 * @author Kirill Berezin
 */

public interface BalanceReportProperties extends ItemProperties {
    @Override
    default <T> T visitBy(ItemPropertiesVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
