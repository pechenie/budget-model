package budget.view.journal.properties;

import org.jetbrains.annotations.NotNull;

/**
 * An extra journal view holding model-independent specific properties of journal records
 *
 * @author Kirill Berezin
 */

public interface JournalProperties {
    /**
     * Size of a list of group properties
     *
     * @return size
     */
    int groupsCount();

    /**
     * Returns an extra period group properties by index
     * @param index index of a group
     * @return group properties
     * @throws IndexOutOfBoundsException if index was out of bounds
     */
    @NotNull
    GroupProperties groupPropertiesAt(int index);
}
