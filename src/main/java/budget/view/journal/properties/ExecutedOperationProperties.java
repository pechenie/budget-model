package budget.view.journal.properties;

/**
 * Properties of an executed operation
 *
 * @author Kirill Berezin
 */

public interface ExecutedOperationProperties extends ItemProperties {
    @Override
    default <T> T visitBy(ItemPropertiesVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
