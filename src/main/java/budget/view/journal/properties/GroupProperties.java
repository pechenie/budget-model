package budget.view.journal.properties;

import org.jetbrains.annotations.NotNull;

/**
 * An extra group properties view
 *
 * @author Kirill Berezin
 */

public interface GroupProperties {
    /**
     * Location of a group in a virtual timeline - past, present or future
     */
    enum Presence {PAST, PRESENT, FUTURE}

    /**
     * Returns the group position on a virtual timeline
     *
     * @return group position on a virtual timeline
     */
    @NotNull
    Presence presence();

    /**
     * Return a group item properties by index
     * @param index index of group item
     * @return item properties
     */
    @NotNull
    ItemProperties itemPropertiesAt(int index);

    /**
     * Size of an item properties list
     * @return size
     */
    int itemsCount();
}
