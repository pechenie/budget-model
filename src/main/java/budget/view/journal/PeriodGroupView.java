package budget.view.journal;

import budget.model.ReferencePeriod;
import budget.view.TimedBalanceWithMargin;
import org.jetbrains.annotations.NotNull;

/**
 * A view representation of reference period. Consists of a list of record items plus a set metrics at period start and end
 *
 * @since 18.04.2017
 */
public interface PeriodGroupView {
    /**
     * Returns a records count of a reference period
     *
     * @return records count
     */
    int itemsCount();

    /**
     * Returns a record view by its index
     * @param index index of a record
     * @return group item
     * @throws IndexOutOfBoundsException if index was out of bounds
     */
    @NotNull
    GroupItem itemAt(int index);

    /**
     * Backing reference period
     * @return backing reference period
     */
    @NotNull
    ReferencePeriod period();

    /**
     * Returns the metrics set (balance and margin) at period start
     * @return metrics at period start
     */
    @NotNull
    TimedBalanceWithMargin startBalance();

    /**
     * Returns the metrics set (balance and margin) at period end
     * @return metrics at period end
     */
    @NotNull
    TimedBalanceWithMargin endBalance();
}
