package budget.view;

/**
 * Time and cost
 *
 * @author Kirill Berezin
 */
public interface TimedCost {
    /**
     * Time of an operation
     *
     * @return time
     */
    long time();

    /**
     * Cost of an operation at the time
     * @return cost
     */
    double cost();
}
