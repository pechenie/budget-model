package budget.view;

/**
 * TimedBalance with available margin value at that time
 *
 * @since 27.04.2017
 */
public interface TimedBalanceWithMargin extends TimedBalance {
    /**
     * Avialable margin at the time
     *
     * @return available margin
     */
    double margin();
}
