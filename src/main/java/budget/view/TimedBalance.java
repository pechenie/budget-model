package budget.view;

/**
 * Time and balance
 *
 * @since 27.04.2017
 */
public interface TimedBalance {
    /**
     * Time at which this balance record is valid
     *
     * @return time
     */
    long time();

    /**
     * Balance value at time
     * @return balance
     */
    double balance();
}
