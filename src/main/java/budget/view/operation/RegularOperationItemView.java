package budget.view.operation;

import budget.view.TimedCost;
import org.jetbrains.annotations.NotNull;

/**
 * A view for planned operation series
 *
 * @author Kirill Berezin
 */

public interface RegularOperationItemView {
    @NotNull
    String name();

    @NotNull
    TimedCost next();

    int operationsLeft();
}
