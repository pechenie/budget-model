package budget.view.operation;

import org.jetbrains.annotations.Nullable;

/**
 * A view of a planned operations series list.
 *
 * @author Kirill Berezin
 */

public interface RegularOperationsView {
    /**
     * Size of an operation series list
     *
     * @return series count
     */
    int itemsCount();

    /**
     * Returns a view of a series by index
     * @param i index of an item
     * @return operation series item (or null if index was out of bounds)
     */
    @Nullable
    RegularOperationItemView viewAt(int i);
}
