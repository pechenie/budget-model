package budget.app;

import budget.api.CustomTags;
import budget.api.Dashboard;
import budget.api.Journal;
import budget.api.PeriodRules;
import budget.api.RegularOperations;
import budget.api.entity.RecurrenceInput;
import budget.api.entity.RuleView;
import budget.model.UpdateListener;
import budget.view.journal.JournalView;
import budget.view.journal.properties.JournalProperties;
import org.jetbrains.annotations.NotNull;

/**
 * The general budget model. Provides all available APIs as well as manages theirs consistency and correctness.
 * All interactions with the API shall be done via this model.
 *
 * @author Kirill Berezin
 */

public interface BudgetModel<RegOpsRV extends RuleView, RegOpsInp extends RecurrenceInput, PeriodRV extends RuleView, PeriodRInp> {
    /**
     * Provides an actual Journal instance.
     *
     * @return actual Journal
     */
    @NotNull
    Journal journal();

    /**
     * Collects and returns extra journal properties by provided view and "current time" timestamp
     *
     * @param view the journal view to collect properties for
     * @param now  current time
     * @return journal properties
     */
    @NotNull
    JournalProperties journalProperties(@NotNull JournalView view, long now);

    /**
     * Provides an access to actual dashboard
     * @return actual dashboard
     */
    @NotNull
    Dashboard dashboard();

    /**
     * Provides an access to regular operations API
     * @return regular operations API
     */
    @NotNull
    RegularOperations<RegOpsRV, RegOpsInp> regularOperations();

    /**
     * Provides an access to reference periods rules API
     * @return reference periods rules API
     */
    @NotNull
    PeriodRules<PeriodRV, PeriodRInp> periodRules();

    /**
     * Provides an access to custom tags
     * @return custom tags API
     */
    CustomTags customTags();

    /**
     * Registers an underlying models update listener. General contract is that no previously obtained API objects are correct
     * and consistent after the update notification has been received.
     * @param updateListener listener to subscribe
     */
    void addListener(@NotNull UpdateListener updateListener);

    /**
     * Removes previously subscribed listener or does nothing if it wasn't subscribed
     * @param updateListener listener to un subscribe
     */
    void removeListener(@NotNull UpdateListener updateListener);
}
