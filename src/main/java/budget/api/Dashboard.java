package budget.api;

import budget.view.TimedBalance;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.function.Predicate;

/**
 * Dashboard API provides an access to live crucial budget parameters.
 * A quick glance over those should be enough for 99% percent use cases of the whole model.
 * @since 15.04.2017
 */
public interface Dashboard {
    /**
     * Returns the exact value of usable margin at the time provided
     *
     * @param time time to check usable margin at
     * @return usable margin value
     */
    double usableMarginAtTime(long time);

    /**
     * Returns the minimal usable margin since time provided within a reference period
     *
     * @param time time since when to check min usable margin
     * @return min usable margin since time till a reference period end
     */
    double minUsableMarginSinceTime(long time);

    /**
     * Provides a time and balance amount of the last report fot the time provided
     * @param time time to check balance report at
     * @return time and balance of the closest balance report in the past
     */
    @NotNull
    TimedBalance lastReportedBalanceForTime(long time);

    /**
     * Gives an estimated balance amount for a time provided
     * @param time time to estimate balance at
     * @return estimated balance (or exact in case time is in the past)
     */
    double estimatedBalanceAtTime(long time);

    /**
     * Returns the minimal estimated balance since last recorded balance report
     * @return minimal estimated balance and the time when it will take place
     */
    @NotNull
    TimedBalance minBalanceSinceLastReport();

    /**
     * Returns the first (since last real balance report) occurred balance record for given criteria with the time of occurrence
     * @param criteria balance checking predicate
     * @return time and estimated balance value of the first record matching criteria since last real balance report
     * (or null if none matched)
     */
    @Nullable
    TimedBalance checkBalanceSinceLastReport(Predicate<Double> criteria);

    /**
     * Returns the amount of time in milliseconds until the end of the period since the time provided
     * @param time time to calculate remaining time from
     * @return remaining time
     */
    long timeTillPeriodEnd(long time);
}

