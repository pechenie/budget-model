package budget.api.action;

/**
 * Prepared action for logging an operation
 * @since 29.04.2017
 */
public interface LogOperationAction {
    /**
     * Collision free logging of executed operation and consequent balance report
     *
     * @param time          desired time of operation
     * @param operationCost cost of operation
     * @param remainingCash balance amount after the operation
     * @return the actual time of the recorded operation (may differ from desired).
     * It is guaranteed that the balance report is right next after the operation record
     */
    long writeWithBalance(long time, double operationCost, double remainingCash);
}
