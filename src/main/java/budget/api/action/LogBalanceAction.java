package budget.api.action;

/**
 * Prepared action for logging a balance report
 *
 * @since 29.04.2017
 */
public interface LogBalanceAction {
    /**
     * Collision free balance report logging
     *
     * @param time desired time of a balance report
     * @param cash amount of balance to report
     * @return actual record time (may differ from desired)
     */
    long write(long time, double cash);
}
