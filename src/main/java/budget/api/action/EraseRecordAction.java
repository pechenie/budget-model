package budget.api.action;

/**
 * Prepared action to erase records from journal
 *
 * @since 29.04.2017
 */
public interface EraseRecordAction {
    /**
     * Erases the record by a specific time. If there is no record at that time - does nothing.
     *
     * @param time time of the record
     */
    void erase(long time);
}
