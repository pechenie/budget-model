package budget.api;

import org.jetbrains.annotations.NotNull;

/**
 * API for tagging entities with user-defined tags
 *
 * @author Kirill Berezin
 */

public interface CustomTags {
    /**
     * Tags a record at specified time with the custom name
     *
     * @param time time of the record
     * @param name new record's name
     */
    void tagWithCustomName(long time, @NotNull String name);
}
