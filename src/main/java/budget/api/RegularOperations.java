package budget.api;

import budget.api.entity.RuleTemplate;
import budget.api.entity.RuleView;
import budget.view.operation.RegularOperationsView;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;

/**
 * API for operations planning.
 *
 * <p>These operations are different from "normal" operations because of their position on a virtual timeline.
 * They are mostly treated as "non-happened" or "future" operations. Normally they appear in the future in human terms
 * or just used as a planning tool or a check if something is affordable in the future.</p>
 *
 * <p>These operations are linked into chains called series. User can plan some series with recurrence, defined by one of
 * the available rules, also providing the cost and the time of the first operation. User then able to delete the whole series
 * or some of the operations one by one via regular Journal API. The operations in a series are guaranteed to maintain
 * consistent links.</p>
 *
 * <p>API also provides an ability to view either all planned series or only those which are within some timestamp</p>
 *
 * @author Kirill Berezin
 */

public interface RegularOperations<View extends RuleView, Input> {
    /**
     * Displays all planned series
     *
     * @return the view of all series
     */
    @NotNull
    RegularOperationsView displayAll();

    /**
     * Displays the series which have occurrences within a time provided
     * @param time time to filter all series with
     * @return series view which have occurrences before the provided timestamp
     */
    @NotNull
    RegularOperationsView displayWithin(long time);

    /**
     * Collection of possible recurrence templates
     * @return recurrence templates
     */
    Collection<? extends RuleTemplate<View, Input, ?>> occurrenceTemplates();

    /**
     * Plans a new series with specific parameters. Attempt to plan a series with similar name will silently do nothing
     * @param name a name of the new series
     * @param cost cost of operations in series
     * @param startTime first operation in series
     * @param view a view of the recurrence template
     * @param input additional input for recurrence rule
     */
    void planSeries(@NotNull String name, double cost, long startTime, View view, Input input);

    /**
     * Deletes a series by its name (if any)
     * @param name series name
     */
    void deleteSeries(@NotNull String name);
}
