package budget.api.entity;

/**
 * Basic interface for recurrence input. Only required to provide an amount of operations to be in the series.
 *
 * @author Kirill Berezin
 */

public interface RecurrenceInput {
    /**
     * How much operations are required in the series
     *
     * @return operations count
     */
    int operationsCount();
}
