package budget.api.entity;

import org.jetbrains.annotations.NotNull;

/**
 * A rule template defined by its view, expected input and the output rule
 *
 * @author Kirill Berezin
 */
public interface RuleTemplate<View extends RuleView, Input, Rule> {
    /**
     * Displays itself providing a view
     *
     * @return view of the template
     */
    @NotNull
    View display();

    /**
     * Generates the rule by input
     * @param data input
     * @return rule
     */
    @NotNull
    Rule toRule(Input data);
}
