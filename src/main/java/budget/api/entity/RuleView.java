package budget.api.entity;

import org.jetbrains.annotations.NotNull;

/**
 * Basic interface of the rule's view
 *
 * @author Kirill Berezin
 */
public interface RuleView {
    /**
     * Unique identifier within a rules universe
     *
     * @return rule id
     */
    @NotNull
    String id();
}
