package budget.api;

import budget.api.action.EraseRecordAction;
import budget.api.action.LogBalanceAction;
import budget.api.action.LogOperationAction;
import budget.view.journal.JournalView;
import org.jetbrains.annotations.NotNull;

/**
 * Journal is the the main entry point ot underlying models. Journal contains a timeline of all the records within defined periods.
 * Journal can be displayed for reading and also written to.
 *
 * @since 18.04.2017
 */
public interface Journal {
    /**
     * Prepares and returns an action which allows to log new balance record into the journal
     *
     * @return a balance logging action
     */
    @NotNull
    LogBalanceAction logBalance();

    /**
     * Prepares and returns an action which allows to log new operation record into the journal
     * @return an operation logging action
     */
    @NotNull
    LogOperationAction logOperation();

    /**
     * Prepares and returns an action which allows to erase any time-based record (operation or balance report) from journal
     * @return a record erasing action
     */
    @NotNull
    EraseRecordAction eraseRecord();

    /**
     * Displays the journal for reading
     * @return journal view
     */
    @NotNull
    JournalView display();
}
