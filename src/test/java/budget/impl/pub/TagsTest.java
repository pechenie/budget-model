package budget.impl.pub;

import budget.api.CustomTags;
import budget.impl.api.CustomTagsImpl;
import budget.impl.api.NotifyingCustomTags;
import budget.impl.model.tag.TagsImpl;
import budget.model.UpdateListener;
import budget.model.tag.Tag;
import budget.model.tag.Tags;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Kirill Berezin
 */

class TagsTest {

    @Test
    void test() {
        Tags tags = new TagsImpl(new HashMap<>());

        assertEquals(0, tags.tags().size());
        Tag emptyTag = tags.tagBy(1234);
        assertNotNull(emptyTag);
        assertEquals(0, emptyTag.allKeys().size());

        {
            Tag tag = tags.tagBy(999);
            assertEquals(999, tag.key());
            assertFalse(tag.hasEntry("foo"));
            assertSame(tag, tag.removeEntry("bar"));
            assertEquals("", tag.valueFor("blah"));
            Tag newTag = tag.withEntry("foo", "bar");
            assertEquals(1, newTag.allKeys().size());
            assertNotSame(tag, newTag);
            assertEquals(999, newTag.key());
            assertTrue(newTag.hasEntry("foo"));
            assertEquals("bar", newTag.valueFor("foo"));
            newTag = newTag.withEntry("qux", "1000");
            assertEquals(1000, newTag.longValueFor("qux"));
            assertEquals(2, newTag.allKeys().size());

            tags.update(newTag);
            assertEquals(1, tags.tags().size());
        }

        {
            Tag tag = tags.tagBy(999);
            assertEquals(2, tag.allKeys().size());
            assertTrue(tag.hasEntry("foo"));
            assertEquals("bar", tag.valueFor("foo"));
            assertEquals(1000, tag.longValueFor("qux"));
            tag = tag.removeEntry("qux");
            assertEquals("", tag.valueFor("qux"));

            tags.remove(999);
            assertTrue(tag.hasEntry("foo"));
            assertEquals("bar", tag.valueFor("foo"));
            assertEquals(0, tags.tags().size());
        }

        Tag tag = tags.tagBy(999);
        assertEquals(999, tag.key());
        assertFalse(tag.hasEntry("foo"));
        assertSame(tag, tag.removeEntry("bar"));
        assertEquals("", tag.valueFor("blah"));
    }

    @Test
    void customTagsTest() {
        Tags tags = new TagsImpl(new HashMap<>());

        CustomTags customTags = new CustomTagsImpl(tags, () -> "foo");

        assertEquals(0, tags.tags().size());
        assertNotNull(tags.tagBy(1234));

        {
            Tag tag = tags.tagBy(999);
            assertEquals(999, tag.key());
            assertFalse(tag.hasEntry("foo"));

            customTags.tagWithCustomName(999, "bar");
            tag = tags.tagBy(999);
            assertEquals(999, tag.key());
            assertTrue(tag.hasEntry("foo"));
            assertEquals("bar", tag.valueFor("foo"));
        }

        CountingListener listener = new CountingListener();
        customTags = new NotifyingCustomTags(listener, customTags);

        customTags.tagWithCustomName(999, "baz");
        assertEquals(1, listener.count);

        Tag tag = tags.tagBy(999);
        assertEquals(999, tag.key());
        assertTrue(tag.hasEntry("foo"));
        assertEquals("baz", tag.valueFor("foo"));
    }

    private static final class CountingListener implements UpdateListener {
        int count;

        @Override
        public void notifyUpdated() {
            count++;
        }
    }
}
