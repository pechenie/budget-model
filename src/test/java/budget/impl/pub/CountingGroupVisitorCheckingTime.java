package budget.impl.pub;

import budget.view.journal.GroupItem;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @since 29.04.2017
 */
class CountingGroupVisitorCheckingTime extends AbstractCountingGroupVisitor<long[]> {
    CountingGroupVisitorCheckingTime(int groupId, TestCounters counters, long[] timeValues) {
        super(groupId, counters, timeValues);
    }

    @Override
    protected void checkItem(int groupId, int index, long[] data, GroupItem item) {
        assertEquals(data[index], item.state().time(), "Group " + groupId + ", record " + index + " time");
    }
}
