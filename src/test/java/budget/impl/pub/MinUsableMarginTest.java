package budget.impl.pub;

import budget.impl.api.DashboardImpl;
import budget.impl.model.BalanceReportsImpl;
import budget.impl.model.OperationsImpl;
import budget.impl.model.ReferencePeriodsImpl;
import budget.impl.model.entity.BalanceReportImpl;
import budget.impl.model.entity.OperationImpl;
import budget.impl.model.entity.ReferencePeriodImpl;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Kirill Berezin
 */
class MinUsableMarginTest {
    @Test
    void test() {
        OperationsImpl ops = new OperationsImpl(
                new OperationImpl(0, 8000d),
                new OperationImpl(25, -5000d),
                new OperationImpl(29, +10000d)
        );

        BalanceReportsImpl reports = new BalanceReportsImpl(Arrays.asList(
                new BalanceReportImpl(1, 8000d), //margin = 5000, "real" margin = 3000
                new BalanceReportImpl(15, 6500d), //margin = 3500, "real" = 1500
                new BalanceReportImpl(17, 6000d) //margin = 3000, "real" = 1000
                //min = 1000
        ));
        ReferencePeriodsImpl periods = new ReferencePeriodsImpl(Collections.singleton(
                new ReferencePeriodImpl(0, 30, 5000d)
        ));

        DashboardImpl dashboard = new DashboardImpl(ops, reports, periods);
        assertEquals(5000d, dashboard.usableMarginAtTime(1));
        assertEquals(3500d, dashboard.usableMarginAtTime(15));
        assertEquals(3000d, dashboard.usableMarginAtTime(17));
        assertEquals(1000d, dashboard.minUsableMarginSinceTime(1));
        assertEquals(1000d, dashboard.minUsableMarginSinceTime(15));
        assertEquals(1000d, dashboard.minUsableMarginSinceTime(17));

        assertEquals(0d, dashboard.minUsableMarginSinceTime(-10000));
        assertEquals(0d, dashboard.minUsableMarginSinceTime(100000));
    }
}
