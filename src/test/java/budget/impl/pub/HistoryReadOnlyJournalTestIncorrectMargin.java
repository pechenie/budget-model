package budget.impl.pub;

import budget.impl.api.JournalImpl;
import budget.impl.model.BalanceReportsImpl;
import budget.impl.model.OperationsImpl;
import budget.impl.model.ReferencePeriodsImpl;
import budget.impl.model.entity.BalanceReportImpl;
import budget.impl.model.entity.OperationImpl;
import budget.impl.model.entity.ReferencePeriodImpl;
import budget.model.source.BalanceReports;
import budget.model.source.Operations;
import budget.model.source.ReferencePeriods;
import budget.view.journal.JournalView;
import budget.view.journal.PeriodGroupView;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @since 20.04.2017
 */
class HistoryReadOnlyJournalTestIncorrectMargin {
    private static JournalView displayedJournal;

    @BeforeAll
    static void prepare() {
        Operations operations = new OperationsImpl(Arrays.asList(
                new OperationImpl(0, 300),
                new OperationImpl(6, 1000)
        ));
        BalanceReports reports = new BalanceReportsImpl(Collections.singletonList(
                new BalanceReportImpl(5, 200)
        ));
        ReferencePeriods periods = new ReferencePeriodsImpl(Collections.singletonList(
                new ReferencePeriodImpl(0, 10, 500)
        ));
        displayedJournal = new JournalImpl(reports, operations, periods, null, null).display();
    }

    /*
    JOURNAL IS GOING TO LOOK LIKE THIS:
    --------------------------------
    0 = PERIOD START, BALANCE = 0, PL = 0
    - 0 = TRANSACTION, VALUE = 300, BALANCE = 300, PL = 300
    - 5 = REPORT, BALANCE = 200, PL = 200
    - 6 = TRANSACTION, VALUE = 1000, BALANCE = 1200, PL = 400
    10 = PERIOD END, BALANCE = 1200, PL = 400
    --------------------------------
     */
    @Test
    void testPredefinedJournalGroupsCount() {
        assertEquals(1, displayedJournal.groupsCount());
    }

    @Test
    void testPredefinedJournalGroup1Items() {
        PeriodGroupView groupView = displayedJournal.groupAt(0);
        //data
        assertEquals(0, groupView.period().start(), "Group 1, start period");
        assertEquals(10, groupView.period().end(), "Group 1, end period");
        assertEquals(500, groupView.period().availableMargin(), "Group 1, available margin");

        assertEquals(0, groupView.startBalance().balance(), "Group 1, start balance");
        assertEquals(1200, groupView.endBalance().balance(), "Group 1, end balance");

        assertEquals(0, groupView.startBalance().margin(), "Group 1, start margin");
        assertEquals(400, groupView.endBalance().margin(), "Group 1, end margin");

        //group items
        assertEquals(3, groupView.itemsCount(), "Group 1, records count");

        GroupVisitorCheckingBalances costVisitor = new GroupVisitorCheckingBalances(1,
                new double[]{300d, Double.NaN, 1000d},
                new double[]{300d, 200d, 1200d},
                new double[]{300, 200, 400});
        TestCounters counters = new TestCounters();
        for (int i = 0; i < groupView.itemsCount(); ++i) {
            counters = groupView.itemAt(i).visitBy(new SimpleCountingGroupVisitor(1, counters));
            groupView.itemAt(i).visitBy(costVisitor);
        }
        assertEquals(2, counters.transactions, "Group 1, transactions #");
        assertEquals(1, counters.reports, "Group 1, reports #");
    }
}