package budget.impl.pub;

import budget.impl.model.BalanceReportsImpl;
import budget.impl.model.OperationsImpl;
import budget.impl.model.ReferencePeriodsImpl;
import budget.impl.model.entity.BalanceReportImpl;
import budget.impl.model.entity.OperationImpl;
import budget.impl.model.metrics.TimedMetricsImpl;
import budget.model.TimedMetrics;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Kirill Berezin
 */
class FirstBalanceTest {
    @SuppressWarnings("ConstantConditions")
    @Test
    void testFirstBalanceWithCriteria() {
        BalanceReportsImpl balanceReports = new BalanceReportsImpl();
        assertTrue(balanceReports.report(new BalanceReportImpl(0, 1000)));
        OperationsImpl operations = new OperationsImpl();
        operations.execute(new OperationImpl(10, -100));
        operations.execute(new OperationImpl(20, -100));
        operations.execute(new OperationImpl(30, -100));
        operations.execute(new OperationImpl(40, -100));
        operations.execute(new OperationImpl(50, -100));

        TimedMetrics metrics = new TimedMetricsImpl(operations, balanceReports, new ReferencePeriodsImpl(Collections.emptyList()));
        assertNotNull(metrics.firstBalanceWithCriteria(0, 50, d -> true));
        assertEquals(0, metrics.firstBalanceWithCriteria(0, 50, d -> true).time());
        assertNotNull(metrics.firstBalanceWithCriteria(0, 50, d -> d < 900d));
        assertEquals(20, metrics.firstBalanceWithCriteria(0, 50, d -> d < 900d).time());
        assertNotNull(metrics.firstBalanceWithCriteria(0, 50, d -> d < 501d));
        assertEquals(50, metrics.firstBalanceWithCriteria(0, 50, d -> d < 501d).time());
        assertNull(metrics.firstBalanceWithCriteria(0, 50, d -> d < 0d));
    }
}
