package budget.impl.pub;

import budget.view.journal.BalanceReportView;
import budget.view.journal.GroupItemVisitor;
import budget.view.journal.OperationView;
import org.jetbrains.annotations.NotNull;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @since 29.04.2017
 */
class GroupVisitorCheckingBalances implements GroupItemVisitor<Object> {
    private final int groupId;
    private final double[] cost;
    private final double[] balance;
    private final double[] margin;

    private int index = 0;

    GroupVisitorCheckingBalances(int groupId, double[] cost, double[] balance, double[] margin) {
        this.groupId = groupId;
        this.cost = cost;
        this.balance = balance;
        this.margin = margin;
    }

    @Override
    public Object onOperation(@NotNull OperationView item) {
        assertEquals(cost[index], item.cost(), "Group " + groupId + ", record " + index + " cost");
        assertEquals(balance[index], item.state().balance(), "Group " + groupId + ", record " + index + " balance");
        assertEquals(margin[index], item.state().margin(), "Group " + groupId + ", record " + index + " margin");

        index++;
        return null;
    }

    @Override
    public TestCounters onBalanceReport(@NotNull BalanceReportView item) {
        assertEquals(balance[index], item.state().balance(), "Group " + groupId + ", record " + index + " cost");
        assertEquals(margin[index], item.state().margin(), "Group " + groupId + ", record " + index + " cost");

        index++;
        return null;
    }
}
