package budget.impl.pub;

import budget.impl.model.time.AlignedMonthHalvesIterator;
import budget.impl.model.time.ConstantIterator;
import budget.impl.model.time.DailyIterator;
import budget.impl.model.time.LastDayOfMonthIterator;
import budget.impl.model.time.MonthlyIterator;
import budget.impl.model.time.StartOfMonthIterator;
import budget.impl.model.time.StartOfWeekIterator;
import budget.impl.model.time.WeeklyIterator;
import budget.model.time.TimestampIterator;
import org.joda.time.LocalDate;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Kirill Berezin
 */
class TimestampIteratorsTest {
    @Test
    void testAlignedMonthsHalves() {
        TimestampIterator iterator = new AlignedMonthHalvesIterator();

        LocalDate date = new LocalDate(2017, 4, 9);
        long prevMillis = date.toDateTimeAtCurrentTime().getMillis();

        LocalDate nextDate = new LocalDate(iterator.next(prevMillis));
        assertEquals(16, nextDate.getDayOfMonth());
        assertEquals(4, nextDate.getMonthOfYear());
        assertTrue(prevMillis < nextDate.toDateTimeAtStartOfDay().getMillis());

        prevMillis = nextDate.toDateTimeAtStartOfDay().getMillis();
        nextDate = new LocalDate(iterator.next(prevMillis));
        assertEquals(1, nextDate.getDayOfMonth());
        assertEquals(5, nextDate.getMonthOfYear());
        assertTrue(prevMillis < nextDate.toDateTimeAtStartOfDay().getMillis());

        prevMillis = nextDate.toDateTimeAtStartOfDay().getMillis();
        nextDate = new LocalDate(iterator.next(prevMillis));
        assertEquals(16, nextDate.getDayOfMonth());
        assertEquals(5, nextDate.getMonthOfYear());
        assertTrue(prevMillis < nextDate.toDateTimeAtStartOfDay().getMillis());

        //now backwards
        prevMillis = nextDate.toDateTimeAtStartOfDay().getMillis();
        nextDate = new LocalDate(iterator.alignedStart(prevMillis - 1));
        assertEquals(1, nextDate.getDayOfMonth());
        assertEquals(5, nextDate.getMonthOfYear());
        assertTrue(prevMillis > nextDate.toDateTimeAtStartOfDay().getMillis());

        prevMillis = nextDate.toDateTimeAtStartOfDay().getMillis();
        nextDate = new LocalDate(iterator.alignedStart(prevMillis - 1));
        assertEquals(16, nextDate.getDayOfMonth());
        assertEquals(4, nextDate.getMonthOfYear());
        assertTrue(prevMillis > nextDate.toDateTimeAtStartOfDay().getMillis());

        prevMillis = nextDate.toDateTimeAtStartOfDay().getMillis();
        nextDate = new LocalDate(iterator.alignedStart(prevMillis - 1));
        assertEquals(1, nextDate.getDayOfMonth());
        assertEquals(4, nextDate.getMonthOfYear());
        assertTrue(prevMillis > nextDate.toDateTimeAtStartOfDay().getMillis());
    }

    @Test
    void testStartOfWeekIterator() {
        doTestStartOfWeekIterator(1);
        doTestStartOfWeekIterator(2);
        doTestStartOfWeekIterator(7);
    }

    private void doTestStartOfWeekIterator(int count) {
        TimestampIterator iterator = new StartOfWeekIterator(count);

        LocalDate date = new LocalDate(2017, 4, 9).withDayOfWeek(1);
        long prevMillis = date.toDateTimeAtCurrentTime().getMillis();

        LocalDate nextDate = new LocalDate(iterator.next(prevMillis));
        assertEquals(1, nextDate.getDayOfWeek());
        assertTrue(prevMillis < nextDate.toDateTimeAtStartOfDay().getMillis());
        assertEquals(7 * count, nextDate.getDayOfYear() - date.getDayOfYear(), date.toString() + " vs " + nextDate.toString());

        prevMillis = nextDate.toDateTimeAtStartOfDay().getMillis();
        nextDate = new LocalDate(iterator.next(prevMillis));
        assertEquals(1, nextDate.getDayOfWeek());
        assertTrue(prevMillis < nextDate.toDateTimeAtStartOfDay().getMillis());
        assertEquals(14 * count, nextDate.getDayOfYear() - date.getDayOfYear(), date.toString() + " vs " + nextDate.toString());

        //now backwards
        prevMillis = nextDate.toDateTimeAtStartOfDay().getMillis();
        nextDate = new LocalDate(iterator.alignedStart(prevMillis - 1));
        assertEquals(1, nextDate.getDayOfWeek());
        assertTrue(prevMillis > nextDate.toDateTimeAtStartOfDay().getMillis());
        assertEquals(14 * count - 7, nextDate.getDayOfYear() - date.getDayOfYear(), date.toString() + " vs " + nextDate.toString());
    }

    @Test
    void testStartOfMonthIterator() {
        doTestStartOfMonthIterator(1);
        doTestStartOfMonthIterator(2);
        doTestStartOfMonthIterator(3);
    }

    private void doTestStartOfMonthIterator(int count) {
        TimestampIterator iterator = new StartOfMonthIterator(count);

        int start = 4;
        LocalDate date = new LocalDate(2017, start, 12);
        date = new LocalDate(iterator.next(date.toDateTimeAtStartOfDay().getMillis()));
        assertEquals(1, date.getDayOfMonth());
        assertEquals(start + count, date.getMonthOfYear());

        date = new LocalDate(iterator.next(date.toDateTimeAtStartOfDay().getMillis()));
        assertEquals(1, date.getDayOfMonth());
        assertEquals(start + 2 * count, date.getMonthOfYear());

        //backwards
        date = new LocalDate(2017, 5, 13);
        date = new LocalDate(iterator.alignedStart(date.toDateTimeAtCurrentTime().getMillis()));
        assertEquals(1, date.getDayOfMonth());
        assertEquals(5, date.getMonthOfYear());
    }

    @Test
    void testWeeklyIterator() {
        doTestWeeklyIterator(1);
        doTestWeeklyIterator(2);
        doTestWeeklyIterator(3);
    }

    private void doTestWeeklyIterator(int count) {
        TimestampIterator iterator = new WeeklyIterator(count);

        LocalDate date = new LocalDate(2017, 7, 4);
        long prevMillis = date.toDateTimeAtStartOfDay().getMillis();
        long nextMillis = iterator.next(prevMillis);
        LocalDate nextDate = new LocalDate(nextMillis);
        assertEquals(7 * count, nextDate.getDayOfYear() - date.getDayOfYear());
        assertEquals(prevMillis, iterator.alignedStart(prevMillis));
    }

    @Test
    void testMonthlyIterator() {
        doTestMonthlyIterator(1);
        doTestMonthlyIterator(2);
        doTestMonthlyIterator(3);
    }

    private void doTestMonthlyIterator(int count) {
        TimestampIterator iterator = new MonthlyIterator(count);

        LocalDate date = new LocalDate(2017, 1, 4);
        long prevMillis = date.toDateTimeAtStartOfDay().getMillis();
        long nextMillis = iterator.next(prevMillis);
        LocalDate nextDate = new LocalDate(nextMillis);
        assertEquals(4, date.getDayOfMonth());
        assertEquals(date.getDayOfMonth(), nextDate.getDayOfMonth());
        assertEquals(count, nextDate.getMonthOfYear() - date.getMonthOfYear());
        assertEquals(prevMillis, iterator.alignedStart(prevMillis));
    }

    @Test
    void testDailyIterator() {
        doTestDailyIterator(1);
        doTestDailyIterator(2);
        doTestDailyIterator(3);
    }

    private void doTestDailyIterator(int count) {
        TimestampIterator iterator = new DailyIterator(count);

        LocalDate date = new LocalDate(2017, 1, 4);
        long prevMillis = date.toDateTimeAtStartOfDay().getMillis();
        long nextMillis = iterator.next(prevMillis);
        LocalDate nextDate = new LocalDate(nextMillis);
        assertEquals(1, date.getMonthOfYear());
        assertEquals(date.getYear(), date.getYear());
        assertEquals(count, nextDate.getDayOfYear() - date.getDayOfYear());
        assertEquals(prevMillis, iterator.alignedStart(prevMillis));
    }

    @Test
    void testLastDayOfMonthIterator() {
        TimestampIterator iterator = new LastDayOfMonthIterator();

        LocalDate date = new LocalDate(2017, 1, 4);
        long prevMillis = date.toDateTimeAtStartOfDay().getMillis();
        long nextMillis = iterator.next(prevMillis);
        LocalDate nextDate = new LocalDate(nextMillis);
        assertEquals(31, nextDate.getDayOfMonth());
        assertEquals(1, nextDate.getMonthOfYear());

        date = nextDate;
        prevMillis = date.toDateTimeAtStartOfDay().getMillis();
        nextMillis = iterator.next(prevMillis);
        nextDate = new LocalDate(nextMillis);
        assertEquals(28, nextDate.getDayOfMonth());
        assertEquals(2, nextDate.getMonthOfYear());

        date = nextDate;
        prevMillis = date.toDateTimeAtStartOfDay().getMillis();
        nextMillis = iterator.next(prevMillis);
        nextDate = new LocalDate(nextMillis);
        assertEquals(31, nextDate.getDayOfMonth());
        assertEquals(3, nextDate.getMonthOfYear());

        //now backwards
        date = nextDate;
        prevMillis = date.toDateTimeAtStartOfDay().getMillis();
        nextMillis = iterator.alignedStart(prevMillis);
        nextDate = new LocalDate(nextMillis);
        assertEquals(28, nextDate.getDayOfMonth());
        assertEquals(2, nextDate.getMonthOfYear());

        date = nextDate;
        prevMillis = date.toDateTimeAtStartOfDay().getMillis();
        nextMillis = iterator.alignedStart(prevMillis);
        nextDate = new LocalDate(nextMillis);
        assertEquals(31, nextDate.getDayOfMonth());
        assertEquals(1, nextDate.getMonthOfYear());
    }

    @Test
    void testConstantIterator() {
        ConstantIterator iterator = new ConstantIterator(55);
        assertEquals(55, iterator.next(0));
        assertEquals(110, iterator.next(55));
        assertEquals(100, iterator.next(45));
        assertEquals(0, iterator.alignedStart(0));
        assertEquals(55, iterator.alignedStart(55));
    }
}
