package budget.impl.pub;

import budget.impl.model.BalanceReportsImpl;
import budget.impl.model.OperationsImpl;
import budget.impl.model.ReferencePeriodsImpl;
import budget.impl.model.entity.BalanceReportImpl;
import budget.impl.model.entity.OperationImpl;
import budget.impl.model.entity.ReferencePeriodImpl;
import budget.impl.model.persistence.JournalPersistence;
import budget.impl.model.persistence.PersistableBalanceBook;
import budget.impl.model.persistence.PersistableOperationBook;
import budget.impl.model.persistence.PersistablePeriodBook;
import budget.model.BalanceReport;
import budget.model.Operation;
import budget.model.ReferencePeriod;
import budget.model.book.PeriodBook;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PersistableBooksTest {

    private static final double EPS = 0.00001;

    private static class PersistenceImpl implements JournalPersistence {
        private final BalanceReportsImpl reports = new BalanceReportsImpl();
        private final OperationsImpl operations = new OperationsImpl();
        private Collection<? extends ReferencePeriod> periods = new ArrayList<>();

        @Override
        public void store(@NotNull Operation operation) {
            assertTrue(operations.execute(operation));
        }

        @Override
        public void store(@NotNull BalanceReport balanceReport) {
            assertTrue(reports.report(balanceReport));
        }

        @Override
        public void replaceAll(@NotNull Collection<? extends ReferencePeriod> referencePeriods) {
            periods = referencePeriods;
        }

        @Override
        public void remove(long recordAtTime) {
            reports.eraseRecord(recordAtTime);
            operations.eraseRecord(recordAtTime);
        }

        @NotNull
        @Override
        public Collection<? extends Operation> readOperations() {
            return operations.range(Long.MIN_VALUE, true, Long.MAX_VALUE, true);
        }

        @NotNull
        @Override
        public Collection<? extends BalanceReport> readReports() {
            return reports.range(Long.MIN_VALUE, true, Long.MAX_VALUE, true);
        }

        @NotNull
        @Override
        public Collection<? extends ReferencePeriod> readPeriods() {
            return periods;
        }
    }

    @SuppressWarnings("ConstantConditions")
    @Test
    void testPeriodBookWithPersistence() {
        JournalPersistence persistence = new PersistenceImpl();

        ReferencePeriodsImpl periods = new ReferencePeriodsImpl(persistence.readPeriods());

        //check empty
        assertEquals(0, periods.periods().size());

        PeriodBook book = new PersistablePeriodBook(persistence, periods, periods);

        assertTrue(book.append(new ReferencePeriodImpl(0, 10, 1d)));
        assertFalse(book.append(new ReferencePeriodImpl(5, 10, 2d)));
        assertTrue(book.append(new ReferencePeriodImpl(10, 30, 2d)));
        assertNotNull(periods.periodAtTime(5));
        assertEquals(1d, periods.periodAtTime(5).availableMargin(), EPS);
        assertNotNull(periods.periodAtTime(15));
        assertEquals(2d, periods.periodAtTime(15).availableMargin(), EPS);
        assertEquals(2, periods.periods().size());
        assertEquals(2, persistence.readPeriods().size());

        assertFalse(book.rewrite(Collections.singletonList(new ReferencePeriodImpl(5, 15, 3d))));
        assertTrue(book.rewrite(Collections.singletonList(new ReferencePeriodImpl(30, 150, 3d))));
        assertNotNull(periods.periodAtTime(5));
        assertEquals(1d, periods.periodAtTime(5).availableMargin(), EPS);
        assertEquals(3, periods.periods().size());
        assertEquals(3, persistence.readPeriods().size());

        assertTrue(book.rewrite(Collections.singletonList(new ReferencePeriodImpl(0, 150, 3d))));
        assertNotNull(periods.periodAtTime(5));
        assertEquals(3d, periods.periodAtTime(5).availableMargin(), EPS);
        assertEquals(1, periods.periods().size());
        assertEquals(1, persistence.readPeriods().size());
    }

    @Test
    void testRecordBooksWithPersistence() throws Exception {
        JournalPersistence persistence = new PersistenceImpl();

        BalanceReportsImpl reports = new BalanceReportsImpl();
        OperationsImpl operations = new OperationsImpl();

        //check empty
        assertEquals(Long.MIN_VALUE, reports.lastReportAtTime(Long.MAX_VALUE, true).time());
        assertEquals(0.0d, reports.lastReportAtTime(Long.MAX_VALUE, true).amount(), EPS);
        assertEquals(0, operations.range(Long.MIN_VALUE, true, Long.MAX_VALUE, true).size());
        assertEquals(0, persistence.readOperations().size());
        assertEquals(0, persistence.readReports().size());

        PersistableBalanceBook balanceBook = new PersistableBalanceBook(persistence, reports);
        PersistableOperationBook operationBook = new PersistableOperationBook(persistence, operations);

        //add balance report
        assertTrue(balanceBook.report(new BalanceReportImpl(10, 100d)));
        assertEquals(10L, reports.lastReportAtTime(Long.MAX_VALUE, true).time());
        assertEquals(100d, reports.lastReportAtTime(Long.MAX_VALUE, true).amount(), EPS);
        assertEquals(0, operations.range(Long.MIN_VALUE, true, Long.MAX_VALUE, true).size());
        assertEquals(0, persistence.readOperations().size());
        assertEquals(1, persistence.readReports().size());
        BalanceReport persistedReport = persistence.readReports().iterator().next();
        assertEquals(10L, persistedReport.time());
        assertEquals(100d, persistedReport.amount(), EPS);

        //add operation
        assertTrue(operationBook.execute(new OperationImpl(11, 200d)));
        assertEquals(10L, reports.lastReportAtTime(Long.MAX_VALUE, true).time());
        assertEquals(100d, reports.lastReportAtTime(Long.MAX_VALUE, true).amount(), EPS);
        assertEquals(1, persistence.readReports().size());
        assertEquals(1, persistence.readOperations().size());
        assertEquals(1, operations.range(Long.MIN_VALUE, true, Long.MAX_VALUE, true).size());
        assertEquals(200d, operations.operationSum(Long.MIN_VALUE, Long.MAX_VALUE, true), EPS);
        Operation persistedOperation = persistence.readOperations().iterator().next();
        assertEquals(11L, persistedOperation.time());
        assertEquals(200d, persistedOperation.cost(), EPS);

        //duplicate balance report and operation
        assertFalse(balanceBook.report(new BalanceReportImpl(10, 9999d)));
        assertFalse(operationBook.execute(new OperationImpl(11, 9999d)));

        assertEquals(1, persistence.readReports().size());
        assertEquals(1, persistence.readOperations().size());

        assertEquals(10L, reports.lastReportAtTime(Long.MAX_VALUE, true).time());
        assertEquals(100d, reports.lastReportAtTime(Long.MAX_VALUE, true).amount(), EPS);
        persistedReport = persistence.readReports().iterator().next();
        assertEquals(10L, persistedReport.time());
        assertEquals(100d, persistedReport.amount(), EPS);

        assertEquals(1, operations.range(Long.MIN_VALUE, true, Long.MAX_VALUE, true).size());
        assertEquals(200d, operations.operationSum(Long.MIN_VALUE, Long.MAX_VALUE, true), EPS);
        persistedOperation = persistence.readOperations().iterator().next();
        assertEquals(11L, persistedOperation.time());
        assertEquals(200d, persistedOperation.cost(), EPS);

        //check records are correct
        assertTrue(balanceBook.hasRecord(10L));
        assertTrue(operationBook.hasRecord(11L));

        //erase records
        balanceBook.eraseRecord(10L);
        operationBook.eraseRecord(11L);

        assertFalse(balanceBook.hasRecord(10L));
        assertFalse(operationBook.hasRecord(11L));

        assertEquals(Long.MIN_VALUE, reports.lastReportAtTime(Long.MAX_VALUE, true).time());
        assertEquals(0.0d, reports.lastReportAtTime(Long.MAX_VALUE, true).amount(), EPS);
        assertEquals(0, operations.range(Long.MIN_VALUE, true, Long.MAX_VALUE, true).size());
        assertEquals(0, persistence.readOperations().size());
        assertEquals(0, persistence.readReports().size());
    }
}