package budget.impl.pub;

/**
 * @since 29.04.2017
 */
class TestCounters {
    int transactions;
    int reports;

    int index() {
        return transactions + reports;
    }
}
