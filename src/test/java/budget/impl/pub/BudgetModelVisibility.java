package budget.impl.pub;

import budget.api.entity.RuleView;
import budget.impl.app.BudgetModelImpl;
import budget.impl.model.persistence.JournalPersistence;
import budget.impl.model.persistence.PeriodRulesPersistence;
import budget.impl.model.tag.TagsImpl;
import budget.model.BalanceReport;
import budget.model.Operation;
import budget.model.ReferencePeriod;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;

/**
 * @author Kirill Berezin
 */
class BudgetModelVisibility {
    @Test
    void test() {
        //just to make sure it is visible outside
        new BudgetModelImpl<>(new EmptyPersistence(), new EmptyRulesPersistence(), Collections.emptyList(),
                Collections.emptyList(), new TagsImpl(new HashMap<>()));
    }

    private static class EmptyPersistence implements JournalPersistence {
        @Override
        public void store(@NotNull Operation operation) {

        }

        @Override
        public void store(@NotNull BalanceReport balanceReport) {

        }

        @Override
        public void replaceAll(@NotNull Collection<? extends ReferencePeriod> referencePeriods) {

        }

        @Override
        public void remove(long recordAtTime) {

        }

        @NotNull
        @Override
        public Collection<? extends Operation> readOperations() {
            return Collections.emptySet();
        }

        @NotNull
        @Override
        public Collection<? extends BalanceReport> readReports() {
            return Collections.emptySet();
        }

        @NotNull
        @Override
        public Collection<? extends ReferencePeriod> readPeriods() {
            return Collections.emptySet();
        }
    }

    private static class EmptyRulesPersistence implements PeriodRulesPersistence<RuleView, Object> {
        @Override
        public void save(@NotNull RuleView view, @NotNull Object o) {

        }

        @Nullable
        @Override
        public String savedId() {
            return null;
        }

        @Nullable
        @Override
        public Object savedInput() {
            return null;
        }
    }
}
