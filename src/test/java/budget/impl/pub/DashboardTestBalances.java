package budget.impl.pub;

import budget.api.Dashboard;
import budget.impl.api.DashboardImpl;
import budget.impl.model.BalanceReportsImpl;
import budget.impl.model.OperationsImpl;
import budget.impl.model.ReferencePeriodsImpl;
import budget.impl.model.entity.BalanceReportImpl;
import budget.impl.model.entity.OperationImpl;
import budget.impl.model.entity.ReferencePeriodImpl;
import budget.view.TimedBalance;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @since 30.04.2017
 */
class DashboardTestBalances {
    @Test
    void testReportedVsEstimated() {
        BalanceReportsImpl balanceReports = new BalanceReportsImpl();
        balanceReports.report(new BalanceReportImpl(0, 1000));
        OperationsImpl operations = new OperationsImpl();
        operations.execute(new OperationImpl(-1, 1000)); //INITIAL MONEY
        operations.execute(new OperationImpl(10, -100));
        operations.execute(new OperationImpl(20, -100));
        operations.execute(new OperationImpl(30, -100));
        operations.execute(new OperationImpl(40, -100));
        operations.execute(new OperationImpl(50, -100));
        Dashboard dashboard = new DashboardImpl(operations, balanceReports, new ReferencePeriodsImpl(Collections.emptyList()));

        assertEquals(1000, dashboard.lastReportedBalanceForTime(0).balance());
        assertEquals(1000, dashboard.lastReportedBalanceForTime(10).balance());
        assertEquals(1000, dashboard.lastReportedBalanceForTime(20).balance());
        assertEquals(1000, dashboard.lastReportedBalanceForTime(25).balance());
        assertEquals(1000, dashboard.lastReportedBalanceForTime(30).balance());
        assertEquals(1000, dashboard.lastReportedBalanceForTime(40).balance());
        assertEquals(1000, dashboard.lastReportedBalanceForTime(50).balance());
        assertEquals(1000, dashboard.lastReportedBalanceForTime(55).balance());
        assertEquals(0, dashboard.lastReportedBalanceForTime(0).time());
        assertEquals(0, dashboard.lastReportedBalanceForTime(10).time());
        assertEquals(0, dashboard.lastReportedBalanceForTime(20).time());
        assertEquals(0, dashboard.lastReportedBalanceForTime(25).time());
        assertEquals(0, dashboard.lastReportedBalanceForTime(30).time());
        assertEquals(0, dashboard.lastReportedBalanceForTime(40).time());
        assertEquals(0, dashboard.lastReportedBalanceForTime(50).time());
        assertEquals(0, dashboard.lastReportedBalanceForTime(55).time());

        assertEquals(1000, dashboard.estimatedBalanceAtTime(0));
        assertEquals(900, dashboard.estimatedBalanceAtTime(10));
        assertEquals(800, dashboard.estimatedBalanceAtTime(20));
        assertEquals(800, dashboard.estimatedBalanceAtTime(25));
        assertEquals(700, dashboard.estimatedBalanceAtTime(30));
        assertEquals(600, dashboard.estimatedBalanceAtTime(40));
        assertEquals(500, dashboard.estimatedBalanceAtTime(50));
        assertEquals(500, dashboard.estimatedBalanceAtTime(55));
    }

    @Test
    void testReportedVsEstimatedWithPeriods() {
        BalanceReportsImpl balanceReports = new BalanceReportsImpl();
        balanceReports.report(new BalanceReportImpl(0, 1000));
        OperationsImpl operations = new OperationsImpl();
        operations.execute(new OperationImpl(-1, 1000)); //INITIAL MONEY
        operations.execute(new OperationImpl(10, -100));
        operations.execute(new OperationImpl(20, -100));
        operations.execute(new OperationImpl(30, -100));
        operations.execute(new OperationImpl(40, -100));
        operations.execute(new OperationImpl(50, -100));
        Dashboard dashboard = new DashboardImpl(operations, balanceReports,
                new ReferencePeriodsImpl(Collections.singleton(new ReferencePeriodImpl(0, 100, 500))));

        assertEquals(1000, dashboard.lastReportedBalanceForTime(0).balance());
        assertEquals(1000, dashboard.lastReportedBalanceForTime(10).balance());
        assertEquals(1000, dashboard.lastReportedBalanceForTime(20).balance());
        assertEquals(1000, dashboard.lastReportedBalanceForTime(25).balance());
        assertEquals(1000, dashboard.lastReportedBalanceForTime(30).balance());
        assertEquals(1000, dashboard.lastReportedBalanceForTime(40).balance());
        assertEquals(1000, dashboard.lastReportedBalanceForTime(50).balance());
        assertEquals(1000, dashboard.lastReportedBalanceForTime(55).balance());
        assertEquals(0, dashboard.lastReportedBalanceForTime(0).time());
        assertEquals(0, dashboard.lastReportedBalanceForTime(10).time());
        assertEquals(0, dashboard.lastReportedBalanceForTime(20).time());
        assertEquals(0, dashboard.lastReportedBalanceForTime(25).time());
        assertEquals(0, dashboard.lastReportedBalanceForTime(30).time());
        assertEquals(0, dashboard.lastReportedBalanceForTime(40).time());
        assertEquals(0, dashboard.lastReportedBalanceForTime(50).time());
        assertEquals(0, dashboard.lastReportedBalanceForTime(55).time());

        assertEquals(1000, dashboard.estimatedBalanceAtTime(0));
        assertEquals(900, dashboard.estimatedBalanceAtTime(10));
        assertEquals(800, dashboard.estimatedBalanceAtTime(20));
        assertEquals(800, dashboard.estimatedBalanceAtTime(25));
        assertEquals(700, dashboard.estimatedBalanceAtTime(30));
        assertEquals(600, dashboard.estimatedBalanceAtTime(40));
        assertEquals(500, dashboard.estimatedBalanceAtTime(50));
        assertEquals(500, dashboard.estimatedBalanceAtTime(55));
    }

    @SuppressWarnings("ConstantConditions")
    @Test
    void testFirstBalanceWithCriteria() {
        BalanceReportsImpl balanceReports = new BalanceReportsImpl();
        balanceReports.report(new BalanceReportImpl(1, 1000));
        OperationsImpl operations = new OperationsImpl();
        operations.execute(new OperationImpl(0, 1000)); //INITIAL MONEY
        operations.execute(new OperationImpl(10, -100));
        operations.execute(new OperationImpl(20, -100));
        operations.execute(new OperationImpl(30, -100));
        operations.execute(new OperationImpl(40, -100));
        operations.execute(new OperationImpl(50, -100));
        Dashboard dashboard = new DashboardImpl(operations, balanceReports,
                new ReferencePeriodsImpl(Collections.singleton(new ReferencePeriodImpl(0, 100, 500))));

        assertNotNull(dashboard.checkBalanceSinceLastReport(d -> true));
        assertEquals(1, dashboard.checkBalanceSinceLastReport(d -> true).time());
        assertNotNull(dashboard.checkBalanceSinceLastReport(d -> d < 900d));
        assertEquals(20, dashboard.checkBalanceSinceLastReport(d -> d < 900d).time());
        assertNotNull(dashboard.checkBalanceSinceLastReport(d -> d < 501d));
        assertEquals(50, dashboard.checkBalanceSinceLastReport(d -> d < 501d).time());
        assertNull(dashboard.checkBalanceSinceLastReport(d -> d < 0d));
    }

    @Test
    void testMinBalances() {
        BalanceReportsImpl balanceReports = new BalanceReportsImpl();
        balanceReports.report(new BalanceReportImpl(1, 1000));
        balanceReports.report(new BalanceReportImpl(2, 900));
        balanceReports.report(new BalanceReportImpl(11, 300));
        OperationsImpl operations = new OperationsImpl();
        operations.execute(new OperationImpl(0, 1000));
        operations.execute(new OperationImpl(10, -100));
        operations.execute(new OperationImpl(20, -100));
        operations.execute(new OperationImpl(30, 100));
        operations.execute(new OperationImpl(40, -300));
        operations.execute(new OperationImpl(50, 200));
        operations.execute(new OperationImpl(51, 10000));
        Dashboard dashboard = new DashboardImpl(operations, balanceReports, new ReferencePeriodsImpl(Collections.singleton(
                new ReferencePeriodImpl(0, 100, 100))));

        TimedBalance timedBalance = dashboard.minBalanceSinceLastReport();
        assertEquals(0, timedBalance.balance());
        assertEquals(40, timedBalance.time());

        dashboard = new DashboardImpl(operations, balanceReports, new ReferencePeriodsImpl(Collections.singleton(
                new ReferencePeriodImpl(0, 30, 100))));

        timedBalance = dashboard.minBalanceSinceLastReport();
        assertEquals(200, timedBalance.balance());
        assertEquals(20, timedBalance.time());
    }

    @Test
    void testMinBalancesWeirdPeriod() {
        BalanceReportsImpl balanceReports = new BalanceReportsImpl();
        balanceReports.report(new BalanceReportImpl(-11, 1000));
        balanceReports.report(new BalanceReportImpl(-9, 900));
        balanceReports.report(new BalanceReportImpl(-5, 300));
        OperationsImpl operations = new OperationsImpl();
        operations.execute(new OperationImpl(-20, 1000));
        operations.execute(new OperationImpl(-10, -100));
        operations.execute(new OperationImpl(-4, -100));
        operations.execute(new OperationImpl(30, 100));
        operations.execute(new OperationImpl(40, -300));
        operations.execute(new OperationImpl(50, 200));
        operations.execute(new OperationImpl(51, 10000));
        Dashboard dashboard = new DashboardImpl(operations, balanceReports, new ReferencePeriodsImpl(Collections.singleton(
                new ReferencePeriodImpl(0, 100, 100))));

        TimedBalance timedBalance = dashboard.minBalanceSinceLastReport();
        assertEquals(0, timedBalance.balance());
        assertEquals(40, timedBalance.time());

        dashboard = new DashboardImpl(operations, balanceReports, new ReferencePeriodsImpl(Collections.singleton(
                new ReferencePeriodImpl(0, 30, 100))));

        timedBalance = dashboard.minBalanceSinceLastReport();
        assertEquals(200, timedBalance.balance());
        assertEquals(-4, timedBalance.time());
    }

    @Test
    void testMinBalancesIncludingPeriods() {
        BalanceReportsImpl balanceReports = new BalanceReportsImpl();
        balanceReports.report(new BalanceReportImpl(1, 11000));
        balanceReports.report(new BalanceReportImpl(2, 1900));
        balanceReports.report(new BalanceReportImpl(11, 1720));
        balanceReports.report(new BalanceReportImpl(33, 1530));
        balanceReports.report(new BalanceReportImpl(41, 1310));
        balanceReports.report(new BalanceReportImpl(49, 1200));
        OperationsImpl operations = new OperationsImpl();
        operations.execute(new OperationImpl(0, 11000));
        operations.execute(new OperationImpl(10, -100));
        operations.execute(new OperationImpl(20, -100));
        operations.execute(new OperationImpl(30, -100));
        operations.execute(new OperationImpl(40, -100));
        operations.execute(new OperationImpl(101, 5000));
        Dashboard dashboard = new DashboardImpl(operations, balanceReports,
                new ReferencePeriodsImpl(Arrays.asList(
                        new ReferencePeriodImpl(0, 50, 100),
                        new ReferencePeriodImpl(50, 100, 500),
                        new ReferencePeriodImpl(100, 200, 100)
                ))
        );

        TimedBalance timedBalance = dashboard.minBalanceSinceLastReport();
        assertEquals(100, timedBalance.time());
        assertEquals(700, timedBalance.balance());
    }

    @Test
    void testMinBalancesIncludingPeriodsBelowZero() {
        BalanceReportsImpl balanceReports = new BalanceReportsImpl();
        balanceReports.report(new BalanceReportImpl(1, 1000));
        balanceReports.report(new BalanceReportImpl(2, 900));
        balanceReports.report(new BalanceReportImpl(11, 720));
        balanceReports.report(new BalanceReportImpl(33, 530));
        balanceReports.report(new BalanceReportImpl(41, 310));
        balanceReports.report(new BalanceReportImpl(49, 200)); //real = 200
        OperationsImpl operations = new OperationsImpl();
        operations.execute(new OperationImpl(0, 1000));
        operations.execute(new OperationImpl(10, -100));
        operations.execute(new OperationImpl(20, -100));
        operations.execute(new OperationImpl(30, -100));
        operations.execute(new OperationImpl(40, -100)); //perfect = 600
        operations.execute(new OperationImpl(101, 5000));
        Dashboard dashboard = new DashboardImpl(operations, balanceReports,
                new ReferencePeriodsImpl(Arrays.asList(
                        new ReferencePeriodImpl(0, 50, 100),
                        new ReferencePeriodImpl(50, 100, 500),
                        new ReferencePeriodImpl(100, 200, 100)
                ))
        );

        TimedBalance timedBalance = dashboard.minBalanceSinceLastReport();
        assertEquals(100, timedBalance.time());
        assertEquals(0, timedBalance.balance()); //can't go below zero only because of margin

        assertTrue(operations.execute(new OperationImpl(55, -300d)));
        timedBalance = dashboard.minBalanceSinceLastReport();
        assertEquals(55, timedBalance.time());
        assertEquals(-100d, timedBalance.balance()); //but surely we can because of operations
    }

    @Test
    void testMinBalancesIncludingPeriodsBelowZero2() {
        BalanceReportsImpl balanceReports = new BalanceReportsImpl();
        balanceReports.report(new BalanceReportImpl(1, 1000));
        balanceReports.report(new BalanceReportImpl(2, 900));
        balanceReports.report(new BalanceReportImpl(11, 720));
        balanceReports.report(new BalanceReportImpl(33, 530));
        balanceReports.report(new BalanceReportImpl(41, 310));
        balanceReports.report(new BalanceReportImpl(49, 200)); //real = 200
        OperationsImpl operations = new OperationsImpl();
        operations.execute(new OperationImpl(0, 1000));
        operations.execute(new OperationImpl(10, -100));
        operations.execute(new OperationImpl(20, -100));
        operations.execute(new OperationImpl(30, -100));
        operations.execute(new OperationImpl(40, -100)); //perfect = 600
        Dashboard dashboard = new DashboardImpl(operations, balanceReports,
                new ReferencePeriodsImpl(Arrays.asList(
                        new ReferencePeriodImpl(0, 50, 100),
                        new ReferencePeriodImpl(50, 100, 500),
                        new ReferencePeriodImpl(100, 200, 100)
                ))
        );

        TimedBalance timedBalance = dashboard.minBalanceSinceLastReport();
        assertEquals(0, timedBalance.balance()); //can't go below zero only because of margin
        assertEquals(100, timedBalance.time());

        assertTrue(operations.execute(new OperationImpl(55, -300d)));
        timedBalance = dashboard.minBalanceSinceLastReport();
        assertEquals(55, timedBalance.time());
        assertEquals(-100d, timedBalance.balance()); //but surely we can because of operations
    }

    @Test
    void testTimeTillEnd() {
        BalanceReportsImpl balanceReports = new BalanceReportsImpl();
        OperationsImpl operations = new OperationsImpl();
        Dashboard dashboard = new DashboardImpl(operations, balanceReports,
                new ReferencePeriodsImpl(Collections.singleton(new ReferencePeriodImpl(0, 100, 1d))));

        assertEquals(0L, dashboard.timeTillPeriodEnd(-1));
        assertEquals(0L, dashboard.timeTillPeriodEnd(100));
        assertEquals(50L, dashboard.timeTillPeriodEnd(50));
    }
}
