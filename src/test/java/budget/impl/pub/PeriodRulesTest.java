package budget.impl.pub;

import budget.api.PeriodRules;
import budget.api.entity.RuleTemplate;
import budget.api.entity.RuleView;
import budget.impl.api.FactoryPeriodRules;
import budget.impl.model.ReferencePeriodsImpl;
import budget.impl.model.entity.ReferencePeriodImpl;
import budget.impl.model.future.ReferencePeriodsFactory;
import budget.impl.model.future.pub.ArithmeticFactory;
import budget.model.ReferencePeriod;
import budget.model.book.PeriodBook;
import budget.model.source.ReferencePeriods;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author Kirill Berezin
 */
class PeriodRulesTest {
    private static class ViewImpl implements RuleView {
        private final String id;

        private ViewImpl(String id) {
            this.id = id;
        }

        @Override
        public @NotNull String id() {
            return id;
        }
    }

    private static class PeriodRulesImpl extends FactoryPeriodRules<ViewImpl, Integer> {

        private PeriodRulesImpl(ReferencePeriods referencePeriods, PeriodBook periodBook) {
            super(referencePeriods, periodBook);
        }

        @Override
        public List<RuleTemplate<ViewImpl, Integer, ReferencePeriodsFactory>> templates() {
            return Collections.singletonList(new RuleTemplateImpl());
        }
    }

    private static class CorruptingPeriods implements ReferencePeriods {
        private final ReferencePeriods decorated;
        private final PeriodBook book;

        private CorruptingPeriods(ReferencePeriods decorated, PeriodBook book) {
            this.decorated = decorated;
            this.book = book;
        }

        @Nullable
        @Override
        public ReferencePeriod periodAtTime(long time) {
            ReferencePeriod period = decorated.periodAtTime(time);
            //CORRUPTION
            book.rewrite(Collections.singletonList(new ReferencePeriodImpl(-1000, 1000, 1d)));
            return period;
        }

        @NotNull
        @Override
        public Collection<ReferencePeriod> periods() {
            return decorated.periods();
        }

        @NotNull
        @Override
        public Collection<ReferencePeriod> range(long from, long to, boolean inclusiveTo) {
            throw new UnsupportedOperationException();
        }

        @NotNull
        @Override
        public ReferencePeriod unite() {
            return decorated.unite();
        }
    }

    private static final class RuleTemplateImpl implements RuleTemplate<ViewImpl, Integer, ReferencePeriodsFactory> {
        @NotNull
        @Override
        public ViewImpl display() {
            return new ViewImpl("dummy");
        }

        @NotNull
        @Override
        public ReferencePeriodsFactory toRule(Integer data) {
            return new ArithmeticFactory(data);
        }
    }

    @SuppressWarnings("ConstantConditions")
    @Test
    void testRewriteMargin() {
        ReferencePeriodsImpl periods = new ReferencePeriodsImpl(Arrays.asList(
                new ReferencePeriodImpl(0, 10, 1d),
                new ReferencePeriodImpl(10, 20, 1d)
        ));

        PeriodRules<ViewImpl, Integer> rules = new PeriodRulesImpl(periods, periods);
        assertEquals(1d, periods.periodAtTime(5).availableMargin());
        assertEquals(1d, periods.periodAtTime(15).availableMargin());

        rules.updatePeriodMargin(-1, 100500d);
        assertEquals(1d, periods.periodAtTime(5).availableMargin());
        assertEquals(1d, periods.periodAtTime(15).availableMargin());

        rules.updatePeriodMargin(1, 100500d);
        assertEquals(100500d, periods.periodAtTime(5).availableMargin());
        assertEquals(1d, periods.periodAtTime(15).availableMargin());

        rules.updatePeriodMargin(19, 9999d);
        assertEquals(100500d, periods.periodAtTime(5).availableMargin());
        assertEquals(9999d, periods.periodAtTime(15).availableMargin());
    }

    @SuppressWarnings("ConstantConditions")
    @Test
    void testRewriteMarginFail() {
        ReferencePeriodsImpl periods = new ReferencePeriodsImpl(Arrays.asList(
                new ReferencePeriodImpl(0, 10, 1d),
                new ReferencePeriodImpl(10, 20, 1d)
        ));

        PeriodRules<ViewImpl, Integer> rules = new PeriodRulesImpl(new CorruptingPeriods(periods, periods), periods);
        assertThrows(IllegalStateException.class, () -> rules.updatePeriodMargin(1, 100500d));
    }

    @Test
    void testAvailableRules() {
        ReferencePeriodsImpl periods = new ReferencePeriodsImpl(Arrays.asList(
                new ReferencePeriodImpl(0, 10, 1d),
                new ReferencePeriodImpl(10, 20, 1d)
        ));

        PeriodRules<ViewImpl, Integer> rules = new PeriodRulesImpl(periods, periods);
        assertEquals(1, rules.templates().size());
        assertEquals("dummy", rules.templates().get(0).display().id());
    }

    @SuppressWarnings("ConstantConditions")
    @Test
    void testApplyNewRule() {
        ReferencePeriodsImpl periods = new ReferencePeriodsImpl(Arrays.asList(
                new ReferencePeriodImpl(0, 15, 1d),
                new ReferencePeriodImpl(15, 27, 1d)
        ));

        PeriodRules<ViewImpl, Integer> rules = new PeriodRulesImpl(periods, periods);
        RuleTemplate<ViewImpl, Integer, ?> stringRuleTemplate = rules.templates().get(0);
        rules.applyNewRuleSince(19, false, stringRuleTemplate.display(), 50);

        assertEquals(5, periods.periods().size());
        assertEquals(27, periods.periodAtTime(29).start());
        assertEquals(30, periods.periodAtTime(29).end());
        assertEquals(0, periods.unite().start());
        assertEquals(50, periods.unite().end());
    }

    @SuppressWarnings("ConstantConditions")
    @Test
    void testApplyNewRuleImmediate() {
        ReferencePeriodsImpl periods = new ReferencePeriodsImpl(Arrays.asList(
                new ReferencePeriodImpl(0, 15, 1d),
                new ReferencePeriodImpl(15, 27, 1d)
        ));

        PeriodRules<ViewImpl, Integer> rules = new PeriodRulesImpl(periods, periods);
        RuleTemplate<ViewImpl, Integer, ?> stringRuleTemplate = rules.templates().get(0);
        rules.applyNewRuleSince(19, true, stringRuleTemplate.display(), 50);

        assertEquals(6, periods.periods().size());
        assertEquals(15, periods.periodAtTime(18).start());
        assertEquals(19, periods.periodAtTime(18).end());
        assertEquals(1d, periods.periodAtTime(18).availableMargin());
        assertEquals(19, periods.periodAtTime(19).start());
        assertEquals(20, periods.periodAtTime(19).end());
        assertEquals(100d, periods.periodAtTime(19).availableMargin());
        assertEquals(100d, periods.periodAtTime(20).availableMargin());
        assertEquals(0, periods.unite().start());
        assertEquals(50, periods.unite().end());
    }

    @Test
    void testInconsistentApplication() {
        ReferencePeriodsImpl periods = new ReferencePeriodsImpl(Arrays.asList(
                new ReferencePeriodImpl(0, 15, 1d),
                new ReferencePeriodImpl(15, 27, 1d)
        ));

        PeriodRules<ViewImpl, Integer> rules = new PeriodRulesImpl(new CorruptingPeriods(periods, periods), periods);
        RuleTemplate<ViewImpl, Integer, ?> stringRuleTemplate = rules.templates().get(0);
        assertThrows(IllegalStateException.class, () -> rules.applyNewRuleSince(25, true, stringRuleTemplate.display(), 33));
    }

    @Test
    void testWrongRule() {
        ReferencePeriodsImpl periods = new ReferencePeriodsImpl(Arrays.asList(
                new ReferencePeriodImpl(0, 15, 1d),
                new ReferencePeriodImpl(15, 27, 1d)
        ));

        PeriodRules<ViewImpl, Integer> rules = new PeriodRulesImpl(periods, periods);
        assertThrows(IllegalArgumentException.class, () -> rules.applyNewRuleSince(25, true, new ViewImpl("baz"), 44));
    }
}
