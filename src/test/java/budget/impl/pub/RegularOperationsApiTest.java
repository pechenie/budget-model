package budget.impl.pub;

import budget.api.RegularOperations;
import budget.api.entity.RecurrenceInput;
import budget.api.entity.RuleTemplate;
import budget.api.entity.RuleView;
import budget.impl.api.RegularOperationsImpl;
import budget.impl.model.BookSchedule;
import budget.impl.model.OperationsImpl;
import budget.impl.model.TestPlannedOpsTagDomain;
import budget.impl.model.notifying.NotifyingOperationBook;
import budget.impl.model.operation.PlannedOperationsImpl;
import budget.impl.model.tag.TagsImpl;
import budget.impl.model.time.DailyIterator;
import budget.impl.model.time.MonthlyIterator;
import budget.impl.model.time.WeeklyIterator;
import budget.model.Operation;
import budget.model.UpdateListener;
import budget.model.book.OperationRecordBook;
import budget.model.source.Operations;
import budget.model.time.TimestampIterator;
import budget.view.operation.RegularOperationItemView;
import budget.view.operation.RegularOperationsView;
import org.jetbrains.annotations.NotNull;
import org.joda.time.LocalDate;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

/**
 * @author Kirill Berezin
 */

class RegularOperationsApiTest {

    private static final double DELTA = 0.0000001;

    @Test
    void test() {
        Operations ops = new OperationsImpl();
        PlannedOperationsImpl plannedOperations = new PlannedOperationsImpl(
                ops, (OperationRecordBook) ops,
                new BookSchedule((OperationRecordBook) ops),
                new TestPlannedOpsTagDomain(), new TagsImpl(new HashMap<>())
        );

        RuleImpl daily = new RuleImpl("daily", new DailyIterator(1));
        RuleImpl weekly = new RuleImpl("weekly", new WeeklyIterator(1));
        RuleImpl monthly = new RuleImpl("monthly", new MonthlyIterator(1));

        RegularOperations<ViewImpl, RecurrenceInput> api = new RegularOperationsImpl<ViewImpl, RecurrenceInput>(plannedOperations) {
            @Override
            public Collection<? extends RuleTemplate<ViewImpl, RecurrenceInput, TimestampIterator>> occurrenceTemplates() {
                return Arrays.asList(daily, weekly, monthly);
            }
        };
        assertEquals(0, api.displayAll().itemsCount());

        api.planSeries("Income", 200d, getMillis(2017, 1, 10), weekly.display(), new InputImpl(3));
        api.planSeries("Credit", -50d, getMillis(2017, 1, 15), daily.display(), new InputImpl(5));
        api.deleteSeries("inn come");

        assertEquals(1, api.displayWithin(getMillis(2017, 1, 14)).itemsCount());

        RegularOperationsView view = api.displayAll();
        assertEquals(2, view.itemsCount());
        assertNull(view.viewAt(-1));
        assertNull(view.viewAt(50));

        RegularOperationItemView viewAt0 = view.viewAt(0);
        assertNotNull(viewAt0);
        assertEquals(3, viewAt0.operationsLeft());
        assertEquals("Income", viewAt0.name());
        assertEquals(getMillis(2017, 1, 10), viewAt0.next().time());
        assertEquals(200d, viewAt0.next().cost(), DELTA);

        RegularOperationItemView viewAt1 = view.viewAt(1);
        assertNotNull(viewAt1);
        assertEquals(5, viewAt1.operationsLeft());
        assertEquals("Credit", viewAt1.name());
        assertEquals(getMillis(2017, 1, 15), viewAt1.next().time());
        assertEquals(-50, viewAt1.next().cost(), DELTA);

        api.planSeries("weird", 1_000_000d, 0, monthly.display(), new InputImpl(1));

        assertEquals(1, api.displayWithin(0).itemsCount());
        view = api.displayAll();
        assertEquals(3, view.itemsCount());

        api.deleteSeries("weird");

        view = api.displayAll();
        assertEquals(2, view.itemsCount());

        Operation nextIncome = ops.operationAt(viewAt0.next().time());
        assertNotNull(nextIncome);
        assertEquals(getMillis(2017, 1, 10), nextIncome.time());
        assertEquals(200d, nextIncome.cost(), DELTA);
        plannedOperations.eraseRecord(nextIncome.time());

        view = api.displayAll();
        viewAt1 = view.viewAt(1);
        assertNotNull(viewAt1);
        assertEquals(2, viewAt1.operationsLeft());
        assertEquals("Income", viewAt1.name());
        assertEquals(getMillis(2017, 1, 17), viewAt1.next().time());
        assertEquals(200d, viewAt1.next().cost(), DELTA);
    }

    @Test
    void testErasingTheRecordWithNotification() {
        ListenerImpl listener = new ListenerImpl();
        OperationsImpl ops = new OperationsImpl();
        PlannedOperationsImpl plannedOperations = new PlannedOperationsImpl(
                ops, new NotifyingOperationBook(listener, ops),
                new BookSchedule((OperationRecordBook) ops),
                new TestPlannedOpsTagDomain(), new TagsImpl(new HashMap<>())
        );

        RuleImpl daily = new RuleImpl("daily", new DailyIterator(1));
        RuleImpl weekly = new RuleImpl("weekly", new WeeklyIterator(1));
        RuleImpl monthly = new RuleImpl("monthly", new MonthlyIterator(1));

        RegularOperations<ViewImpl, RecurrenceInput> api = new RegularOperationsImpl<ViewImpl, RecurrenceInput>(plannedOperations) {
            @Override
            public Collection<? extends RuleTemplate<ViewImpl, RecurrenceInput, TimestampIterator>> occurrenceTemplates() {
                return Arrays.asList(daily, weekly, monthly);
            }
        };
        listener.api = api;
        assertEquals(0, api.displayAll().itemsCount());

        api.planSeries("Income", 200d, 123L, weekly.display(), new InputImpl(1));

        plannedOperations.eraseRecord(123L);

        assertEquals(0, api.displayAll().itemsCount());
    }

    private final static class ListenerImpl implements UpdateListener {
        RegularOperations api;

        @Override
        public void notifyUpdated() {
            api.displayAll();
        }
    }

    @SuppressWarnings("SameParameterValue")
    private long getMillis(int year, int month, int day) {
        return new LocalDate(year, month, day).toDateTimeAtStartOfDay().getMillis();
    }

    private static final class ViewImpl implements RuleView {
        private final String id;

        ViewImpl(String id) {
            this.id = id;
        }

        @NotNull
        @Override
        public String id() {
            return id;
        }
    }

    private static final class InputImpl implements RecurrenceInput {

        private final int count;

        InputImpl(int count) {
            this.count = count;
        }

        @Override
        public int operationsCount() {
            return count;
        }
    }

    private static class RuleImpl implements RuleTemplate<ViewImpl, RecurrenceInput, TimestampIterator> {

        private final ViewImpl view;
        private final TimestampIterator iterator;

        private RuleImpl(String id, TimestampIterator iterator) {
            this.view = new ViewImpl(id);
            this.iterator = iterator;
        }

        @NotNull
        @Override
        public ViewImpl display() {
            return view;
        }

        @NotNull
        @Override
        public TimestampIterator toRule(RecurrenceInput data) {
            return iterator;
        }
    }
}
