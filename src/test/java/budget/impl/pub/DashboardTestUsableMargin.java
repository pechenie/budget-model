package budget.impl.pub;

import budget.api.Dashboard;
import budget.impl.api.DashboardImpl;
import budget.impl.model.BalanceReportsImpl;
import budget.impl.model.OperationsImpl;
import budget.impl.model.ReferencePeriodsImpl;
import budget.impl.model.entity.BalanceReportImpl;
import budget.impl.model.entity.OperationImpl;
import budget.impl.model.entity.ReferencePeriodImpl;
import budget.model.source.BalanceReports;
import budget.model.source.Operations;
import budget.model.source.ReferencePeriods;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

/**
 * @since 15.04.2017
 */
class DashboardTestUsableMargin {
    @Test
    void testEmpty() {
        Operations operations = new OperationsImpl(Collections.emptyList());
        BalanceReports reports = new BalanceReportsImpl(Collections.emptyList());
        ReferencePeriods periods = new ReferencePeriodsImpl(Collections.emptyList());
        Dashboard calc = new DashboardImpl(operations, reports, periods);
        Assertions.assertEquals(0d, calc.usableMarginAtTime(System.currentTimeMillis()));
    }

    @Test
    void testNoPeriods() {
        Operations operations = new OperationsImpl(Collections.emptyList());
        BalanceReports reports = new BalanceReportsImpl(Collections.singleton(new BalanceReportImpl(0, 100d)));
        ReferencePeriods periods = new ReferencePeriodsImpl(Collections.emptyList());
        Dashboard calc = new DashboardImpl(operations, reports, periods);
        Assertions.assertEquals(100d, calc.usableMarginAtTime(System.currentTimeMillis()));
    }

    private static Dashboard dashboard;

    @BeforeAll
    static void prepare() {
        Operations operations = new OperationsImpl(Arrays.asList(
                //initial money, belongs to period 1
                new OperationImpl(0, 100),
                //belongs to period 2
                new OperationImpl(10, 100),
                //belongs to period 3
                new OperationImpl(20, 200),
                //belongs to period 4
                new OperationImpl(30, -100),
                new OperationImpl(40, -100)
        ));
        BalanceReports reports = new BalanceReportsImpl(Arrays.asList(
                //1
                new BalanceReportImpl(1, 100),
                //2
                new BalanceReportImpl(11, 170),
                new BalanceReportImpl(15, 150),
                //3
                new BalanceReportImpl(21, 310),
                new BalanceReportImpl(25, 250),
                //4
                new BalanceReportImpl(35, 140),
                //-
                new BalanceReportImpl(50, 10),
                new BalanceReportImpl(55, 999)
        ));
        ReferencePeriods periods = new ReferencePeriodsImpl(Arrays.asList(
                new ReferencePeriodImpl(0, 10, 30),
                new ReferencePeriodImpl(10, 20, 50),
                new ReferencePeriodImpl(20, 30, 30),
                new ReferencePeriodImpl(30, 50, 50)
        ));
        dashboard = new DashboardImpl(operations, reports, periods);
    }

    @Test
    void testIntraPeriod() {
        printExpectedAndReal(5, "30.0");
        printExpectedAndReal(11, "20.0");
        printExpectedAndReal(15, "0.0");
        printExpectedAndReal(21, "-10.0");
        printExpectedAndReal(26, "-70.0");
        printExpectedAndReal(35, "40.0");

        Assertions.assertEquals(30, dashboard.usableMarginAtTime(5));
        Assertions.assertEquals(20, dashboard.usableMarginAtTime(11));
        Assertions.assertEquals(0, dashboard.usableMarginAtTime(15));
        Assertions.assertEquals(-10, dashboard.usableMarginAtTime(21));
        Assertions.assertEquals(-70, dashboard.usableMarginAtTime(26));
        Assertions.assertEquals(40, dashboard.usableMarginAtTime(35));
    }

    @Test
    void testPeriodStart() {
        printExpectedAndReal(10, "20.0");

        Assertions.assertEquals(50, dashboard.usableMarginAtTime(10));
    }

    @Test
    void testOutOfPeriod() {
        printExpectedAndReal(50, "10.0");
        printExpectedAndReal(51, "10.0");
        printExpectedAndReal(100, "999.0");

        Assertions.assertEquals(10, dashboard.usableMarginAtTime(50));
        Assertions.assertEquals(10, dashboard.usableMarginAtTime(51));
        Assertions.assertEquals(999, dashboard.usableMarginAtTime(100));
    }

    @Test
    void testPeriodStartNoBalanceRecordsOnStart() {
        printExpectedAndReal(0, "30.0"); //NO BALANCE REPORT HERE
        printExpectedAndReal(20, "30.0"); //NO BALANCE REPORT HERE
        printExpectedAndReal(30, "50.0"); //NO BALANCE REPORT HERE

        Assertions.assertEquals(30, dashboard.usableMarginAtTime(0)); //NO BALANCE REPORT HERE
        Assertions.assertEquals(30, dashboard.usableMarginAtTime(20)); //NO BALANCE REPORT HERE
        Assertions.assertEquals(50, dashboard.usableMarginAtTime(30)); //NO BALANCE REPORT HERE
    }

    private static void printExpectedAndReal(long time, String expected) {
        System.out.println("[time = " + time + "] " + expected + " vs " + dashboard.usableMarginAtTime(time));
    }
}
