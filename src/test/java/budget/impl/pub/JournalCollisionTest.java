package budget.impl.pub;

import budget.api.Journal;
import budget.impl.api.JournalImpl;
import budget.impl.model.BalanceReportsImpl;
import budget.impl.model.OperationsImpl;
import budget.impl.model.ReferencePeriodsImpl;
import budget.impl.model.entity.ReferencePeriodImpl;
import budget.model.source.ReferencePeriods;
import budget.view.journal.JournalView;
import budget.view.journal.PeriodGroupView;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @since 20.04.2017
 */
class JournalCollisionTest {
    private Journal journal;

    @BeforeEach
    void prepare() {
        OperationsImpl transactions = new OperationsImpl();
        BalanceReportsImpl reports = new BalanceReportsImpl();
        ReferencePeriods periods = new ReferencePeriodsImpl(Collections.singletonList(new ReferencePeriodImpl(0, 100, 100)));

        journal = new JournalImpl(reports, transactions, periods, reports, transactions);
    }

    @Test
    void testEmpty() {
        JournalView journalView = journal.display();

        assertEquals(1, journalView.groupsCount());
        assertEquals(0, journalView.groupAt(0).itemsCount());
    }

    @Test
    void testCollision() {
        journal.logOperation().writeWithBalance(0, 100, 100);
        journal.logBalance().write(15, 150);
        journal.logOperation().writeWithBalance(50, 200, 300);

        JournalView journalView = journal.display();

        assertEquals(1, journalView.groupsCount());
        PeriodGroupView periodGroupView = journalView.groupAt(0);
        TestCounters counters = new TestCounters();
        CountingGroupVisitorCheckingTime visitor = new CountingGroupVisitorCheckingTime(0, counters,
                new long[]{0, 1, 15, 50, 51});
        assertEquals(5, periodGroupView.itemsCount());
        for (int i = 0; i < periodGroupView.itemsCount(); ++i) {
            periodGroupView.itemAt(i).visitBy(visitor);
        }
        assertEquals(2, counters.transactions);
        assertEquals(3, counters.reports);

        //now duplicate!
        journal.logOperation().writeWithBalance(0, 100, 100);
        journal.logBalance().write(15, 150);
        journal.logOperation().writeWithBalance(50, 200, 300);

        journalView = journal.display();

        assertEquals(1, journalView.groupsCount());
        periodGroupView = journalView.groupAt(0);
        counters = new TestCounters();
        visitor = new CountingGroupVisitorCheckingTime(0, counters, new long[]{0, 1, 2, 3, 15, 16, 50, 51, 52, 53});
        assertEquals(10, periodGroupView.itemsCount());
        for (int i = 0; i < periodGroupView.itemsCount(); ++i) {
            periodGroupView.itemAt(i).visitBy(visitor);
        }
        assertEquals(4, counters.transactions);
        assertEquals(6, counters.reports);
    }
}
