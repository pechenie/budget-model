package budget.impl.pub;

import budget.impl.api.JournalImpl;
import budget.impl.model.BalanceReportsImpl;
import budget.impl.model.OperationsImpl;
import budget.impl.model.ReferencePeriodsImpl;
import budget.impl.model.entity.BalanceReportImpl;
import budget.impl.model.entity.OperationImpl;
import budget.impl.model.entity.ReferencePeriodImpl;
import budget.model.source.BalanceReports;
import budget.model.source.Operations;
import budget.model.source.ReferencePeriods;
import budget.view.journal.JournalView;
import budget.view.journal.PeriodGroupView;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @since 20.04.2017
 */
class HistoryReadOnlyJournalTestWithNegativePL {
    private static JournalView displayedJournal;

    @BeforeAll
    static void prepare() {
        Operations operations = new OperationsImpl(Arrays.asList(
                //initial money, belongs to period 1
                new OperationImpl(0, 100),
                //belongs to period 2
                new OperationImpl(10, 100),
                //belongs to period 3
                new OperationImpl(20, 200),
                //belongs to period 4
                new OperationImpl(30, -100),
                new OperationImpl(40, -50),
                //belongs to period 5
                new OperationImpl(52, -10),
                //belongs to period 6
                new OperationImpl(69, -5)
        ));
        BalanceReports reports = new BalanceReportsImpl(Arrays.asList(
                //period 1
                new BalanceReportImpl(1, 100),
                //period 2
                new BalanceReportImpl(11, 170),
                new BalanceReportImpl(15, 150),
                //period 3
                new BalanceReportImpl(21, 310),
                new BalanceReportImpl(25, 250),
                //period 4
                new BalanceReportImpl(35, 90)
        ));
        ReferencePeriods periods = new ReferencePeriodsImpl(Arrays.asList(
                new ReferencePeriodImpl(0, 10, 30),
                new ReferencePeriodImpl(10, 20, 50),
                new ReferencePeriodImpl(20, 30, 30),
                new ReferencePeriodImpl(30, 50, 50),
                new ReferencePeriodImpl(50, 60, 10),
                new ReferencePeriodImpl(60, 70, 10)
        ));
        displayedJournal = new JournalImpl(reports, operations, periods, null, null).display();
    }

    /*
    JOURNAL IS GOING TO LOOK LIKE THIS:
    --------------------------------
    0 = PERIOD START, BALANCE = 0, PL = 0
    - 0 = TRANSACTION, VALUE = 100, NAME = "INITIAL CASH", BALANCE = 100, PL = 30
    - 1 = REPORT, BALANCE = 100, PL = 30
    10 = PERIOD END, BALANCE = 100, PL = 30
    --------------------------------
    10 = PERIOD START, BALANCE = 100, PL = 50
    - 10 = TRANSACTION, VALUE = 100, NAME = "SALARY", BALANCE = 200, PL = 50
    - 11 = REPORT, BALANCE = 170, PL = 20
    - 15 = REPORT, BALANCE = 150, PL = 0
    20 = PERIOD END, BALANCE = 150, PL = 0
    --------------------------------
    20 = PERIOD START, BALANCE = 150, PL = 30
    - 20 = TRANSACTION, VALUE = 200, NAME = "SALARY", BALANCE = 350, PL = 30
    - 21 = REPORT, BALANCE = 310, PL = -10
    - 25 = REPORT, BALANCE = 250, PL = -70
    30 = PERIOD END, BALANCE = 250, PL = -70
    --------------------------------
    30 = PERIOD START, BALANCE = 250, PL = 50
    - 30 = TRANSACTION, VALUE = -100, NAME = "CREDIT", BALANCE = 150, PL = 50
    - 35 = REPORT, BALANCE = 90, PL = -10
    - 40 = TRANSACTION, VALUE = -50, NAME = "RENT", BALANCE = 40, PL = -10
    50 = PERIOD END, BALANCE = 40, PL = -10
    --------------------------------
    THEN COME "FUTURE" PERIODS (NO REPORTS)
    --------------------------------
    50 = PERIOD START, BALANCE = 40, PL = 10
    - 52 = OPERATION, VALUE = -10, NAME = "RENT", BALANCE = 40, PL = 10
    60 = PERIOD END, BALANCE = 20, PL = 10
    --------------------------------
    60 = PERIOD START, BALANCE = 20, PL = 10
    - 69 = OPERATION, VALUE = -5, NAME = "RENT", BALANCE = 25, PL = 10
    70 = PERIOD END, BALANCE = 5, PL = 10
    --------------------------------
     */
    @Test
    void testPredefinedJournalGroupsCount() {
        assertEquals(6, displayedJournal.groupsCount());
    }

    @Test
    void testPredefinedJournalGroup1Items() {
        PeriodGroupView groupView = displayedJournal.groupAt(0);
        //data
        assertEquals(0, groupView.period().start(), "Group 1, start period");
        assertEquals(10, groupView.period().end(), "Group 1, end period");
        assertEquals(30, groupView.period().availableMargin(), "Group 1, available margin");

        assertEquals(0, groupView.startBalance().balance(), "Group 1, start balance");
        assertEquals(100, groupView.endBalance().balance(), "Group 1, end balance");

        assertEquals(0, groupView.startBalance().margin(), "Group 1, start margin");
        assertEquals(30, groupView.endBalance().margin(), "Group 1, end margin");

        //group items
        assertEquals(2, groupView.itemsCount(), "Group 1, records count");

        GroupVisitorCheckingBalances costVisitor = new GroupVisitorCheckingBalances(1,
                new double[]{100d, Double.NaN},
                new double[]{100d, 100d},
                new double[]{30d, 30d});
        TestCounters counters = new TestCounters();
        for (int i = 0; i < groupView.itemsCount(); ++i) {
            counters = groupView.itemAt(i).visitBy(new SimpleCountingGroupVisitor(1, counters));
            groupView.itemAt(i).visitBy(costVisitor);
        }
        assertEquals(1, counters.transactions, "Group 1, transactions #");
        assertEquals(1, counters.reports, "Group 1, reports #");
    }

    @Test
    void testPredefinedJournalGroup2Items() {
        PeriodGroupView groupView = displayedJournal.groupAt(1);
        //data
        assertEquals(10, groupView.period().start(), "Group 2, start period");
        assertEquals(20, groupView.period().end(), "Group 2, end period");
        assertEquals(50, groupView.period().availableMargin(), "Group 2, available margin");

        assertEquals(100, groupView.startBalance().balance(), "Group 2, start balance");
        assertEquals(150, groupView.endBalance().balance(), "Group 2, end balance");

        assertEquals(50, groupView.startBalance().margin(), "Group 2, start margin");
        assertEquals(0, groupView.endBalance().margin(), "Group 2, end margin");

        //group items
        assertEquals(3, groupView.itemsCount(), "Group 2, items count");

        GroupVisitorCheckingBalances costVisitor = new GroupVisitorCheckingBalances(2,
                new double[]{100d, Double.NaN, Double.NaN},
                new double[]{200d, 170d, 150d},
                new double[]{50, 20, 0});
        TestCounters counters = new TestCounters();
        for (int i = 0; i < groupView.itemsCount(); ++i) {
            counters = groupView.itemAt(i).visitBy(new SimpleCountingGroupVisitor(2, counters));
            groupView.itemAt(i).visitBy(costVisitor);
        }
        assertEquals(1, counters.transactions, "Group 2, transactions #");
        assertEquals(2, counters.reports, "Group 2, reports #");
    }

    @Test
    void testPredefinedJournalGroup3Items() {
        PeriodGroupView groupView = displayedJournal.groupAt(2);
        //data
        assertEquals(20, groupView.period().start(), "Group 3, start period");
        assertEquals(30, groupView.period().end(), "Group 3, end period");
        assertEquals(30, groupView.period().availableMargin(), "Group 3, available margin");

        assertEquals(150, groupView.startBalance().balance(), "Group 3, start balance");
        assertEquals(250, groupView.endBalance().balance(), "Group 3, end balance");

        assertEquals(30, groupView.startBalance().margin(), "Group 3, start margin");
        assertEquals(-70, groupView.endBalance().margin(), "Group 3, end margin");

        //group items
        assertEquals(3, groupView.itemsCount(), "Group 3, items count");

        GroupVisitorCheckingBalances costVisitor = new GroupVisitorCheckingBalances(3,
                new double[]{200d, Double.NaN, Double.NaN},
                new double[]{350d, 310d, 250d},
                new double[]{30, -10, -70});
        TestCounters counters = new TestCounters();
        for (int i = 0; i < groupView.itemsCount(); ++i) {
            counters = groupView.itemAt(i).visitBy(new SimpleCountingGroupVisitor(3, counters));
            groupView.itemAt(i).visitBy(costVisitor);
        }
        assertEquals(1, counters.transactions, "Group 3, transactions #");
        assertEquals(2, counters.reports, "Group 3, reports #");
    }

    @Test
    void testPredefinedJournalGroup4Items() {
        PeriodGroupView groupView = displayedJournal.groupAt(3);
        //data
        assertEquals(30, groupView.period().start(), "Group 4, start period");
        assertEquals(50, groupView.period().end(), "Group 4, end period");
        assertEquals(50, groupView.period().availableMargin(), "Group 4, available margin");

        assertEquals(250, groupView.startBalance().balance(), "Group 4, start balance");
        assertEquals(40, groupView.endBalance().balance(), "Group 4, end balance");

        assertEquals(50, groupView.startBalance().margin(), "Group 4, start margin");
        assertEquals(-10, groupView.endBalance().margin(), "Group 4, end margin");

        //group items
        assertEquals(3, groupView.itemsCount(), "Group 4, items count");

        GroupVisitorCheckingBalances costVisitor = new GroupVisitorCheckingBalances(4,
                new double[]{-100d, Double.NaN, -50d},
                new double[]{150d, 90d, 40d},
                new double[]{50, -10, -10});
        TestCounters counters = new TestCounters();
        for (int i = 0; i < groupView.itemsCount(); ++i) {
            counters = groupView.itemAt(i).visitBy(new SimpleCountingGroupVisitor(4, counters));
            groupView.itemAt(i).visitBy(costVisitor);
        }
        assertEquals(2, counters.transactions, "Group 4, transactions #");
        assertEquals(1, counters.reports, "Group 4, reports #");
    }

    @Test
    void testPredefinedJournalGroup5Items() {
        PeriodGroupView groupView = displayedJournal.groupAt(4);
        //data
        assertEquals(50, groupView.period().start(), "Group 5, start period");
        assertEquals(60, groupView.period().end(), "Group 5, end period");
        assertEquals(10, groupView.period().availableMargin(), "Group 5, available margin");

        assertEquals(40, groupView.startBalance().balance(), "Group 5, start balance");
        assertEquals(30, groupView.endBalance().balance(), "Group 5, end balance");

        assertEquals(10, groupView.startBalance().margin(), "Group 5, start margin");
        assertEquals(10, groupView.endBalance().margin(), "Group 5, end margin");

        //group items
        assertEquals(1, groupView.itemsCount(), "Group 5, items count");

        GroupVisitorCheckingBalances costVisitor = new GroupVisitorCheckingBalances(5,
                new double[]{-10d},
                new double[]{30d},
                new double[]{10d});
        TestCounters counters = new TestCounters();
        for (int i = 0; i < groupView.itemsCount(); ++i) {
            counters = groupView.itemAt(i).visitBy(new SimpleCountingGroupVisitor(5, counters));
            groupView.itemAt(i).visitBy(costVisitor);
        }
        assertEquals(1, counters.transactions, "Group 5, transactions #");
        assertEquals(0, counters.reports, "Group 5, reports #");
    }

    /*
   --------------------------------
   60 = PERIOD START, BALANCE = 20, PL = 10
   - 69 = OPERATION, VALUE = -5, NAME = "RENT", BALANCE = 15, PL = 10
   70 = PERIOD END, BALANCE = 15, PL = 10
   --------------------------------
    */
    @Test
    void testPredefinedJournalGroup6Items() {
        PeriodGroupView groupView = displayedJournal.groupAt(5);
        //data
        assertEquals(60, groupView.period().start(), "Group 6, start period");
        assertEquals(70, groupView.period().end(), "Group 6, end period");
        assertEquals(10, groupView.period().availableMargin(), "Group 6, available margin");

        assertEquals(20, groupView.startBalance().balance(), "Group 6, start balance");
        assertEquals(15, groupView.endBalance().balance(), "Group 6, end balance");

        assertEquals(10, groupView.startBalance().margin(), "Group 6, start margin");
        assertEquals(10, groupView.endBalance().margin(), "Group 6, end margin");

        //group items
        assertEquals(1, groupView.itemsCount(), "Group 6, items count");

        GroupVisitorCheckingBalances costVisitor = new GroupVisitorCheckingBalances(6,
                new double[]{-5d},
                new double[]{15d},
                new double[]{10d});
        TestCounters counters = new TestCounters();
        for (int i = 0; i < groupView.itemsCount(); ++i) {
            counters = groupView.itemAt(i).visitBy(new SimpleCountingGroupVisitor(6, counters));
            groupView.itemAt(i).visitBy(costVisitor);
        }
        assertEquals(1, counters.transactions, "Group 6, transactions #");
        assertEquals(0, counters.reports, "Group 6, reports #");
    }
}
