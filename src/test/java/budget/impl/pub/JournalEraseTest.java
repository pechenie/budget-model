package budget.impl.pub;

import budget.api.Journal;
import budget.impl.api.JournalImpl;
import budget.impl.model.BalanceReportsImpl;
import budget.impl.model.OperationsImpl;
import budget.impl.model.ReferencePeriodsImpl;
import budget.impl.model.entity.ReferencePeriodImpl;
import budget.model.source.ReferencePeriods;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @since 29.04.2017
 */
class JournalEraseTest {
    private Journal journal;

    @BeforeEach
    void prepare() {
        OperationsImpl transactions = new OperationsImpl();
        BalanceReportsImpl reports = new BalanceReportsImpl();
        ReferencePeriods periods = new ReferencePeriodsImpl(Collections.singletonList(new ReferencePeriodImpl(0, 100, 100)));

        journal = new JournalImpl(reports, transactions, periods, reports, transactions);
    }

    @Test
    void testEraseBalance() {
        assertEquals(0, journal.display().groupAt(0).itemsCount());
        journal.logBalance().write(10, 50);
        assertEquals(1, journal.display().groupAt(0).itemsCount());
        journal.eraseRecord().erase(10);
        assertEquals(0, journal.display().groupAt(0).itemsCount());
    }

    @Test
    void testEraseTransaction() {
        assertEquals(0, journal.display().groupAt(0).itemsCount());
        journal.logOperation().writeWithBalance(10, 10, 10);
        assertEquals(2, journal.display().groupAt(0).itemsCount());
        journal.eraseRecord().erase(10);
        assertEquals(1, journal.display().groupAt(0).itemsCount());
        journal.eraseRecord().erase(11);
        assertEquals(0, journal.display().groupAt(0).itemsCount());
    }
}
