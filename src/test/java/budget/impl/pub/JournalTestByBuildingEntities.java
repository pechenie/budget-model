package budget.impl.pub;

import budget.impl.api.JournalImpl;
import budget.impl.model.BalanceReportsImpl;
import budget.impl.model.OperationsImpl;
import budget.impl.model.ReferencePeriodsImpl;
import budget.impl.model.entity.BalanceReportImpl;
import budget.impl.model.entity.OperationImpl;
import budget.impl.model.entity.ReferencePeriodImpl;
import budget.model.BalanceReport;
import budget.model.Operation;
import budget.model.source.ReferencePeriods;
import budget.view.journal.JournalView;
import budget.view.journal.PeriodGroupView;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @since 20.04.2017
 */
class JournalTestByBuildingEntities {
    private static JournalView displayedJournal;

    @BeforeAll
    static void prepare() {
        final List<Operation> operationList = Arrays.asList(
                new OperationImpl(0, 100),
                new OperationImpl(10, 100),
                new OperationImpl(20, 200),
                new OperationImpl(30, -100),
                new OperationImpl(40, -100)
        );
        final List<BalanceReport> reportList = Arrays.asList(
                new BalanceReportImpl(1, 100),
                new BalanceReportImpl(11, 170),
                new BalanceReportImpl(15, 150),
                new BalanceReportImpl(21, 310),
                new BalanceReportImpl(25, 250),
                new BalanceReportImpl(35, 140),
                new BalanceReportImpl(50, 10)
        );

        OperationsImpl transactions = new OperationsImpl();
        operationList.forEach(t -> {
            successfulAdd(t, transactions::execute);
            failedAdd(new OperationImpl(t.time(), 999), transactions::execute);
        });

        BalanceReportsImpl reports = new BalanceReportsImpl();
        reportList.forEach(r -> {
            successfulAdd(r, reports::report);
            failedAdd(new BalanceReportImpl(r.time(), 999), reports::report);
        });

        ReferencePeriods periods = new ReferencePeriodsImpl(Arrays.asList(//
                new ReferencePeriodImpl(0, 10, 30), //
                new ReferencePeriodImpl(10, 20, 50), //
                new ReferencePeriodImpl(20, 30, 30), //
                new ReferencePeriodImpl(30, 50, 50) //
        ));
        displayedJournal = new JournalImpl(reports, transactions, periods, null, null).display();
    }

    private static <T> void successfulAdd(T entity, Function<T, Boolean> addFunc) {
        assertTrue(addFunc.apply(entity));
    }

    private static <T> void failedAdd(T entity, Function<T, Boolean> addFunc) {
        assertFalse(addFunc.apply(entity));
    }

    /*
    JOURNAL IS GOING TO LOOK LIKE THIS:
    --------------------------------
    0 = PERIOD START, BALANCE = 0, PL = 0
    - 0 = TRANSACTION, VALUE = 100, NAME = "INITIAL CASH", BALANCE = 100, PL = 30
    - 1 = REPORT, BALANCE = 100, PL = 30
    10 = PERIOD END, BALANCE = 100, PL = 30
    This period free cash = 30, PL = 30
    --------------------------------
    10 = PERIOD START, BALANCE = 100, PL = 50
    - 10 = TRANSACTION, VALUE = 100, NAME = "SALARY", BALANCE = 200, PL = 50
    - 11 = REPORT, BALANCE = 170, PL = 20
    - 15 = REPORT, BALANCE = 150, PL = 0
    20 = PERIOD END, BALANCE = 150, PL = 0
    This period free cash = 50, PL = 0
    --------------------------------
    20 = PERIOD START, BALANCE = 150, PL = 30
    - 20 = TRANSACTION, VALUE = 200, NAME = "SALARY", BALANCE = 350, PL = 30
    - 21 = REPORT, BALANCE = 310, PL = -10
    - 25 = REPORT, BALANCE = 250, PL = -70
    30 = PERIOD END, BALANCE = 250, PL = -70
    This period free cash = 30, PL = -70
    --------------------------------
    30 = PERIOD START, BALANCE = 250, PL = 50
    - 30 = TRANSACTION, VALUE = -100, NAME = "CREDIT", BALANCE = 150, PL = 50
    - 35 = REPORT, BALANCE = 140, PL = 40
    - 40 = TRANSACTION, VALUE = -100, NAME = "RENT", BALANCE = 40, PL = 40
    50 = PERIOD END, BALANCE = 40, PL = 40
    This period free cash = 50, PL = 40
    --------------------------------
     */
    @Test
    void testPredefinedJournalGroupsCount() {
        assertEquals(4, displayedJournal.groupsCount());
    }

    @Test
    void testPredefinedJournalGroup1Items() {
        PeriodGroupView groupView = displayedJournal.groupAt(0);
        //data
        assertEquals(0, groupView.period().start(), "Group 1, start period");
        assertEquals(10, groupView.period().end(), "Group 1, end period");
        assertEquals(30, groupView.period().availableMargin(), "Group 1, available margin");

        assertEquals(0, groupView.startBalance().balance(), "Group 1, start balance");
        assertEquals(100, groupView.endBalance().balance(), "Group 1, end balance");

        assertEquals(0, groupView.startBalance().margin(), "Group 1, start margin");
        assertEquals(30, groupView.endBalance().margin(), "Group 1, end margin");

        //group items
        assertEquals(2, groupView.itemsCount(), "Group 1, records count");

        GroupVisitorCheckingBalances costVisitor = new GroupVisitorCheckingBalances(1,
                new double[]{100d, Double.NaN},
                new double[]{100d, 100d},
                new double[]{30d, 30d});
        TestCounters counters = new TestCounters();
        for (int i = 0; i < groupView.itemsCount(); ++i) {
            counters = groupView.itemAt(i).visitBy(new SimpleCountingGroupVisitor(1, counters));
            groupView.itemAt(i).visitBy(costVisitor);
        }
        assertEquals(1, counters.transactions, "Group 1, transactions #");
        assertEquals(1, counters.reports, "Group 1, reports #");
    }

    @Test
    void testPredefinedJournalGroup2Items() {
        PeriodGroupView groupView = displayedJournal.groupAt(1);
        //data
        assertEquals(10, groupView.period().start(), "Group 2, start period");
        assertEquals(20, groupView.period().end(), "Group 2, end period");
        assertEquals(50, groupView.period().availableMargin(), "Group 2, available margin");

        assertEquals(100, groupView.startBalance().balance(), "Group 2, start balance");
        assertEquals(150, groupView.endBalance().balance(), "Group 2, end balance");

        assertEquals(50, groupView.startBalance().margin(), "Group 2, start margin");
        assertEquals(0, groupView.endBalance().margin(), "Group 2, end margin");

        //group items
        assertEquals(3, groupView.itemsCount(), "Group 2, items count");
        GroupVisitorCheckingBalances costVisitor = new GroupVisitorCheckingBalances(2,
                new double[]{100d, Double.NaN, Double.NaN},
                new double[]{200d, 170d, 150d},
                new double[]{50, 20, 0});
        TestCounters counters = new TestCounters();
        for (int i = 0; i < groupView.itemsCount(); ++i) {
            counters = groupView.itemAt(i).visitBy(new SimpleCountingGroupVisitor(2, counters));
            groupView.itemAt(i).visitBy(costVisitor);
        }
        assertEquals(1, counters.transactions, "Group 2, transactions #");
        assertEquals(2, counters.reports, "Group 2, reports #");
    }

    @Test
    void testPredefinedJournalGroup3Items() {
        PeriodGroupView groupView = displayedJournal.groupAt(2);
        //data
        assertEquals(20, groupView.period().start(), "Group 3, start period");
        assertEquals(30, groupView.period().end(), "Group 3, end period");
        assertEquals(30, groupView.period().availableMargin(), "Group 3, available margin");

        assertEquals(150, groupView.startBalance().balance(), "Group 3, start balance");
        assertEquals(250, groupView.endBalance().balance(), "Group 3, end balance");

        assertEquals(30, groupView.startBalance().margin(), "Group 3, start margin");
        assertEquals(-70, groupView.endBalance().margin(), "Group 3, end margin");

        //group items
        assertEquals(3, groupView.itemsCount(), "Group 3, items count");
        GroupVisitorCheckingBalances costVisitor = new GroupVisitorCheckingBalances(3,
                new double[]{200d, Double.NaN, Double.NaN},
                new double[]{350d, 310d, 250d},
                new double[]{30, -10, -70});
        TestCounters counters = new TestCounters();
        for (int i = 0; i < groupView.itemsCount(); ++i) {
            counters = groupView.itemAt(i).visitBy(new SimpleCountingGroupVisitor(3, counters));
            groupView.itemAt(i).visitBy(costVisitor);
        }
        assertEquals(1, counters.transactions, "Group 3, transactions #");
        assertEquals(2, counters.reports, "Group 3, reports #");
    }

    @Test
    void testPredefinedJournalGroup4Items() {
        PeriodGroupView groupView = displayedJournal.groupAt(3);
        //data
        assertEquals(30, groupView.period().start(), "Group 4, start period");
        assertEquals(50, groupView.period().end(), "Group 4, end period");
        assertEquals(50, groupView.period().availableMargin(), "Group 4, available margin");

        assertEquals(250, groupView.startBalance().balance(), "Group 4, start balance");
        assertEquals(40, groupView.endBalance().balance(), "Group 4, end balance");

        assertEquals(50, groupView.startBalance().margin(), "Group 4, start margin");
        assertEquals(40, groupView.endBalance().margin(), "Group 4, end margin");

        //group items
        assertEquals(3, groupView.itemsCount(), "Group 4, items count");
        GroupVisitorCheckingBalances costVisitor = new GroupVisitorCheckingBalances(4,
                new double[]{-100d, Double.NaN, -100d},
                new double[]{150d, 140d, 40d},
                new double[]{50, 40, 40});
        TestCounters counters = new TestCounters();
        for (int i = 0; i < groupView.itemsCount(); ++i) {
            counters = groupView.itemAt(i).visitBy(new SimpleCountingGroupVisitor(4, counters));
            groupView.itemAt(i).visitBy(costVisitor);
        }
        assertEquals(2, counters.transactions, "Group 4, transactions #");
        assertEquals(1, counters.reports, "Group 4, reports #");
    }
}
