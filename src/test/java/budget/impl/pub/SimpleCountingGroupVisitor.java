package budget.impl.pub;

import budget.view.journal.GroupItem;

/**
 * @since 29.04.2017
 */
class SimpleCountingGroupVisitor extends AbstractCountingGroupVisitor<Object> {
    SimpleCountingGroupVisitor(int groupId, TestCounters counters) {
        super(groupId, counters, null);
    }

    @Override
    protected void checkItem(int groupId, int index, Object o, GroupItem item) {
        //no op
    }
}
