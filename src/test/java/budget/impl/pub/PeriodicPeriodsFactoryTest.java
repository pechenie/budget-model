package budget.impl.pub;

import budget.impl.model.ReferencePeriodsImpl;
import budget.impl.model.period.CalendarLimit;
import budget.impl.model.period.PeriodicReferencePeriodFactory;
import budget.impl.model.time.StartOfWeekIterator;
import budget.model.ReferencePeriod;
import budget.model.source.ReferencePeriods;
import org.joda.time.LocalDate;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Kirill Berezin
 */

class PeriodicPeriodsFactoryTest {
    @Test
    void test() {
        long startMillis = new LocalDate(2017, 5, 3).toDateTimeAtStartOfDay().getMillis();
        ReferencePeriods referencePeriods = new ReferencePeriodsImpl(
                new PeriodicReferencePeriodFactory(
                        new StartOfWeekIterator(1), 100d, from -> new CalendarLimit(new LocalDate(from).plusWeeks(19))
                ).calculateWithStartingTime(startMillis));

        assertEquals(20, referencePeriods.periods().size());
        assertEquals(100d, (double) referencePeriods.periods().stream()
                .findAny().map(ReferencePeriod::availableMargin).orElse(Double.NaN));

        assertEquals(startMillis, referencePeriods.unite().start());
        long endMillis = new LocalDate(startMillis).plusWeeks(20).withDayOfWeek(1).toDateTimeAtStartOfDay().getMillis();
        assertEquals(endMillis, referencePeriods.unite().end());
    }
}
