package budget.impl.pub;

import budget.impl.model.BalanceReportsImpl;
import budget.impl.model.OperationsImpl;
import budget.impl.model.ReferencePeriodsImpl;
import budget.impl.model.entity.BalanceReportImpl;
import budget.impl.model.entity.OperationImpl;
import budget.impl.model.metrics.TimedMetricsImpl;
import budget.model.TimedMetrics;
import budget.view.TimedBalance;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @since 30.04.2017
 */
class MinBalanceTest {
    @Test
    void testEmpty() {
        OperationsImpl operations = new OperationsImpl();
        BalanceReportsImpl balanceReports = new BalanceReportsImpl();
        TimedMetrics metrics = new TimedMetricsImpl(operations, balanceReports, new ReferencePeriodsImpl(Collections.emptyList()));
        TimedBalance timedBalance = metrics.minBalanceAtRange(-1000, 1000);
        assertEquals(0, timedBalance.balance());
        assertEquals(-1000, timedBalance.time());
    }

    @Test
    void testDecreasingBalance() {
        BalanceReportsImpl balanceReports = new BalanceReportsImpl();
        assertTrue(balanceReports.report(new BalanceReportImpl(0, 1000)));
        OperationsImpl operations = new OperationsImpl();
        operations.execute(new OperationImpl(10, -100));
        operations.execute(new OperationImpl(20, -100));
        operations.execute(new OperationImpl(30, -100));
        operations.execute(new OperationImpl(40, -100));
        operations.execute(new OperationImpl(50, -100));

        TimedMetrics metrics = new TimedMetricsImpl(operations, balanceReports, new ReferencePeriodsImpl(Collections.emptyList()));
        for (int i = 0; i <= 50; ++i) {
            TimedBalance timedBalance = metrics.minBalanceAtRange(i, 50);
            assertEquals(500, timedBalance.balance());
            assertEquals(50, timedBalance.time());
        }
    }

    @Test
    void testDecreasingBalanceWithReportInTheEnd2() {
        BalanceReportsImpl balanceReports = new BalanceReportsImpl();
        assertTrue(balanceReports.report(new BalanceReportImpl(0, 1000)));
        assertTrue(balanceReports.report(new BalanceReportImpl(50, 100)));
        OperationsImpl operations = new OperationsImpl();
        operations.execute(new OperationImpl(10, -100));
        operations.execute(new OperationImpl(20, -100));
        operations.execute(new OperationImpl(30, -100));
        operations.execute(new OperationImpl(40, -100));

        TimedMetrics metrics = new TimedMetricsImpl(operations, balanceReports, new ReferencePeriodsImpl(Collections.emptyList()));
        for (int i = 0; i <= 50; ++i) {
            TimedBalance timedBalance = metrics.minBalanceAtRange(i, 50);
            assertEquals(100, timedBalance.balance(), "Min Balance From " + i);
            assertEquals(50, timedBalance.time(), "Min Time " + i);
        }
    }

    @Test
    void testDecreasingBalanceWithReportInTheEnd() {
        BalanceReportsImpl balanceReports = new BalanceReportsImpl();
        balanceReports.report(new BalanceReportImpl(0, 1000));
        balanceReports.report(new BalanceReportImpl(49, 900));
        OperationsImpl operations = new OperationsImpl();
        operations.execute(new OperationImpl(10, -100));
        operations.execute(new OperationImpl(20, -100));
        operations.execute(new OperationImpl(30, -100));
        operations.execute(new OperationImpl(40, -100));
        operations.execute(new OperationImpl(50, -100));

        TimedMetrics metrics = new TimedMetricsImpl(operations, balanceReports, new ReferencePeriodsImpl(Collections.emptyList()));
        for (int i = 0; i <= 40; ++i) {
            TimedBalance timedBalance = metrics.minBalanceAtRange(i, 50);
            assertEquals(600, timedBalance.balance());
            assertEquals(40, timedBalance.time());
        }
        for (int i = 41; i < 49; ++i) {
            TimedBalance timedBalance = metrics.minBalanceAtRange(i, 50);
            assertEquals(600, timedBalance.balance());
            assertEquals(i, timedBalance.time());
        }
        for (int i = 49; i <= 50; ++i) {
            TimedBalance timedBalance = metrics.minBalanceAtRange(i, 50);
            assertEquals(800, timedBalance.balance());
            assertEquals(50, timedBalance.time());
        }
    }

    @Test
    void testDecreasingBalanceWithWeirdReports() {
        BalanceReportsImpl balanceReports = new BalanceReportsImpl();
        balanceReports.report(new BalanceReportImpl(0, 1000));
        balanceReports.report(new BalanceReportImpl(1, 900));
        balanceReports.report(new BalanceReportImpl(11, 220));
        balanceReports.report(new BalanceReportImpl(33, 130));
        balanceReports.report(new BalanceReportImpl(41, 110));
        balanceReports.report(new BalanceReportImpl(51, 1000));
        OperationsImpl operations = new OperationsImpl();
        operations.execute(new OperationImpl(10, -100));
        operations.execute(new OperationImpl(20, -100));
        operations.execute(new OperationImpl(30, -100));
        operations.execute(new OperationImpl(40, -100));
        operations.execute(new OperationImpl(50, -100));

        TimedMetrics metrics = new TimedMetricsImpl(operations, balanceReports, new ReferencePeriodsImpl(Collections.emptyList()));
        for (int i = 0; i <= 50; ++i) {
            TimedBalance timedBalance = metrics.minBalanceAtRange(i, 50);
            assertEquals(10, timedBalance.balance());
            assertEquals(50, timedBalance.time());
        }
    }

    @Test
    void testIncreasingBalance() {
        BalanceReportsImpl balanceReports = new BalanceReportsImpl();
        balanceReports.report(new BalanceReportImpl(0, 1000));
        OperationsImpl operations = new OperationsImpl();
        operations.execute(new OperationImpl(10, 100));
        operations.execute(new OperationImpl(20, 100));
        operations.execute(new OperationImpl(30, 100));

        TimedMetrics metrics = new TimedMetricsImpl(operations, balanceReports, new ReferencePeriodsImpl(Collections.emptyList()));
        for (int i = 0; i < 10; ++i) {
            TimedBalance timedBalance = metrics.minBalanceAtRange(i, 50);
            assertEquals(1000, timedBalance.balance());
            assertEquals(i, timedBalance.time());
        }
        for (int i = 10; i < 20; ++i) {
            TimedBalance timedBalance = metrics.minBalanceAtRange(i, 50);
            assertEquals(1100, timedBalance.balance());
            assertEquals(i, timedBalance.time());
        }
        for (int i = 20; i < 30; ++i) {
            TimedBalance timedBalance = metrics.minBalanceAtRange(i, 50);
            assertEquals(1200, timedBalance.balance());
            assertEquals(i, timedBalance.time());
        }
    }

    @Test
    void testIncreasingBalance2() {
        BalanceReportsImpl balanceReports = new BalanceReportsImpl();
        balanceReports.report(new BalanceReportImpl(0, 1000));
        balanceReports.report(new BalanceReportImpl(25, 100));
        OperationsImpl operations = new OperationsImpl();
        operations.execute(new OperationImpl(10, 100));
        operations.execute(new OperationImpl(20, 100));
        operations.execute(new OperationImpl(30, 100));
        operations.execute(new OperationImpl(40, 100));
        operations.execute(new OperationImpl(50, 100));

        TimedMetrics metrics = new TimedMetricsImpl(operations, balanceReports, new ReferencePeriodsImpl(Collections.emptyList()));
        for (int i = 0; i <= 50; ++i) {
            TimedBalance timedBalance = metrics.minBalanceAtRange(0, 50);
            assertEquals(100, timedBalance.balance());
            assertEquals(25, timedBalance.time());
        }
    }

}
