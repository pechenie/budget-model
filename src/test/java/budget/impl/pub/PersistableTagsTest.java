package budget.impl.pub;

import budget.impl.model.persistence.TagsPersistence;
import budget.impl.model.tag.PersistableTags;
import budget.impl.model.tag.TagImpl;
import budget.model.tag.Tag;
import budget.model.tag.Tags;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.HashMap;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Kirill Berezin
 */

class PersistableTagsTest {
    private final class TagsPersistenceImpl implements TagsPersistence {
        private final HashMap<Long, Tag> tags = new HashMap<>();

        @Override
        public void store(@NotNull Tag tag) {
            //copy
            Tag newTag = new TagImpl(tag.key(), tag.allKeys().stream().collect(
                    Collectors.toMap(Function.identity(), tag::valueFor)));
            tags.put(tag.key(), newTag);
        }

        @Override
        public void remove(long recordAtTime) {
            tags.remove(recordAtTime);
        }

        @Override
        public @NotNull Collection<? extends Tag> readTags() {
            return tags.values();
        }
    }

    @Test
    void test() {
        TagsPersistenceImpl tagStorage = new TagsPersistenceImpl();
        Tags tags = new PersistableTags(tagStorage);

        assertEquals(0, tags.tags().size());
        assertNotNull(tags.tagBy(1234));

        {
            Tag tag = tags.tagBy(999).withEntry("foo", "bar").withEntry("qux", "1000");
            assertEquals(999, tag.key());
            assertTrue(tag.hasEntry("foo"));
            assertEquals("bar", tag.valueFor("foo"));
            assertEquals(1000, tag.longValueFor("qux"));
            tags.update(tag);
            assertEquals(1, tags.tags().size());

            tag = tags.tagBy(1).withEntry("abc", "def").withEntry("xyz", "500");
            assertEquals(1, tag.key());
            assertTrue(tag.hasEntry("abc"));
            assertEquals("def", tag.valueFor("abc"));
            assertEquals(500, tag.longValueFor("xyz"));
            tags.update(tag);
            assertEquals(2, tags.tags().size());

            tags.update(tags.tagBy(1000000L)); //won't be saved - it's empty
            assertEquals(3, tags.tags().size());

            tags.update(tags.tagBy(1000001L)); //won't be saved - it's empty
            assertEquals(4, tags.tags().size());

            tags.remove(1000001L);
        }

        tags = new PersistableTags(tagStorage);

        {
            Tag tag = tags.tagBy(999);
            assertEquals(999, tag.key());
            assertTrue(tag.hasEntry("foo"));
            assertEquals("bar", tag.valueFor("foo"));
            assertEquals(1000, tag.longValueFor("qux"));
            assertNotSame(tag, tag.removeEntry("foo"));
            assertSame(tag, tag.removeEntry("non-existent"));
            tags.update(tag);
            assertEquals(2, tags.tags().size());

            tag = tags.tagBy(1);
            assertEquals(1, tag.key());
            assertTrue(tag.hasEntry("abc"));
            assertEquals("def", tag.valueFor("abc"));
            assertEquals(500, tag.longValueFor("xyz"));
            tags.update(tag);
            assertEquals(2, tags.tags().size());
        }
    }
}
