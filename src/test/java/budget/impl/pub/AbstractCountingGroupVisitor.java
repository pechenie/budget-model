package budget.impl.pub;

import budget.view.journal.BalanceReportView;
import budget.view.journal.GroupItem;
import budget.view.journal.GroupItemVisitor;
import budget.view.journal.OperationView;
import org.jetbrains.annotations.NotNull;

/**
 * @since 29.04.2017
 */
abstract class AbstractCountingGroupVisitor<DATA> implements GroupItemVisitor<TestCounters> {
    private final int groupId;
    private final TestCounters counters;
    private final DATA data;

    AbstractCountingGroupVisitor(int groupId, TestCounters counters, DATA data) {
        this.groupId = groupId;
        this.counters = counters;
        this.data = data;
    }

    protected abstract void checkItem(int groupId, int index, DATA data, GroupItem item);

    @Override
    public TestCounters onOperation(@NotNull OperationView item) {
        checkItem(groupId, counters.index(), data, item);

        counters.transactions++;
        return counters;
    }

    @Override
    public TestCounters onBalanceReport(@NotNull BalanceReportView item) {
        checkItem(groupId, counters.index(), data, item);

        counters.reports++;
        return counters;
    }
}
