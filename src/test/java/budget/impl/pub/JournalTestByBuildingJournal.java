package budget.impl.pub;

import budget.impl.api.JournalImpl;
import budget.impl.model.BalanceReportsImpl;
import budget.impl.model.OperationsImpl;
import budget.impl.model.ReferencePeriodsImpl;
import budget.impl.model.entity.ReferencePeriodImpl;
import budget.model.source.ReferencePeriods;
import budget.view.journal.JournalView;
import budget.view.journal.PeriodGroupView;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @since 20.04.2017
 */
class JournalTestByBuildingJournal {
    private static JournalView displayedJournal;

    @BeforeAll
    static void prepare() {
        OperationsImpl transactions = new OperationsImpl();
        BalanceReportsImpl reports = new BalanceReportsImpl();
        ReferencePeriods periods = new ReferencePeriodsImpl(Arrays.asList(//
                new ReferencePeriodImpl(0, 10, 30), //
                new ReferencePeriodImpl(10, 20, 50), //
                new ReferencePeriodImpl(20, 30, 30), //
                new ReferencePeriodImpl(30, 50, 50) //
        ));

        JournalImpl journal = new JournalImpl(reports, transactions, periods, reports, transactions);
        journal.logOperation().writeWithBalance(0, 100, 100);
        journal.logOperation().writeWithBalance(10, 100, 170);
        journal.logBalance().write(15, 150);
        journal.logOperation().writeWithBalance(20, 200, 310);
        journal.logBalance().write(25, 250);
        journal.logOperation().writeWithBalance(30, -100, 150);
        journal.logBalance().write(35, 140);
        journal.logOperation().writeWithBalance(40, -100, 40);
        journal.logBalance().write(50, 10);

        displayedJournal = journal.display();
    }

    @Test
    void testPredefinedJournalGroupsCount() {
        assertEquals(4, displayedJournal.groupsCount());
    }

    @Test
    void testPredefinedJournalGroup1Items() {
        PeriodGroupView groupView = displayedJournal.groupAt(0);
        //data
        assertEquals(0, groupView.period().start(), "Group 1, start period");
        assertEquals(10, groupView.period().end(), "Group 1, end period");
        assertEquals(30, groupView.period().availableMargin(), "Group 1, available margin");

        assertEquals(0, groupView.startBalance().balance(), "Group 1, start balance");
        assertEquals(100, groupView.endBalance().balance(), "Group 1, end balance");

        assertEquals(0, groupView.startBalance().margin(), "Group 1, start margin");
        assertEquals(30, groupView.endBalance().margin(), "Group 1, end margin");

        //group items
        assertEquals(2, groupView.itemsCount(), "Group 1, records count");

        GroupVisitorCheckingBalances costVisitor = new GroupVisitorCheckingBalances(1,
                new double[]{100d, Double.NaN},
                new double[]{100d, 100d},
                new double[]{30d, 30d});
        TestCounters counters = new TestCounters();
        for (int i = 0; i < groupView.itemsCount(); ++i) {
            counters = groupView.itemAt(i).visitBy(new SimpleCountingGroupVisitor(1, counters));
            groupView.itemAt(i).visitBy(costVisitor);
        }
        assertEquals(1, counters.transactions, "Group 1, transactions #");
        assertEquals(1, counters.reports, "Group 1, reports #");
    }

    @Test
    void testPredefinedJournalGroup2Items() {
        PeriodGroupView groupView = displayedJournal.groupAt(1);
        //data
        assertEquals(10, groupView.period().start(), "Group 2, start period");
        assertEquals(20, groupView.period().end(), "Group 2, end period");
        assertEquals(50, groupView.period().availableMargin(), "Group 2, available margin");

        assertEquals(100, groupView.startBalance().balance(), "Group 2, start balance");
        assertEquals(150, groupView.endBalance().balance(), "Group 2, end balance");

        assertEquals(50, groupView.startBalance().margin(), "Group 2, start margin");
        assertEquals(0, groupView.endBalance().margin(), "Group 2, end margin");

        //group items
        assertEquals(3, groupView.itemsCount(), "Group 2, items count");
        GroupVisitorCheckingBalances costVisitor = new GroupVisitorCheckingBalances(2,
                new double[]{100d, Double.NaN, Double.NaN},
                new double[]{200d, 170d, 150d},
                new double[]{50, 20, 0});
        TestCounters counters = new TestCounters();
        for (int i = 0; i < groupView.itemsCount(); ++i) {
            counters = groupView.itemAt(i).visitBy(new SimpleCountingGroupVisitor(2, counters));
            groupView.itemAt(i).visitBy(costVisitor);
        }
        assertEquals(1, counters.transactions, "Group 2, transactions #");
        assertEquals(2, counters.reports, "Group 2, reports #");
    }

    @Test
    void testPredefinedJournalGroup3Items() {
        PeriodGroupView groupView = displayedJournal.groupAt(2);
        //data
        assertEquals(20, groupView.period().start(), "Group 3, start period");
        assertEquals(30, groupView.period().end(), "Group 3, end period");
        assertEquals(30, groupView.period().availableMargin(), "Group 3, available margin");

        assertEquals(150, groupView.startBalance().balance(), "Group 3, start balance");
        assertEquals(250, groupView.endBalance().balance(), "Group 3, end balance");

        assertEquals(30, groupView.startBalance().margin(), "Group 3, start margin");
        assertEquals(-70, groupView.endBalance().margin(), "Group 3, end margin");

        //group items
        assertEquals(3, groupView.itemsCount(), "Group 3, items count");
        GroupVisitorCheckingBalances costVisitor = new GroupVisitorCheckingBalances(3,
                new double[]{200d, Double.NaN, Double.NaN},
                new double[]{350d, 310d, 250d},
                new double[]{30, -10, -70});
        TestCounters counters = new TestCounters();
        for (int i = 0; i < groupView.itemsCount(); ++i) {
            counters = groupView.itemAt(i).visitBy(new SimpleCountingGroupVisitor(3, counters));
            groupView.itemAt(i).visitBy(costVisitor);
        }
        assertEquals(1, counters.transactions, "Group 3, transactions #");
        assertEquals(2, counters.reports, "Group 3, reports #");
    }

    @Test
    void testPredefinedJournalGroup4Items() {
        PeriodGroupView groupView = displayedJournal.groupAt(3);
        //data
        assertEquals(30, groupView.period().start(), "Group 4, start period");
        assertEquals(50, groupView.period().end(), "Group 4, end period");
        assertEquals(50, groupView.period().availableMargin(), "Group 4, available margin");

        assertEquals(250, groupView.startBalance().balance(), "Group 4, start balance");
        assertEquals(40, groupView.endBalance().balance(), "Group 4, end balance");

        assertEquals(50, groupView.startBalance().margin(), "Group 4, start margin");
        assertEquals(40, groupView.endBalance().margin(), "Group 4, end margin");

        //group items
        assertEquals(5, groupView.itemsCount(), "Group 4, items count");
        GroupVisitorCheckingBalances costVisitor = new GroupVisitorCheckingBalances(4,
                new double[]{-100d, Double.NaN, Double.NaN, -100d, Double.NaN},
                new double[]{150d, 150d, 140d, 40d, 40d},
                new double[]{50, 50, 40, 40, 40});
        TestCounters counters = new TestCounters();
        for (int i = 0; i < groupView.itemsCount(); ++i) {
            counters = groupView.itemAt(i).visitBy(new SimpleCountingGroupVisitor(4, counters));
            groupView.itemAt(i).visitBy(costVisitor);
        }
        assertEquals(2, counters.transactions, "Group 4, transactions #");
        assertEquals(3, counters.reports, "Group 4, reports #");
    }

    @Test
    void testLookup() {
        assertEquals(0, displayedJournal.groupIndexAtTime(-10));
        assertEquals(0, displayedJournal.groupIndexAtTime(0));
        assertEquals(0, displayedJournal.groupIndexAtTime(9));
        assertEquals(1, displayedJournal.groupIndexAtTime(10));
        assertEquals(2, displayedJournal.groupIndexAtTime(25));
        assertEquals(3, displayedJournal.groupIndexAtTime(30));
        assertEquals(3, displayedJournal.groupIndexAtTime(50));
        assertEquals(3, displayedJournal.groupIndexAtTime(9999));
    }
}
