package budget.impl.model;

import budget.impl.model.entity.OperationImpl;
import budget.model.source.Operations;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @since 29.04.2017
 */
class OperationsTest {
    private static Operations operations;

    @BeforeAll
    static void prepare() {
        operations = new OperationsImpl(Arrays.asList(
                new OperationImpl(100, 100),
                new OperationImpl(200, 100),
                new OperationImpl(300, 100)
        ));
    }

    @SuppressWarnings("ConstantConditions")
    @Test
    void testOperationAt() {
        assertNotNull(operations.operationAt(100));
        assertNotNull(operations.operationAt(200));
        assertNotNull(operations.operationAt(300));
        assertEquals(100, operations.operationAt(100).time());
        assertEquals(100, operations.operationAt(100).cost());
        assertEquals(200, operations.operationAt(200).time());
        assertEquals(100, operations.operationAt(100).cost());
        assertEquals(300, operations.operationAt(300).time());
        assertEquals(100, operations.operationAt(100).cost());

        assertNull(operations.operationAt(99));
        assertNull(operations.operationAt(199));
        assertNull(operations.operationAt(399));
        assertNull(operations.operationAt(101));
        assertNull(operations.operationAt(201));
        assertNull(operations.operationAt(301));
        assertNull(operations.operationAt(Long.MIN_VALUE));
        assertNull(operations.operationAt(Long.MAX_VALUE));
        assertNull(operations.operationAt(0));
    }

    @Test
    void operationsTestCount() {
        assertEquals(3, operations.range(0, true, 1000, true).size());
        assertEquals(0, operations.range(0, true, 0, true).size());
        assertEquals(0, operations.range(1000, true, 0, true).size());
        assertEquals(3, operations.range(100, true, 300, true).size());

        assertEquals(3, operations.range(100, true, 301, false).size());
        assertEquals(2, operations.range(100, true, 300, false).size());
        assertEquals(1, operations.range(100, true, 200, false).size());

        assertEquals(0, operations.range(1000, false, 0, true).size());
        assertEquals(2, operations.range(100, false, 300, true).size());
        assertEquals(2, operations.range(100, false, 301, false).size());
        assertEquals(1, operations.range(100, false, 300, false).size());
        assertEquals(0, operations.range(100, false, 200, false).size());
    }

    @Test
    void operationsTestSum() {
        assertEquals(300, operations.operationSum(0, 1000, true));
        assertEquals(0, operations.operationSum(0, 0, true));
        assertEquals(0, operations.operationSum(1000, 0, true));
        assertEquals(300, operations.operationSum(100, 300, true));
        assertEquals(300, operations.operationSum(100, 301, false));
        assertEquals(200, operations.operationSum(100, 300, false));
        assertEquals(100, operations.operationSum(100, 200, false));
    }
}
