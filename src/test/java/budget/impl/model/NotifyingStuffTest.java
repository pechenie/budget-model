package budget.impl.model;

import budget.impl.model.entity.BalanceReportImpl;
import budget.impl.model.entity.OperationImpl;
import budget.impl.model.entity.ReferencePeriodImpl;
import budget.impl.model.notifying.NotifyingBalanceBook;
import budget.impl.model.notifying.NotifyingOperationBook;
import budget.impl.model.notifying.NotifyingPeriodBook;
import budget.impl.model.notifying.NotifyingPlannedOperations;
import budget.impl.model.operation.PlannedOperationSeriesImpl;
import budget.impl.model.operation.PlannedOperationsImpl;
import budget.impl.model.tag.TagsImpl;
import budget.impl.model.time.ConstantIterator;
import budget.model.UpdateListener;
import budget.model.book.BalanceRecordBook;
import budget.model.book.OperationRecordBook;
import budget.model.book.PeriodBook;
import budget.model.operation.PlannedOperations;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Kirill Berezin
 */

class NotifyingStuffTest {
    private static class CountingUpdateListener implements UpdateListener {
        int count;

        @Override
        public void notifyUpdated() {
            count++;
        }
    }

    @Test
    void testNotifyingBalanceBook() {
        CountingUpdateListener listener = new CountingUpdateListener();
        BalanceRecordBook book = new NotifyingBalanceBook(listener, new BalanceReportsImpl());

        assertTrue(book.report(new BalanceReportImpl(10, 10d)));
        assertFalse(book.report(new BalanceReportImpl(10, 10d)));
        assertTrue(book.hasRecord(10));
        book.eraseRecord(10);

        assertEquals(2, listener.count);
    }

    @Test
    void testNotifyingOperationBook() {
        CountingUpdateListener listener = new CountingUpdateListener();
        OperationRecordBook book = new NotifyingOperationBook(listener, new OperationsImpl());

        assertTrue(book.execute(new OperationImpl(10, 10d)));
        assertFalse(book.execute(new OperationImpl(10, 10d)));
        assertTrue(book.hasRecord(10));
        book.eraseRecord(10);

        assertEquals(2, listener.count);
    }

    @Test
    void testNotifyingReferencePeriods() {
        CountingUpdateListener listener = new CountingUpdateListener();
        ReferencePeriodsImpl periodsImpl = new ReferencePeriodsImpl(Collections.emptyList());
        PeriodBook periodBook = new NotifyingPeriodBook(listener, periodsImpl);

        assertTrue(periodBook.append(new ReferencePeriodImpl(0, 10, 10d)));
        assertFalse(periodBook.append(new ReferencePeriodImpl(9, 20, 10d)));
        assertTrue(periodBook.append(new ReferencePeriodImpl(10, 20, 10d)));
        assertFalse(periodBook.rewrite(Collections.singletonList(new ReferencePeriodImpl(9, 20, 10d))));

        assertEquals(2, periodsImpl.periods().size());
        assertEquals(0, periodsImpl.unite().start());
        assertEquals(20, periodsImpl.unite().end());
        assertNotNull(periodsImpl.periodAtTime(5));

        assertTrue(periodBook.rewrite(Collections.singletonList(new ReferencePeriodImpl(0, 100, 999d))));
        assertEquals(1, periodsImpl.periods().size());
        assertEquals(0, periodsImpl.unite().start());
        assertEquals(100, periodsImpl.unite().end());
        assertNotNull(periodsImpl.periodAtTime(5));

        assertEquals(3, listener.count);
    }

    @Test
    void testNotifyingPlannedOperations() {
        CountingUpdateListener listener = new CountingUpdateListener();
        OperationsImpl ops = new OperationsImpl();
        PlannedOperations operations = new NotifyingPlannedOperations(listener, new PlannedOperationsImpl(
                ops, ops, new BookSchedule(ops), new TestPlannedOpsTagDomain(), new TagsImpl(new HashMap<>())
        ));

        assertTrue(operations.plan(new PlannedOperationSeriesImpl("foo", new OperationImpl(10, 10d), new ConstantIterator(100), 2)));
        assertEquals(1, operations.plannedSeries().size());
        assertFalse(operations.plan(new PlannedOperationSeriesImpl("foo", new OperationImpl(10, 999d), new ConstantIterator(100), 2)));
        assertEquals(1, operations.plannedSeries().size());
        operations.delete("foo");
        assertEquals(2, listener.count);
    }

}
