package budget.impl.model.metrics;

import budget.impl.model.entity.BalanceReportImpl;
import budget.impl.model.entity.OperationImpl;
import budget.impl.model.entity.ReferencePeriodImpl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Kirill Berezin
 */
class MetricsTest {
    @Test
    void testTime() {
        ReportBalanceItem reportBalanceItem = new ReportBalanceItem(new BalanceReportImpl(10, 50d));
        assertEquals(10, reportBalanceItem.time());
        OperationBalanceItem operationBalanceItem = new OperationBalanceItem(new OperationImpl(14, 50d));
        assertEquals(14, operationBalanceItem.time());
        StartOfPeriodBalanceItem startOfPeriodBalanceItem = new StartOfPeriodBalanceItem(new ReferencePeriodImpl(22, 44, 100d));
        assertEquals(22, startOfPeriodBalanceItem.time());
        EndOfPeriodBalanceItem endOfPeriodBalanceItem = new EndOfPeriodBalanceItem(new ReferencePeriodImpl(44, 66, 100d));
        assertEquals(66, endOfPeriodBalanceItem.time());

        RunningMetrics metrics = new RunningMetrics(1000L, 0, 0);
        assertEquals(10, reportBalanceItem.visitBy(metrics).time());
        assertEquals(14, operationBalanceItem.visitBy(metrics).time());
        assertEquals(22, startOfPeriodBalanceItem.visitBy(metrics).time());
        assertEquals(66, endOfPeriodBalanceItem.visitBy(metrics).time());

    }
}
