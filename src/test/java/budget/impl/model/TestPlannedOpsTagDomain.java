package budget.impl.model;

import budget.impl.model.operation.PlannedOperationsTagDomain;
import org.jetbrains.annotations.NotNull;

/**
 * @author Kirill Berezin
 */
public class TestPlannedOpsTagDomain implements PlannedOperationsTagDomain {
    @NotNull
    @Override
    public String previousOperationKey() {
        return "prev";
    }

    @NotNull
    @Override
    public String nextOperationKey() {
        return "next";
    }

    @NotNull
    @Override
    public String seriesNameKey() {
        return "s.name";
    }
}
