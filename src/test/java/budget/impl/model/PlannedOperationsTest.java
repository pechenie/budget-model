package budget.impl.model;

import budget.impl.PlannedOperationsTester;
import budget.impl.model.entity.OperationImpl;
import budget.impl.model.operation.PlannedOperationSeriesImpl;
import budget.impl.model.operation.PlannedOperationsImpl;
import budget.impl.model.tag.TagsImpl;
import budget.impl.model.time.ConstantIterator;
import budget.model.book.OperationRecordBook;
import budget.model.operation.PlannedOperations;
import budget.model.source.Operations;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Kirill Berezin
 */

class PlannedOperationsTest {

    @Test
    void test() throws Throwable {
        OperationsImpl ops = new OperationsImpl();
        PlannedOperationsImpl plannedOperations = new PlannedOperationsImpl(
                ops, ops, new BookSchedule(ops), new TestPlannedOpsTagDomain(), new TagsImpl(new HashMap<>())
        );
        new PlannedOperationsTester(new PlannedOperationsTester.Provider() {
            @Override
            public PlannedOperations plannedOperations() {
                return plannedOperations;
            }

            @Override
            public Operations operations() {
                return ops;
            }

            @Override
            public OperationRecordBook book() {
                return plannedOperations;
            }
        }).doTest();
    }

    @Test
    void testIndexRebuilding() throws Throwable {
        OperationsImpl ops = new OperationsImpl();
        TagsImpl tags = new TagsImpl(new HashMap<>());
        PlannedOperationsImpl plannedOperations = new PlannedOperationsImpl(
                ops, ops, new BookSchedule(ops), new TestPlannedOpsTagDomain(), tags
        );
        new PlannedOperationsTester(new PlannedOperationsTester.Provider() {
            @Override
            public PlannedOperations plannedOperations() {
                return plannedOperations;
            }

            @Override
            public Operations operations() {
                return ops;
            }

            @Override
            public OperationRecordBook book() {
                return plannedOperations;
            }
        }, false).doTest();

        PlannedOperationsImpl plannedOperationsRebuilt = new PlannedOperationsImpl(
                ops, ops, new BookSchedule(ops), new TestPlannedOpsTagDomain(), tags
        );
        new PlannedOperationsTester(new PlannedOperationsTester.Provider() {
            @Override
            public PlannedOperations plannedOperations() {
                return plannedOperationsRebuilt;
            }

            @Override
            public Operations operations() {
                return ops;
            }

            @Override
            public OperationRecordBook book() {
                return plannedOperationsRebuilt;
            }
        }, false).finalCheck();
    }

    @Test
    void testInconsistentSchedule() throws Throwable {
        OperationsImpl ops = new OperationsImpl();
        PlannedOperationsImpl plannedOperations = new PlannedOperationsImpl(
                ops, ops, time -> false, new TestPlannedOpsTagDomain(), new TagsImpl(new HashMap<>())
        );

        plannedOperations.plan(new PlannedOperationSeriesImpl("a", new OperationImpl(10, 10d), new ConstantIterator(0), 1));
        assertThrows(IllegalStateException.class,
                () -> plannedOperations.plan(new PlannedOperationSeriesImpl("b", new OperationImpl(10, 999d), new ConstantIterator(0), 1)));
    }

    @SuppressWarnings("ConstantConditions")
    @Test
    void testClash() throws Throwable {
        OperationsImpl ops = new OperationsImpl();
        PlannedOperationsImpl plannedOperations = new PlannedOperationsImpl(
                ops, ops, new BookSchedule(ops), new TestPlannedOpsTagDomain(), new TagsImpl(new HashMap<>())
        );

        plannedOperations.plan(new PlannedOperationSeriesImpl("a", new OperationImpl(10, 10d), new ConstantIterator(0), 1));
        plannedOperations.plan(new PlannedOperationSeriesImpl("b", new OperationImpl(10, 999d), new ConstantIterator(0), 1));

        assertTrue(plannedOperations.hasRecord(10));
        assertEquals(10d, ops.operationAt(10).cost());
        assertTrue(plannedOperations.hasRecord(11));
        assertEquals(999d, ops.operationAt(11).cost());
    }

    @SuppressWarnings("ConstantConditions")
    @Test
    void testNameCollision() throws Throwable {
        OperationsImpl ops = new OperationsImpl();
        PlannedOperationsImpl plannedOperations = new PlannedOperationsImpl(
                ops, ops, new BookSchedule(ops), new TestPlannedOpsTagDomain(), new TagsImpl(new HashMap<>())
        );

        assertTrue(plannedOperations.plan(new PlannedOperationSeriesImpl("a", new OperationImpl(10, 10d), new ConstantIterator(0), 1)));
        assertFalse(plannedOperations.plan(new PlannedOperationSeriesImpl("a", new OperationImpl(10, 999d), new ConstantIterator(0), 1)));

        assertTrue(plannedOperations.hasRecord(10));
        assertEquals(10d, ops.operationAt(10).cost());
        assertFalse(plannedOperations.hasRecord(11));
    }
}
