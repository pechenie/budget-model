package budget.impl.model.future.pub;

import budget.impl.model.OperationsImpl;
import budget.impl.model.entity.OperationImpl;
import budget.impl.model.future.MultipleSourceOperations;
import budget.model.Operation;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @since 29.04.2017
 */
class MultipleOperationsTest {
    private static MultipleSourceOperations transactionsWithCollection;
    private static MultipleSourceOperations transactionsWithVarargs;

    @BeforeAll
    static void prepare() {
        transactionsWithCollection = new MultipleSourceOperations(Arrays.asList(
                new OperationsImpl(),
                new OperationsImpl(
                        new OperationImpl(10, 100),
                        new OperationImpl(20, 100)),
                new OperationsImpl(
                        new OperationImpl(15, 100),
                        new OperationImpl(25, 100)),
                new OperationsImpl(
                        new OperationImpl(5, 100),
                        new OperationImpl(10, 100)),
                new OperationsImpl(
                        new OperationImpl(15, 100),
                        new OperationImpl(20, 100))
        ));

        transactionsWithVarargs = new MultipleSourceOperations(
                new OperationsImpl(),
                new OperationsImpl(
                        new OperationImpl(10, 100),
                        new OperationImpl(20, 100)),
                new OperationsImpl(
                        new OperationImpl(15, 100),
                        new OperationImpl(25, 100)),
                new OperationsImpl(
                        new OperationImpl(5, 100),
                        new OperationImpl(10, 100)),
                new OperationsImpl(
                        new OperationImpl(15, 100),
                        new OperationImpl(20, 100))
        );
    }

    //times: 5, 10, 10, 15, 15, 20, 20, 25

    @SuppressWarnings("ConstantConditions")
    @Test
    void testOperationAt() {
        assertNotNull(transactionsWithCollection.operationAt(5));
        assertNotNull(transactionsWithVarargs.operationAt(5));
        assertNotNull(transactionsWithCollection.operationAt(10));
        assertNotNull(transactionsWithVarargs.operationAt(10));
        assertNotNull(transactionsWithCollection.operationAt(15));
        assertNotNull(transactionsWithVarargs.operationAt(15));
        assertNotNull(transactionsWithCollection.operationAt(20));
        assertNotNull(transactionsWithVarargs.operationAt(20));
        assertNotNull(transactionsWithCollection.operationAt(25));
        assertNotNull(transactionsWithVarargs.operationAt(25));

        assertEquals(5, transactionsWithCollection.operationAt(5).time());
        assertEquals(5, transactionsWithVarargs.operationAt(5).time());
        assertEquals(10, transactionsWithCollection.operationAt(10).time());
        assertEquals(10, transactionsWithVarargs.operationAt(10).time());
        assertEquals(15, transactionsWithCollection.operationAt(15).time());
        assertEquals(15, transactionsWithVarargs.operationAt(15).time());
        assertEquals(20, transactionsWithCollection.operationAt(20).time());
        assertEquals(20, transactionsWithVarargs.operationAt(20).time());
        assertEquals(25, transactionsWithCollection.operationAt(25).time());
        assertEquals(25, transactionsWithVarargs.operationAt(25).time());

        assertNull(transactionsWithCollection.operationAt(50));
        assertNull(transactionsWithVarargs.operationAt(50));
        assertNull(transactionsWithCollection.operationAt(100));
        assertNull(transactionsWithVarargs.operationAt(100));
    }


    //times: 5, 10, 10, 15, 15, 20, 20, 25

    @Test
    void testSums() {
        assertEquals(0, transactionsWithCollection.operationSum(0, 1, true));
        assertEquals(800, transactionsWithCollection.operationSum(0, 100, true));
        assertEquals(100, transactionsWithCollection.operationSum(0, 5, true));
        assertEquals(0, transactionsWithCollection.operationSum(0, 5, false));
        assertEquals(300, transactionsWithCollection.operationSum(5, 10, true));
        assertEquals(100, transactionsWithCollection.operationSum(5, 10, false));
        assertEquals(200, transactionsWithCollection.operationSum(10, 10, true));
        assertEquals(600, transactionsWithCollection.operationSum(10, 20, true));
        assertEquals(400, transactionsWithCollection.operationSum(10, 20, false));
        assertEquals(500, transactionsWithCollection.operationSum(15, 25, true));
        assertEquals(400, transactionsWithCollection.operationSum(15, 25, false));
        assertEquals(100, transactionsWithCollection.operationSum(25, 25, true));
        assertEquals(0, transactionsWithCollection.operationSum(25, 25, false));
        assertEquals(100, transactionsWithCollection.operationSum(25, 30, false));
        assertEquals(0, transactionsWithCollection.operationSum(26, 100, false));
    }

    //times: 5, 10, 10, 15, 15, 20, 20, 25
    @Test
    void testStreams() {
        TestWithStream.checkStream(transactionsWithCollection.range(0, true, 1, true).stream(), Operation::time);
        TestWithStream
                .checkStream(transactionsWithCollection.range(0, true, 100, true).stream(), Operation::time, 5, 10, 10, 15, 15, 20, 20, 25);
        TestWithStream.checkStream(transactionsWithCollection.range(0, true, 5, true).stream(), Operation::time, 5);
        TestWithStream.checkStream(transactionsWithCollection.range(0, true, 5, false).stream(), Operation::time);
        TestWithStream.checkStream(transactionsWithCollection.range(5, true, 10, true).stream(), Operation::time, 5, 10, 10);
        TestWithStream.checkStream(transactionsWithCollection.range(5, true, 10, false).stream(), Operation::time, 5);
        TestWithStream.checkStream(transactionsWithCollection.range(10, true, 10, true).stream(), Operation::time, 10, 10);
        TestWithStream.checkStream(transactionsWithCollection.range(10, true, 20, true).stream(), Operation::time, 10, 10, 15, 15, 20, 20);
        TestWithStream.checkStream(transactionsWithCollection.range(10, true, 20, false).stream(), Operation::time, 10, 10, 15, 15);
        TestWithStream.checkStream(transactionsWithCollection.range(15, true, 25, true).stream(), Operation::time, 15, 15, 20, 20, 25);
        TestWithStream.checkStream(transactionsWithCollection.range(15, true, 25, false).stream(), Operation::time, 15, 15, 20, 20);
        TestWithStream.checkStream(transactionsWithCollection.range(25, true, 25, true).stream(), Operation::time, 25);
        TestWithStream.checkStream(transactionsWithCollection.range(25, true, 25, false).stream(), Operation::time);
        TestWithStream.checkStream(transactionsWithCollection.range(25, true, 30, false).stream(), Operation::time, 25);
        TestWithStream.checkStream(transactionsWithCollection.range(26, true, 100, false).stream(), Operation::time);
    }

    @Test
    void testSumsVarargs() {
        assertEquals(0, transactionsWithVarargs.operationSum(0, 1, true));
        assertEquals(800, transactionsWithVarargs.operationSum(0, 100, true));
        assertEquals(100, transactionsWithVarargs.operationSum(0, 5, true));
        assertEquals(0, transactionsWithVarargs.operationSum(0, 5, false));
        assertEquals(300, transactionsWithVarargs.operationSum(5, 10, true));
        assertEquals(100, transactionsWithVarargs.operationSum(5, 10, false));
        assertEquals(200, transactionsWithVarargs.operationSum(10, 10, true));
        assertEquals(600, transactionsWithVarargs.operationSum(10, 20, true));
        assertEquals(400, transactionsWithVarargs.operationSum(10, 20, false));
        assertEquals(500, transactionsWithVarargs.operationSum(15, 25, true));
        assertEquals(400, transactionsWithVarargs.operationSum(15, 25, false));
        assertEquals(100, transactionsWithVarargs.operationSum(25, 25, true));
        assertEquals(0, transactionsWithVarargs.operationSum(25, 25, false));
        assertEquals(100, transactionsWithVarargs.operationSum(25, 30, false));
        assertEquals(0, transactionsWithVarargs.operationSum(26, 100, false));
    }

    //times: 5, 10, 10, 15, 15, 20, 20, 25
    @Test
    void testStreamsVarargs() {
        TestWithStream.checkStream(transactionsWithVarargs.range(0, true, 1, true).stream(), Operation::time);
        TestWithStream
                .checkStream(transactionsWithVarargs.range(0, true, 100, true).stream(), Operation::time, 5, 10, 10, 15, 15, 20, 20, 25);
        TestWithStream.checkStream(transactionsWithVarargs.range(0, true, 5, true).stream(), Operation::time, 5);
        TestWithStream.checkStream(transactionsWithVarargs.range(0, true, 5, false).stream(), Operation::time);
        TestWithStream.checkStream(transactionsWithVarargs.range(5, true, 10, true).stream(), Operation::time, 5, 10, 10);
        TestWithStream.checkStream(transactionsWithVarargs.range(5, true, 10, false).stream(), Operation::time, 5);
        TestWithStream.checkStream(transactionsWithVarargs.range(10, true, 10, true).stream(), Operation::time, 10, 10);
        TestWithStream.checkStream(transactionsWithVarargs.range(10, true, 20, true).stream(), Operation::time, 10, 10, 15, 15, 20, 20);
        TestWithStream.checkStream(transactionsWithVarargs.range(10, true, 20, false).stream(), Operation::time, 10, 10, 15, 15);
        TestWithStream.checkStream(transactionsWithVarargs.range(15, true, 25, true).stream(), Operation::time, 15, 15, 20, 20, 25);
        TestWithStream.checkStream(transactionsWithVarargs.range(15, true, 25, false).stream(), Operation::time, 15, 15, 20, 20);
        TestWithStream.checkStream(transactionsWithVarargs.range(25, true, 25, true).stream(), Operation::time, 25);
        TestWithStream.checkStream(transactionsWithVarargs.range(25, true, 25, false).stream(), Operation::time);
        TestWithStream.checkStream(transactionsWithVarargs.range(25, true, 30, false).stream(), Operation::time, 25);
        TestWithStream.checkStream(transactionsWithVarargs.range(26, true, 100, false).stream(), Operation::time);
    }
}
