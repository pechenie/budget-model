package budget.impl.model.future.pub;

import budget.impl.model.entity.ReferencePeriodImpl;
import budget.model.ReferencePeriod;
import budget.model.source.ReferencePeriods;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

/**
 * @author Kirill Berezin
 */
final class ArithmeticIncrementPeriods implements ReferencePeriods {
    private final long from;
    private final long increment;
    private final long to;

    ArithmeticIncrementPeriods(long from, long increment, long to) {
        this.from = from;
        this.increment = increment;
        this.to = to;
    }

    @Override
    public ReferencePeriod periodAtTime(long time) {
        if (time < from || time >= to) {
            return null;
        }
        long multiplier = time / increment;
        return new ReferencePeriodImpl(Math.max(from, multiplier * increment),
                Math.min(to, (multiplier + 1) * increment), 100);
    }

    @NotNull
    @Override
    public Collection<ReferencePeriod> periods() {
        long modFrom = from - from % increment;
        long modTo = to + ((to / increment + 1) * increment - to) % increment;
        long cleanPeriodsCount = (modTo - modFrom) / increment;
        return LongStream.iterate(from, operand -> (operand + increment) / increment * increment)
                .limit(cleanPeriodsCount).mapToObj(this::periodAtTime).collect(Collectors.toList());
    }

    @Override
    public @NotNull Collection<ReferencePeriod> range(long from, long to, boolean inclusiveTo) {
        throw new UnsupportedOperationException();
    }

    @NotNull
    @Override
    public ReferencePeriod unite() {
        return new ReferencePeriodImpl(from, to, Double.NaN);
    }
}
