package budget.impl.model.future.pub;

import budget.impl.model.ReferencePeriodsImpl;
import budget.impl.model.entity.ReferencePeriodImpl;
import budget.model.ReferencePeriod;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @since 29.04.2017
 */
@SuppressWarnings("ConstantConditions")
class ReferencePeriodsFactoryAndBookTest {

    @Test
    void testEmpty() {
        ReferencePeriodsImpl periods = new ReferencePeriodsImpl(Collections.emptyList());
        assertTrue(periods.rewrite(new ArithmeticFactory(150).calculateWithStartingTime(0)));

        assertEquals(15, periods.periods().size());
        TestWithStream.checkStream(periods.periods().stream(), ReferencePeriod::start,
                0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140);
        TestWithStream.checkStream(periods.periods().stream(), ReferencePeriod::end,
                10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150);
        assertEquals(0, periods.unite().start());
        assertEquals(150, periods.unite().end());

        assertNull(periods.periodAtTime(-1));
        assertNotNull(periods.periodAtTime(33));
        assertNotNull(periods.periodAtTime(99));
        assertNotNull(periods.periodAtTime(123));
        assertNull(periods.periodAtTime(150));
    }

    @Test
    void testEmptyAppend() {
        ReferencePeriodsImpl periods = new ReferencePeriodsImpl(Collections.emptyList());
        assertTrue(periods.append(new ReferencePeriodImpl(99, 123, 1d)));

        assertEquals(1, periods.periods().size());
        TestWithStream.checkStream(periods.periods().stream(), ReferencePeriod::start, 99);
        TestWithStream.checkStream(periods.periods().stream(), ReferencePeriod::end, 123);
        assertEquals(99, periods.unite().start());
        assertEquals(123, periods.unite().end());

        assertNull(periods.periodAtTime(98));
        assertNotNull(periods.periodAtTime(99));
        assertNotNull(periods.periodAtTime(122));
        assertNull(periods.periodAtTime(123));
    }

    @Test
    void testOfFactory() {
        ReferencePeriodsImpl periods = new ReferencePeriodsImpl(Arrays.asList(
                new ReferencePeriodImpl(2, 13, 999),
                new ReferencePeriodImpl(13, 27, 999)));
        assertTrue(periods.rewrite(new ArithmeticFactory(31).calculateWithStartingTime(27)));

        assertEquals(4, periods.periods().size());
        TestWithStream.checkStream(periods.periods().stream(), ReferencePeriod::start, 2, 13, 27, 30);
        TestWithStream.checkStream(periods.periods().stream(), ReferencePeriod::end, 13, 27, 30, 31);
        assertEquals(2, periods.unite().start());
        assertEquals(31, periods.unite().end());

        assertNull(periods.periodAtTime(1));
        assertNotNull(periods.periodAtTime(2));
        assertNotNull(periods.periodAtTime(13));
        assertNotNull(periods.periodAtTime(27));
        assertNotNull(periods.periodAtTime(30));
        assertNull(periods.periodAtTime(31));
    }

    @Test
    void test() {
        ReferencePeriodsImpl periods = new ReferencePeriodsImpl(Arrays.asList(
                new ReferencePeriodImpl(13, 27, 999),
                new ReferencePeriodImpl(0, 13, 999),
                new ReferencePeriodImpl(27, 33, 999)));
        assertTrue(periods.rewrite(new ArithmeticFactory(150).calculateWithStartingTime(33)));

        assertEquals(15, periods.periods().size());
        TestWithStream.checkStream(periods.periods().stream(), ReferencePeriod::start,
                0, 13, 27, 33, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140);
        TestWithStream.checkStream(periods.periods().stream(), ReferencePeriod::end,
                13, 27, 33, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150);
        assertEquals(0, periods.unite().start());
        assertEquals(150, periods.unite().end());

        assertNull(periods.periodAtTime(-1));
        assertEquals(999, periods.periodAtTime(0).availableMargin());
        assertEquals(999, periods.periodAtTime(27).availableMargin());
        assertEquals(999, periods.periodAtTime(32).availableMargin());
        assertNotEquals(999, periods.periodAtTime(33));
        assertNotNull(periods.periodAtTime(123));
        assertNull(periods.periodAtTime(150));
    }

    @Test
    void testWithAppend() {
        ReferencePeriodsImpl periods = new ReferencePeriodsImpl(Collections.emptyList());
        assertTrue(periods.rewrite(new ArithmeticFactory(150).calculateWithStartingTime(0)));

        assertEquals(15, periods.periods().size());
        TestWithStream.checkStream(periods.periods().stream(), ReferencePeriod::start,
                0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140);
        TestWithStream.checkStream(periods.periods().stream(), ReferencePeriod::end,
                10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150);
        assertEquals(0, periods.unite().start());
        assertEquals(150, periods.unite().end());

        List<ReferencePeriod> newPeriods = new ArrayList<>();
        newPeriods.addAll(new ArithmeticFactory(150).calculateWithStartingTime(67));
        assertFalse(periods.rewrite(newPeriods));

        newPeriods.add(new ReferencePeriodImpl(0, 67, 999d));
        assertTrue(periods.rewrite(newPeriods));

        assertEquals(10, periods.periods().size());
        TestWithStream.checkStream(periods.periods().stream(), ReferencePeriod::start,
                0, 67, 70, 80, 90, 100, 110, 120, 130, 140);
        TestWithStream.checkStream(periods.periods().stream(), ReferencePeriod::end,
                67, 70, 80, 90, 100, 110, 120, 130, 140, 150);
        assertEquals(0, periods.unite().start());
        assertEquals(150, periods.unite().end());

        assertFalse(periods.append(new ReferencePeriodImpl(10, 20, 99999d)));
        assertEquals(10, periods.periods().size());
        TestWithStream.checkStream(periods.periods().stream(), ReferencePeriod::start,
                0, 67, 70, 80, 90, 100, 110, 120, 130, 140);
        TestWithStream.checkStream(periods.periods().stream(), ReferencePeriod::end,
                67, 70, 80, 90, 100, 110, 120, 130, 140, 150);
        assertEquals(0, periods.unite().start());
        assertEquals(150, periods.unite().end());

        assertTrue(periods.append(new ReferencePeriodImpl(150, 200, 99999d)));
        assertEquals(11, periods.periods().size());
        TestWithStream.checkStream(periods.periods().stream(), ReferencePeriod::start,
                0, 67, 70, 80, 90, 100, 110, 120, 130, 140, 150);
        TestWithStream.checkStream(periods.periods().stream(), ReferencePeriod::end,
                67, 70, 80, 90, 100, 110, 120, 130, 140, 150, 200);
        assertEquals(0, periods.unite().start());
        assertEquals(200, periods.unite().end());
    }

    @Test
    void testRewriteBeforeStart() {
        ReferencePeriodsImpl periods = new ReferencePeriodsImpl(Collections.emptyList());
        assertTrue(periods.rewrite(new ArithmeticFactory(150).calculateWithStartingTime(0)));

        assertEquals(15, periods.periods().size());
        TestWithStream.checkStream(periods.periods().stream(), ReferencePeriod::start,
                0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140);
        TestWithStream.checkStream(periods.periods().stream(), ReferencePeriod::end,
                10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150);
        assertEquals(0, periods.unite().start());
        assertEquals(150, periods.unite().end());

        //NOW REWRITE
        assertTrue(periods.rewrite(Arrays.asList(
                new ReferencePeriodImpl(-140, -55, 9999d),
                new ReferencePeriodImpl(-55, -13, 9999d),
                new ReferencePeriodImpl(-13, 20, 9999d)
        )));

        assertEquals(16, periods.periods().size());
        TestWithStream.checkStream(periods.periods().stream(), ReferencePeriod::start,
                -140, -55, -13, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140);
        TestWithStream.checkStream(periods.periods().stream(), ReferencePeriod::end,
                -55, -13, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150);
        assertEquals(-140, periods.unite().start());
        assertEquals(150, periods.unite().end());
    }

    @Test
    void testRewriteAfterEnd() {
        ReferencePeriodsImpl periods = new ReferencePeriodsImpl(Collections.emptyList());
        assertTrue(periods.rewrite(new ArithmeticFactory(150).calculateWithStartingTime(0)));

        assertEquals(15, periods.periods().size());
        TestWithStream.checkStream(periods.periods().stream(), ReferencePeriod::start,
                0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140);
        TestWithStream.checkStream(periods.periods().stream(), ReferencePeriod::end,
                10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150);
        assertEquals(0, periods.unite().start());
        assertEquals(150, periods.unite().end());

        //NOW REWRITE
        assertTrue(periods.rewrite(Arrays.asList(
                new ReferencePeriodImpl(120, 200, 9999d),
                new ReferencePeriodImpl(200, 300, 9999d),
                new ReferencePeriodImpl(300, 301, 9999d)
        )));

        assertEquals(15, periods.periods().size());
        TestWithStream.checkStream(periods.periods().stream(), ReferencePeriod::start,
                0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 200, 300);
        TestWithStream.checkStream(periods.periods().stream(), ReferencePeriod::end,
                10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 200, 300, 301);
        assertEquals(0, periods.unite().start());
        assertEquals(301, periods.unite().end());
    }

    @Test
    void testRewriteInTheMiddle() {
        ReferencePeriodsImpl periods = new ReferencePeriodsImpl(Collections.emptyList());
        assertTrue(periods.rewrite(new ArithmeticFactory(150).calculateWithStartingTime(0)));

        assertEquals(15, periods.periods().size());
        TestWithStream.checkStream(periods.periods().stream(), ReferencePeriod::start,
                0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140);
        TestWithStream.checkStream(periods.periods().stream(), ReferencePeriod::end,
                10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150);
        assertEquals(0, periods.unite().start());
        assertEquals(150, periods.unite().end());

        //NOW REWRITE
        assertTrue(periods.rewrite(Arrays.asList(
                new ReferencePeriodImpl(10, 77, 9999d),
                new ReferencePeriodImpl(77, 140, 9999d)
        )));

        assertEquals(4, periods.periods().size());
        TestWithStream.checkStream(periods.periods().stream(), ReferencePeriod::start,
                0, 10, 77, 140);
        TestWithStream.checkStream(periods.periods().stream(), ReferencePeriod::end,
                10, 77, 140, 150);
        assertEquals(0, periods.unite().start());
        assertEquals(150, periods.unite().end());

    }

    @Test
    void testRewriteCompletely() {
        ReferencePeriodsImpl periods = new ReferencePeriodsImpl(Collections.emptyList());
        assertTrue(periods.rewrite(new ArithmeticFactory(150).calculateWithStartingTime(0)));

        assertEquals(15, periods.periods().size());
        TestWithStream.checkStream(periods.periods().stream(), ReferencePeriod::start,
                0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140);
        TestWithStream.checkStream(periods.periods().stream(), ReferencePeriod::end,
                10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150);
        assertEquals(0, periods.unite().start());
        assertEquals(150, periods.unite().end());

        //NOW REWRITE
        assertTrue(periods.rewrite(Arrays.asList(
                new ReferencePeriodImpl(-10, 77, 9999d),
                new ReferencePeriodImpl(77, 140, 9999d),
                new ReferencePeriodImpl(140, 198, 9999d)
        )));

        assertEquals(3, periods.periods().size());
        TestWithStream.checkStream(periods.periods().stream(), ReferencePeriod::start,
                -10, 77, 140);
        TestWithStream.checkStream(periods.periods().stream(), ReferencePeriod::end,
                77, 140, 198);
        assertEquals(-10, periods.unite().start());
        assertEquals(198, periods.unite().end());
    }
}
