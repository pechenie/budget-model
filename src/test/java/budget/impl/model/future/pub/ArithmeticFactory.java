package budget.impl.model.future.pub;

import budget.impl.model.future.ReferencePeriodsFactory;
import budget.model.ReferencePeriod;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Kirill Berezin
 */
public final class ArithmeticFactory implements ReferencePeriodsFactory {
    private final int to;

    public ArithmeticFactory(int to) {
        this.to = to;
    }

    @NotNull
    @Override
    public List<? extends ReferencePeriod> calculateWithStartingTime(long time) {
        return new ArrayList<>(new ArithmeticIncrementPeriods(time, 10, to).periods());
    }
}
