package budget.impl.model.future.pub;

import java.util.Iterator;
import java.util.function.Function;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @since 29.04.2017
 */
final class TestWithStream {
    static <T> void checkStream(Stream<T> stream, Function<T, Long> fetchValueFunc, long... values) {
        Iterator<T> iterator = stream.iterator();
        for (long value : values) {
            assertEquals(value, (long) fetchValueFunc.apply(iterator.next()));
        }
    }
}
