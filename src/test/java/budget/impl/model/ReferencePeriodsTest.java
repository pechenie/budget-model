package budget.impl.model;

import budget.impl.model.entity.ReferencePeriodImpl;
import budget.model.ReferencePeriod;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @since 29.04.2017
 */
@SuppressWarnings("ConstantConditions")
class ReferencePeriodsTest {
    private static ReferencePeriodsImpl referencePeriods;

    @BeforeAll
    static void prepare() {
        referencePeriods = new ReferencePeriodsImpl(Arrays.asList(
                new ReferencePeriodImpl(100, 200, 10),
                new ReferencePeriodImpl(200, 300, 50)
        ));
    }

    @Test
    void testReferencePeriodsCount() {
        assertEquals(2, referencePeriods.periods().size());
    }

    @Test
    void testReferencePeriodsRange() {
        assertEquals(0, referencePeriods.range(50, 100, false).size());
        assertEquals(1, referencePeriods.range(50, 100, true).size());
        assertEquals(0, referencePeriods.range(50, 150, false).size());
        assertEquals(1, referencePeriods.range(50, 150, true).size());
        assertEquals(1, referencePeriods.range(50, 200, false).size());
        assertEquals(2, referencePeriods.range(50, 200, true).size());
        assertEquals(1, referencePeriods.range(50, 250, false).size());
        assertEquals(2, referencePeriods.range(50, 1000, true).size());
        assertEquals(2, referencePeriods.range(50, 1000, false).size());
        assertEquals(0, referencePeriods.range(500, 100, true).size());
        assertEquals(0, referencePeriods.range(500, 100, false).size());
    }

    @Test
    void testReferencePeriodsBounds() {
        assertNull(referencePeriods.periodAtTime(99));
        assertNotNull(referencePeriods.periodAtTime(100));
        assertNotNull(referencePeriods.periodAtTime(299));
        assertNull(referencePeriods.periodAtTime(300));
    }

    @Test
    void testReferencePeriodsLookup() {
        assertEquals(10, referencePeriods.periodAtTime(100).availableMargin());
        assertEquals(10, referencePeriods.periodAtTime(150).availableMargin());
        assertEquals(10, referencePeriods.periodAtTime(199).availableMargin());
        assertEquals(50, referencePeriods.periodAtTime(200).availableMargin());
        assertEquals(50, referencePeriods.periodAtTime(250).availableMargin());
        assertEquals(50, referencePeriods.periodAtTime(299).availableMargin());
    }

    @Test
    void testReferencePeriodsConsistency() {
        assertTrue(referencePeriods.periodAtTime(100).includes(100));
        assertTrue(referencePeriods.periodAtTime(150).includes(150));
        assertTrue(referencePeriods.periodAtTime(199).includes(199));
        assertTrue(referencePeriods.periodAtTime(200).includes(200));
        assertTrue(referencePeriods.periodAtTime(250).includes(250));
        assertTrue(referencePeriods.periodAtTime(299).includes(299));
    }

    @Test
    void testUnion() {
        ReferencePeriod union = referencePeriods.unite();
        assertNull(referencePeriods.periodAtTime(99));
        assertFalse(union.includes(99));
        assertTrue(union.includes(100));
        assertTrue(union.includes(150));
        assertTrue(union.includes(199));
        assertTrue(union.includes(200));
        assertTrue(union.includes(250));
        assertTrue(union.includes(299));
        assertNull(referencePeriods.periodAtTime(300));
        assertFalse(union.includes(300));
    }

    @Test
    void testEmptyUnion() {
        ReferencePeriod unite = new ReferencePeriodsImpl(Collections.emptyList()).unite();
        assertEquals(Long.MIN_VALUE, unite.start());
        assertEquals(Long.MIN_VALUE, unite.end());
    }

    @Test
    void testInconsistentPeriods() {
        assertThrows(IllegalArgumentException.class, () -> new ReferencePeriodsImpl(Arrays.asList(
                new ReferencePeriodImpl(0, 10, 999d),
                new ReferencePeriodImpl(11, 20, 999d)
        )));

        assertThrows(IllegalArgumentException.class, () -> new ReferencePeriodsImpl(Arrays.asList(
                new ClosedReferencePeriodDecorator(new ReferencePeriodImpl(0, 10, 999d)),
                new ReferencePeriodImpl(10, 20, 999d)
        )));
    }

    @Test
    void testAppendWithInclusiveEnd() {
        ReferencePeriodsImpl periods = new ReferencePeriodsImpl(Collections.singleton(
                new ClosedReferencePeriodDecorator(new ReferencePeriodImpl(0, 100, 500d))
        ));

        assertFalse(periods.append(new ReferencePeriodImpl(100, 200, 1)));
        assertNull(periods.periodAtTime(150));
        assertTrue(periods.append(new ReferencePeriodImpl(101, 350, 1)));
        assertEquals(1, periods.periodAtTime(340).availableMargin());
    }

    @AfterAll
    static void testAppend() {
        assertFalse(referencePeriods.append(new ReferencePeriodImpl(0, 100, 1)));
        assertNull(referencePeriods.periodAtTime(50));
        assertFalse(referencePeriods.append(new ReferencePeriodImpl(250, 350, 1)));
        assertNull(referencePeriods.periodAtTime(340));
        assertFalse(referencePeriods.append(new ReferencePeriodImpl(888, 1999, 1)));
        assertNull(referencePeriods.periodAtTime(1000));
        assertFalse(referencePeriods.append(new ReferencePeriodImpl(301, 1999, 1)));
        assertNull(referencePeriods.periodAtTime(1000));

        //and finally proper append
        assertTrue(referencePeriods.append(new ReferencePeriodImpl(300, 1999, 1)));
        assertNotNull(referencePeriods.periodAtTime(1000));
        assertEquals(1, referencePeriods.periodAtTime(1000).availableMargin());
    }

    private static final class ClosedReferencePeriodDecorator implements ReferencePeriod {
        private final ReferencePeriod decorated;

        private ClosedReferencePeriodDecorator(ReferencePeriod decorated) {
            this.decorated = decorated;
        }

        @Override
        public long start() {
            return decorated.start();
        }

        @Override
        public long end() {
            return decorated.end();
        }

        @Override
        public @NotNull ReferencePeriod.Relation relatesTo(long time) {
            if (time < start()) {
                return Relation.PERIOD_IS_AFTER;
            } else if (end() < time) {
                return Relation.PERIOD_IS_BEFORE;
            } else {
                return Relation.PERIOD_INCLUDES;
            }
        }

        @Override
        public boolean includes(long time) {
            return start() <= time && time <= end();
        }

        @Override
        public double availableMargin() {
            return decorated.availableMargin();
        }
    }
}
