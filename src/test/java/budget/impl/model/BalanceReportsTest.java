package budget.impl.model;

import budget.impl.model.entity.BalanceReportImpl;
import budget.model.source.BalanceReports;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @since 29.04.2017
 */
class BalanceReportsTest {
    private static BalanceReports balanceReports;

    @BeforeAll
    static void prepare() {
        balanceReports = new BalanceReportsImpl(Arrays.asList(
                new BalanceReportImpl(100, 100),
                new BalanceReportImpl(200, 100),
                new BalanceReportImpl(300, 100)
        ));
    }

    @Test
    void balanceReportsTestCount() {
        assertEquals(3, balanceReports.range(0, true, 1000, true).size());
        assertEquals(0, balanceReports.range(0, true, 0, true).size());
        assertEquals(0, balanceReports.range(1000, true, 0, true).size());
        assertEquals(3, balanceReports.range(100, true, 300, true).size());

        assertEquals(3, balanceReports.range(100, true, 301, false).size());
        assertEquals(2, balanceReports.range(100, true, 300, false).size());
        assertEquals(1, balanceReports.range(100, true, 200, false).size());

        assertEquals(0, balanceReports.range(1000, false, 0, true).size());
        assertEquals(2, balanceReports.range(100, false, 300, true).size());
        assertEquals(2, balanceReports.range(100, false, 301, false).size());
        assertEquals(1, balanceReports.range(100, false, 300, false).size());
        assertEquals(0, balanceReports.range(100, false, 200, false).size());
    }

    @Test
    void balanceReportsReportToDate() {
        assertEquals(Long.MIN_VALUE, balanceReports.lastReportAtTime(0, true).time());
        assertEquals(Long.MIN_VALUE, balanceReports.lastReportAtTime(100, false).time());
        assertEquals(100, balanceReports.lastReportAtTime(100, true).time());
        assertEquals(100, balanceReports.lastReportAtTime(200, false).time());
        assertEquals(200, balanceReports.lastReportAtTime(200, true).time());
        assertEquals(200, balanceReports.lastReportAtTime(300, false).time());
        assertEquals(300, balanceReports.lastReportAtTime(300, true).time());
        assertEquals(300, balanceReports.lastReportAtTime(500, false).time());
        assertEquals(300, balanceReports.lastReportAtTime(500, true).time());
    }
}
