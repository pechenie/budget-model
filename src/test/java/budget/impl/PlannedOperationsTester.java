package budget.impl;

import budget.impl.model.entity.OperationImpl;
import budget.impl.model.operation.PlannedOperationSeriesImpl;
import budget.impl.model.time.ConstantIterator;
import budget.model.book.OperationRecordBook;
import budget.model.operation.PlannedOperationSeries;
import budget.model.operation.PlannedOperations;
import budget.model.source.Operations;
import org.junit.jupiter.api.Assertions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Kirill Berezin
 */

public final class PlannedOperationsTester {
    public interface Provider {
        PlannedOperations plannedOperations();

        Operations operations();

        OperationRecordBook book();
    }

    private static final double DELTA = 0.000001;

    private final Provider provider;
    private final boolean deleteWeirdStuff;

    public PlannedOperationsTester(Provider provider, boolean deleteWeirdStuff) {
        this.provider = provider;
        this.deleteWeirdStuff = deleteWeirdStuff;
    }

    public PlannedOperationsTester(Provider provider) {
        this(provider, true);
    }

    public void doTest() throws Throwable {
        PlannedOperations plannedOperations = provider.plannedOperations();
        assertEquals(0, plannedOperations.plannedSeries().size());

        PlannedOperationSeriesImpl incomeSeries = new PlannedOperationSeriesImpl("Income",
                new OperationImpl(25, 200d), new ConstantIterator(100), 3);
        assertEquals(3, incomeSeries.size());

        plannedOperations.plan(incomeSeries);
        plannedOperations.plan(new PlannedOperationSeriesImpl(
                "Credit",
                new OperationImpl(17, -150d),
                new ConstantIterator(100),
                3)
        );
        assertEquals(2, plannedOperations.plannedSeries().size());
        assertEquals(6, plannedOperations.plannedSeries().stream().mapToInt(PlannedOperationSeries::size).sum());
        plannedOperations.plannedSeries().forEach(s -> s.processSeries(Assertions::assertNotNull));

        plannedOperations = provider.plannedOperations();

        Operations operations = provider.operations();
        assertEquals(200d, operations.operationSum(24, 26, false), DELTA);
        assertEquals(50d, operations.operationSum(0, 26, false), DELTA);
        assertEquals(150d, operations.operationSum(0, 10000, false), DELTA);

        plannedOperations.plannedSeries().stream().filter(s -> s.name().equals("Credit")).findAny()
                .map(s -> {
                    s.processSeries(operation -> {
                        assertEquals(-150d, operation.cost(), DELTA);
                        assertEquals(17L, operation.time());
                    });
                    return 0;
                }).orElseThrow(AssertionError::new);
        provider.book().eraseRecord(17L);

        plannedOperations = provider.plannedOperations();
        operations = provider.operations();

        assertEquals(200d, operations.operationSum(24, 26, false), DELTA);
        assertEquals(200d, operations.operationSum(0, 26, false), DELTA);
        assertEquals(300d, operations.operationSum(0, 10000, false), DELTA);

        plannedOperations.plannedSeries().stream().filter(s -> s.name().equals("Credit")).findAny()
                .map(s -> {
                    s.processSeries(operation -> {
                        assertEquals(-150d, operation.cost(), DELTA);
                        assertEquals(117L, operation.time());
                    });
                    return 0;
                }).orElseThrow(AssertionError::new);
        provider.book().eraseRecord(117L);

        plannedOperations = provider.plannedOperations();

        plannedOperations.plannedSeries().stream().filter(s -> s.name().equals("Income")).findAny()
                .map(s -> {
                    s.processSeries(operation -> {
                        assertEquals(200d, operation.cost(), DELTA);
                        assertEquals(25L, operation.time());
                    });
                    return 0;
                }).orElseThrow(AssertionError::new);
        provider.book().eraseRecord(25L);

        operations = provider.operations();

        assertEquals(0d, operations.operationSum(24, 26, false), DELTA);
        assertEquals(0d, operations.operationSum(0, 120, false), DELTA);
        assertEquals(250d, operations.operationSum(0, 10000, false), DELTA);

        plannedOperations = provider.plannedOperations();
        operations = provider.operations();

        plannedOperations.plannedSeries().stream().filter(s -> s.name().equals("Credit")).findAny()
                .map(s -> {
                    s.processSeries(operation -> {
                        assertEquals(-150d, operation.cost(), DELTA);
                        assertEquals(217L, operation.time());
                    });
                    return 0;
                }).orElseThrow(AssertionError::new);
        provider.book().eraseRecord(217L);

        assertEquals(400d, operations.operationSum(0, 10000, false), DELTA);
        assertEquals(1, plannedOperations.plannedSeries().size());

        plannedOperations.plan(new PlannedOperationSeriesImpl(
                "something weird",
                new OperationImpl(500, 1d),
                new ConstantIterator(10),
                100)
        );

        plannedOperations = provider.plannedOperations();
        operations = provider.operations();

        assertEquals(2, plannedOperations.plannedSeries().size());
        checkSize(plannedOperations, 2, "Income");
        assertEquals(102, plannedOperations.plannedSeries().stream().mapToInt(PlannedOperationSeries::size).sum());
        plannedOperations.plannedSeries().forEach(s -> s.processSeries(Assertions::assertNotNull));
        assertEquals(500d, operations.operationSum(0, 10000, false), DELTA);

        //now deletions
        checkSize(plannedOperations, 100, "something weird");
        provider.book().eraseRecord(1490); //last one
        assertEquals(499d, operations.operationSum(0, 10000, false), DELTA);
        checkSize(plannedOperations, 99, "something weird");

        provider.book().eraseRecord(590); //somewhere in the middle
        assertEquals(498d, operations.operationSum(0, 10000, false), DELTA);
        checkSize(plannedOperations, 98, "something weird");

        if (deleteWeirdStuff) {
            plannedOperations.delete("something weird");
        }

        finalCheck();
    }

    public void finalCheck() {
        PlannedOperations plannedOperations = provider.plannedOperations();
        Operations operations = provider.operations();
        if (deleteWeirdStuff) {
            assertEquals(400d, operations.operationSum(0, 10000, false), DELTA);
            assertEquals(1, plannedOperations.plannedSeries().size());
        } else {
            assertEquals(498d, operations.operationSum(0, 10000, false), DELTA);
            assertEquals(2, plannedOperations.plannedSeries().size());
            plannedOperations.plannedSeries().forEach(
                    s -> assertTrue(s.name().equals("Income") || s.name().equals("something weird")));
        }
    }

    private void checkSize(PlannedOperations plannedOperations, int expected, String seriesName) throws Throwable {
        plannedOperations.plannedSeries().stream().filter(s -> s.name().equals(seriesName)).findAny()
                .map(s -> {
                    assertEquals(expected, s.size());
                    return 0;
                }).orElseThrow(AssertionError::new);
    }
}
