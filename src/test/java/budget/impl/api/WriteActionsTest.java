package budget.impl.api;

import budget.impl.model.BalanceReportsImpl;
import budget.impl.model.BookSchedule;
import budget.impl.model.OperationsImpl;
import budget.impl.model.entity.BalanceReportImpl;
import budget.impl.model.entity.OperationImpl;
import budget.model.BalanceReport;
import budget.model.Operation;
import budget.model.Schedule;
import budget.model.book.BalanceRecordBook;
import budget.model.book.OperationRecordBook;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @since 29.04.2017
 */
class WriteActionsTest {
    private static final class FreeSchedule implements Schedule {
        @Override
        public boolean busy(long time) {
            return false;
        }
    }

    @Test
    void testInconsistentSchedule() {
        Operation operation = new OperationImpl(100, 100);
        assertThrows(IllegalStateException.class, () -> new LogOperationActionImpl(
                new OperationsImpl(Collections.singleton(operation)),
                new BalanceReportsImpl(),
                new FreeSchedule())
                .writeWithBalance(100, 500, 100));
    }

    @Test
    void testInconsistentSchedule2() {
        BalanceReport report = new BalanceReportImpl(101, 100);
        Operation operation = new OperationImpl(99, 100);
        assertThrows(IllegalStateException.class, () -> new LogOperationActionImpl(
                new OperationsImpl(Collections.singleton(operation)),
                new BalanceReportsImpl(Collections.singleton(report)),
                new FreeSchedule())
                .writeWithBalance(100, 500, 100));
    }

    @Test
    void testInconsistentSchedule3() {
        BalanceReport report = new BalanceReportImpl(100, 100);
        assertThrows(IllegalStateException.class, () -> new LogBalanceActionImpl(
                new BalanceReportsImpl(Collections.singleton(report)),
                new FreeSchedule())
                .write(100, 500));
    }

    @Test
    void testBookScheduleShift() {
        OperationRecordBook operationBook = new OperationsImpl(Collections.singleton(new OperationImpl(100, 100)));
        BalanceRecordBook balanceBook = new BalanceReportsImpl();
        BookSchedule schedule = new BookSchedule(operationBook, balanceBook);
        new LogOperationActionImpl(operationBook, balanceBook, schedule).writeWithBalance(100, 500, 100);

        assertTrue(operationBook.hasRecord(100));
        assertTrue(operationBook.hasRecord(101));
        assertTrue(balanceBook.hasRecord(102));
    }

    @Test
    void testBookScheduleShift2() {
        OperationRecordBook operationBook = new OperationsImpl(Collections.singleton(new OperationImpl(100, 100)));
        BalanceRecordBook balanceBook = new BalanceReportsImpl(Collections.singleton(new BalanceReportImpl(101, 100)));
        BookSchedule schedule = new BookSchedule(operationBook, balanceBook);
        new LogOperationActionImpl(operationBook, balanceBook, schedule).writeWithBalance(100, 500, 100);

        assertTrue(operationBook.hasRecord(100));
        assertTrue(balanceBook.hasRecord(101));
        assertTrue(operationBook.hasRecord(102));
        assertTrue(balanceBook.hasRecord(103));
    }
}
