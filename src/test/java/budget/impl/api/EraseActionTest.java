package budget.impl.api;

import budget.impl.model.BalanceReportsImpl;
import budget.impl.model.BookSchedule;
import budget.impl.model.OperationsImpl;
import budget.impl.model.entity.BalanceReportImpl;
import budget.impl.model.entity.OperationImpl;
import budget.model.book.BalanceRecordBook;
import budget.model.book.OperationRecordBook;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @since 29.04.2017
 */
class EraseActionTest {
    @Test
    void testEraseTransactions() {
        OperationRecordBook operationBook = new OperationsImpl(new OperationImpl(100, 100));
        BalanceRecordBook balanceBook = new BalanceReportsImpl();

        assertTrue(operationBook.hasRecord(100));
        new EraseRecordActionImpl(operationBook, balanceBook).erase(100);
        assertFalse(operationBook.hasRecord(100));
    }

    @Test
    void testEraseBalance() {
        OperationRecordBook operationBook = new OperationsImpl();
        BalanceRecordBook balanceBook = new BalanceReportsImpl(Collections.singleton(new BalanceReportImpl(101, 100)));

        assertTrue(balanceBook.hasRecord(101));
        new EraseRecordActionImpl(operationBook, balanceBook).erase(101);
        assertFalse(balanceBook.hasRecord(101));
    }

    @Test
    void testEraseNothing() {
        OperationRecordBook operationBook = new OperationsImpl(Collections.singleton(new OperationImpl(100, 100)));
        BalanceRecordBook balanceBook = new BalanceReportsImpl(Collections.singleton(new BalanceReportImpl(101, 100)));

        assertTrue(operationBook.hasRecord(100));
        assertTrue(balanceBook.hasRecord(101));
        new EraseRecordActionImpl(operationBook, balanceBook).erase(99);
        new EraseRecordActionImpl(operationBook, balanceBook).erase(102);
        assertTrue(operationBook.hasRecord(100));
        assertTrue(balanceBook.hasRecord(101));
    }

    @Test
    void testEraseBoth() {
        OperationRecordBook operationBook = new OperationsImpl(Collections.singleton(new OperationImpl(100, 100)));
        BalanceRecordBook balanceBook = new BalanceReportsImpl(Collections.singleton(new BalanceReportImpl(100, 100)));

        assertTrue(operationBook.hasRecord(100));
        assertTrue(balanceBook.hasRecord(100));
        new EraseRecordActionImpl(operationBook, balanceBook).erase(100);
        assertFalse(operationBook.hasRecord(100));
        assertFalse(balanceBook.hasRecord(100));
    }

    @Test
    void testBookScheduleShift2() {
        OperationRecordBook operationBook = new OperationsImpl(Collections.singleton(new OperationImpl(100, 100)));
        BalanceRecordBook balanceBook = new BalanceReportsImpl(Collections.singleton(new BalanceReportImpl(101, 100)));
        BookSchedule schedule = new BookSchedule(operationBook, balanceBook);
        new LogOperationActionImpl(operationBook, balanceBook, schedule).writeWithBalance(100, 500, 100);

        assertTrue(operationBook.hasRecord(100));
        assertTrue(balanceBook.hasRecord(101));
        assertTrue(operationBook.hasRecord(102));
        assertTrue(balanceBook.hasRecord(103));
    }
}
