package budget.impl.api;

import budget.impl.model.OpenReferencePeriodDecorator;
import budget.impl.model.entity.ReferencePeriodImpl;
import budget.model.ReferencePeriod;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @since 28.04.2017
 */
class OpenReferencePeriodTest {
    private static ReferencePeriod original;
    private static ReferencePeriod decorator;

    @BeforeAll
    static void prepare() {
        original = new ReferencePeriodImpl(15, 25, 100);
        decorator = new OpenReferencePeriodDecorator(original);
    }

    @Test
    void testOpenReferencePeriodCorrect() {
        assertEquals(15, decorator.start());
        assertEquals(25, decorator.end());
        assertEquals(100, decorator.availableMargin());
    }

    @Test
    void testOpenReferencePeriodSameAsOriginal() {
        assertEquals(original.start(), decorator.start());
        assertEquals(original.end(), decorator.end());
        assertEquals(original.availableMargin(), decorator.availableMargin());
    }

    @Test
    void testOpenReferencePeriodExternalTime() {
        assertFalse(original.includes(0));
        assertEquals(ReferencePeriod.Relation.PERIOD_IS_AFTER, original.relatesTo(0));
        assertFalse(decorator.includes(0));
        assertEquals(ReferencePeriod.Relation.PERIOD_IS_AFTER, decorator.relatesTo(0));

        assertFalse(original.includes(100));
        assertEquals(ReferencePeriod.Relation.PERIOD_IS_BEFORE, original.relatesTo(100));
        assertFalse(decorator.includes(100));
        assertEquals(ReferencePeriod.Relation.PERIOD_IS_BEFORE, decorator.relatesTo(100));
    }

    @Test
    void testOpenReferencePeriodInternalTime() {
        assertTrue(original.includes(20));
        assertEquals(ReferencePeriod.Relation.PERIOD_INCLUDES, original.relatesTo(20));
        assertTrue(decorator.includes(20));
        assertEquals(ReferencePeriod.Relation.PERIOD_INCLUDES, decorator.relatesTo(20));
    }

    @Test
    void testOpenReferencePeriodStart() {
        assertTrue(original.includes(15));
        assertEquals(ReferencePeriod.Relation.PERIOD_INCLUDES, original.relatesTo(15));
        assertFalse(decorator.includes(15));
        assertEquals(ReferencePeriod.Relation.PERIOD_IS_AFTER, decorator.relatesTo(15));
    }

    @Test
    void testOpenReferencePeriodEnd() {
        assertFalse(original.includes(25));
        assertEquals(ReferencePeriod.Relation.PERIOD_IS_BEFORE, original.relatesTo(25));
        assertFalse(decorator.includes(25));
        assertEquals(ReferencePeriod.Relation.PERIOD_IS_BEFORE, decorator.relatesTo(25));
    }
}
