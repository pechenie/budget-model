package budget.impl.app;

import budget.api.Dashboard;
import budget.api.Journal;
import budget.api.RegularOperations;
import budget.api.entity.RecurrenceInput;
import budget.api.entity.RuleTemplate;
import budget.api.entity.RuleView;
import budget.app.BudgetModel;
import budget.impl.model.BalanceReportsImpl;
import budget.impl.model.OperationsImpl;
import budget.impl.model.RecordTagDomain;
import budget.impl.model.future.ReferencePeriodsFactory;
import budget.impl.model.period.PeriodicReferencePeriodFactory;
import budget.impl.model.persistence.JournalPersistence;
import budget.impl.model.persistence.PeriodRulesPersistence;
import budget.impl.model.tag.TagsImpl;
import budget.impl.model.time.ConstantIterator;
import budget.impl.model.time.DailyIterator;
import budget.impl.model.time.MonthlyIterator;
import budget.impl.model.time.WeeklyIterator;
import budget.model.BalanceReport;
import budget.model.Operation;
import budget.model.ReferencePeriod;
import budget.model.UpdateListener;
import budget.model.time.TimestampIterator;
import budget.view.journal.properties.BalanceReportProperties;
import budget.view.journal.properties.ExecutedOperationProperties;
import budget.view.journal.properties.GroupProperties;
import budget.view.journal.properties.ItemPropertiesVisitor;
import budget.view.journal.properties.JournalProperties;
import budget.view.journal.properties.PlannedOperationProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * @author Kirill Berezin
 */
class BudgetModelImplTest {

    private interface RecurrenceView extends RuleView {
    }

    private interface RuleTemplateView extends RuleView {
    }

    private interface RuleTemplateInput {
    }

    @Test
    void motherOfAllTests() {
        JournalPersistenceImpl journalPersistence = new JournalPersistenceImpl();
        RulesPersistenceImpl rulesPersistence = new RulesPersistenceImpl();
        List<RuleTemplate<RuleTemplateView, RuleTemplateInput, ReferencePeriodsFactory>> templates =
                Collections.singletonList(new TemplateImpl(new ViewImpl("abc"),
                        new PeriodicReferencePeriodFactory(new ConstantIterator(100), 1000d,
                                fromTime -> (time -> time + fromTime >= 100))
                ));

        RecurrenceRuleImpl daily = new RecurrenceRuleImpl("daily", new DailyIterator(1));
        RecurrenceRuleImpl weekly = new RecurrenceRuleImpl("weekly", new WeeklyIterator(1));
        RecurrenceRuleImpl monthly = new RecurrenceRuleImpl("monthly", new MonthlyIterator(1));

        TagsImpl tags = new TagsImpl(new HashMap<>());
        BudgetModel<RecurrenceView, RecurrenceInput, RuleTemplateView, RuleTemplateInput> model
                = new BudgetModelImpl<>(journalPersistence, rulesPersistence, templates, Arrays.asList(daily, weekly, monthly), tags);

        CountingListener updateListener = new CountingListener();
        model.addListener(updateListener);
        //call on listener addition
        assertEquals(1, updateListener.count);

        //now test!
        RegularOperations<RecurrenceView, RecurrenceInput> regularOperations = model.regularOperations();

        regularOperations.planSeries("foo", -100d, 10, daily.display(), new RecurrenceInputImpl(1));
        assertEquals(2, updateListener.count);

        //add some periods finally!
        RuleTemplateView ruleView = model.periodRules().templates().get(0).display();
        assertEquals("abc", ruleView.id());
        model.periodRules().applyNewRuleSince(0, true, ruleView, new TestInput());
        assertNotNull(rulesPersistence.savedId());
        assertNotNull(rulesPersistence.savedInput());

        assertEquals(3, updateListener.count);

        Journal journal = model.journal();
        assertEquals(1, journal.display().groupsCount());
        assertEquals(1, journal.display().groupAt(0).itemsCount());

        Dashboard dashboard = model.dashboard();

        //check log operation (+2 updates)
        journal.logOperation().writeWithBalance(1, 1000d, 1000d);
        assertEquals(5, updateListener.count);

        assertSame(model.journal(), journal);
        assertSame(model.dashboard(), dashboard);
        assertSame(model.regularOperations(), regularOperations);

        journal = model.journal();
        dashboard = model.dashboard();
        regularOperations = model.regularOperations();

        //check log operation and balance (+2 updates)
        journal.logOperation().writeWithBalance(5, -100d, 880d);
        assertEquals(7, updateListener.count);

        regularOperations.planSeries("huge income", 2000d, 99, daily.display(), new RecurrenceInputImpl(1));
        assertEquals(8, updateListener.count);

        assertSame(model.journal(), journal);
        assertSame(model.dashboard(), dashboard);
        assertSame(model.regularOperations(), regularOperations);

        assertEquals(880d, dashboard.estimatedBalanceAtTime(6));
        assertEquals(780d, dashboard.estimatedBalanceAtTime(50));

        assertEquals(880d, dashboard.usableMarginAtTime(6));
        assertEquals(780d, dashboard.usableMarginAtTime(50));

        assertEquals(10L, dashboard.minBalanceSinceLastReport().time());
        assertEquals(780d, dashboard.minBalanceSinceLastReport().balance());

        assertEquals(880d, dashboard.lastReportedBalanceForTime(10).balance());
        assertEquals(6L, dashboard.lastReportedBalanceForTime(10).time());

        //check notified after series removal
        regularOperations.deleteSeries("foo");
        assertEquals(9, updateListener.count);

        assertSame(model.journal(), journal);
        assertSame(model.dashboard(), dashboard);
        assertSame(model.regularOperations(), regularOperations);

        //check remove update listener
        model.removeListener(updateListener);
        journal.logBalance().write(90, 750d);
        assertEquals(9, updateListener.count);

        //check if restore from persistence works
        model = new BudgetModelImpl<>(journalPersistence, rulesPersistence, templates,
                Arrays.asList(daily, weekly, monthly), tags);
        journal = model.journal();
        dashboard = model.dashboard();
        assertEquals(1, journal.display().groupsCount());
        assertEquals(6, journal.display().groupAt(0).itemsCount());
        assertEquals(880d, dashboard.estimatedBalanceAtTime(6));
        assertEquals(880d, dashboard.estimatedBalanceAtTime(50));
        assertEquals(880d, dashboard.usableMarginAtTime(6));
        assertEquals(880d, dashboard.usableMarginAtTime(50));
        assertEquals(750d, dashboard.minBalanceSinceLastReport().balance());
        assertEquals(90L, dashboard.minBalanceSinceLastReport().time());
        assertEquals(880d, dashboard.lastReportedBalanceForTime(10).balance());
        assertEquals(6L, dashboard.lastReportedBalanceForTime(10).time());

        //more rules now
        model.periodRules().updatePeriodMargin(45, 500d);
        assertEquals(480d, dashboard.usableMarginAtTime(6));
        assertEquals(480d, dashboard.usableMarginAtTime(50));
        model.periodRules().updatePeriodMargin(45, 1000d);
        assertEquals(880d, dashboard.usableMarginAtTime(6));
        assertEquals(880d, dashboard.usableMarginAtTime(50));

        //custom tags
        model.customTags().tagWithCustomName(123123L, "bar");
        assertNotNull(tags.tagBy(123123L));
        assertEquals("bar", tags.tagBy(123123L).valueFor(new RecordTagDomainImpl().customTitleKey()));
    }

    @Test
    void journalDecoration() {
        JournalPersistenceImpl journalPersistence = new JournalPersistenceImpl();
        RulesPersistenceImpl rulesPersistence = new RulesPersistenceImpl();
        List<RuleTemplate<RuleTemplateView, RuleTemplateInput, ReferencePeriodsFactory>> templates =
                Collections.singletonList(new TemplateImpl(new ViewImpl("abc"),
                        new PeriodicReferencePeriodFactory(new ConstantIterator(100), 1000d,
                                fromTime -> (time -> time + fromTime >= 300))
                ));

        RecurrenceRuleImpl onceIn5Ms = new RecurrenceRuleImpl("onceIn5Ms", new ConstantIterator(5));
        RecurrenceRuleImpl weekly = new RecurrenceRuleImpl("weekly", new WeeklyIterator(1));
        RecurrenceRuleImpl monthly = new RecurrenceRuleImpl("monthly", new MonthlyIterator(1));

        TagsImpl tags = new TagsImpl(new HashMap<>());
        BudgetModel<RecurrenceView, RecurrenceInput, RuleTemplateView, RuleTemplateInput> model = new BudgetModelImpl<>(journalPersistence,
                rulesPersistence, templates, Arrays.asList(onceIn5Ms, weekly, monthly), tags);

        model.periodRules().applyNewRuleSince(0, true, templates.get(0).display(), new TestInput());
        model.regularOperations().planSeries("foo", 10d, 125, onceIn5Ms.view, new RecurrenceInputImpl(3));
        model.journal().logOperation().writeWithBalance(0, 1000d, 1000d);
        model.journal().logOperation().writeWithBalance(101, 900d, 800d);

        RecordTagDomain domain = new RecordTagDomainImpl();
        tags.update(tags.tagBy(101).withEntry(domain.customTitleKey(), "Salary"));

        JournalProperties properties = model.journalProperties(model.journal().display(), 105);

        assertEquals(3, properties.groupsCount());
        {
            GroupProperties group0 = properties.groupPropertiesAt(0);
            assertEquals(GroupProperties.Presence.PAST, group0.presence());
            assertEquals(2, group0.itemsCount());
            group0.itemPropertiesAt(0).visitBy(new ItemPropertiesVisitor<Object>() {
                @Override
                public Object visit(BalanceReportProperties properties) {
                    fail("shouldn't happen");
                    return null;
                }

                @Override
                public Object visit(ExecutedOperationProperties properties) {
                    assertEquals("", properties.name());
                    return null;
                }

                @Override
                public Object visit(PlannedOperationProperties properties) {
                    fail("shouldn't happen");
                    return null;
                }
            });
            group0.itemPropertiesAt(1).visitBy(new ItemPropertiesVisitor<Object>() {
                @Override
                public Object visit(BalanceReportProperties properties) {
                    assertEquals("", properties.name());
                    return null;
                }

                @Override
                public Object visit(ExecutedOperationProperties properties) {
                    fail("shouldn't happen");
                    return null;
                }

                @Override
                public Object visit(PlannedOperationProperties properties) {
                    fail("shouldn't happen");
                    return null;
                }
            });
        }

        {
            GroupProperties group1 = properties.groupPropertiesAt(1);
            assertEquals(GroupProperties.Presence.PRESENT, group1.presence());
            assertEquals(5, group1.itemsCount());
            group1.itemPropertiesAt(0).visitBy(new ItemPropertiesVisitor<Object>() {
                @Override
                public Object visit(BalanceReportProperties properties) {
                    fail("shouldn't happen");
                    return null;
                }

                @Override
                public Object visit(ExecutedOperationProperties properties) {
                    assertEquals("Salary", properties.name());
                    return null;
                }

                @Override
                public Object visit(PlannedOperationProperties properties) {
                    fail("shouldn't happen");
                    return null;
                }
            });
            group1.itemPropertiesAt(1).visitBy(new ItemPropertiesVisitor<Object>() {
                @Override
                public Object visit(BalanceReportProperties properties) {
                    assertEquals("", properties.name());
                    return null;
                }

                @Override
                public Object visit(ExecutedOperationProperties properties) {
                    fail("shouldn't happen");
                    return null;
                }

                @Override
                public Object visit(PlannedOperationProperties properties) {
                    fail("shouldn't happen");
                    return null;
                }
            });
            for (int i = 2; i < 5; ++i) {
                group1.itemPropertiesAt(i).visitBy(new ItemPropertiesVisitor<Object>() {
                    @Override
                    public Object visit(BalanceReportProperties properties) {
                        fail("shouldn't happen");
                        return null;
                    }

                    @Override
                    public Object visit(ExecutedOperationProperties properties) {
                        fail("shouldn't happen");
                        return null;
                    }

                    @Override
                    public Object visit(PlannedOperationProperties properties) {
                        assertEquals("foo", properties.name());
                        assertEquals(125L, properties.chainStartTime());
                        return null;
                    }
                });
            }
        }

        {
            GroupProperties group2 = properties.groupPropertiesAt(2);
            assertEquals(GroupProperties.Presence.FUTURE, group2.presence());
        }

        properties = model.journalProperties(model.journal().display(), 10000);
        for (int i = 0; i < properties.groupsCount() - 1; ++i) {
            assertEquals(GroupProperties.Presence.PAST, properties.groupPropertiesAt(i).presence());
        }
        assertEquals(GroupProperties.Presence.PRESENT, properties.groupPropertiesAt(properties.groupsCount() - 1).presence());
    }

    private static class CountingListener implements UpdateListener {
        private int count;

        @Override
        public void notifyUpdated() {
            count++;
        }
    }

    private static class TemplateImpl implements RuleTemplate<RuleTemplateView, RuleTemplateInput, ReferencePeriodsFactory> {
        private final ViewImpl view;
        private final ReferencePeriodsFactory factory;

        private TemplateImpl(ViewImpl view, ReferencePeriodsFactory factory) {
            this.view = view;
            this.factory = factory;
        }

        @NotNull
        @Override
        public ViewImpl display() {
            return view;
        }

        @NotNull
        @Override
        public ReferencePeriodsFactory toRule(RuleTemplateInput data) {
            return factory;
        }
    }

    private static class ViewImpl implements RuleTemplateView {
        private final String id;

        private ViewImpl(String id) {
            this.id = id;
        }

        @NotNull
        @Override
        public String id() {
            return id;
        }
    }

    private static class JournalPersistenceImpl implements JournalPersistence {
        private final BalanceReportsImpl reports = new BalanceReportsImpl();
        private final OperationsImpl operations = new OperationsImpl();
        private Collection<? extends ReferencePeriod> periods = new ArrayList<>();

        @Override
        public void store(@NotNull Operation operation) {
            assertTrue(operations.execute(operation));
        }

        @Override
        public void store(@NotNull BalanceReport balanceReport) {
            assertTrue(reports.report(balanceReport));
        }

        @Override
        public void replaceAll(@NotNull Collection<? extends ReferencePeriod> referencePeriods) {
            periods = referencePeriods;
        }

        @Override
        public void remove(long recordAtTime) {
            reports.eraseRecord(recordAtTime);
            operations.eraseRecord(recordAtTime);
        }

        @NotNull
        @Override
        public Collection<? extends Operation> readOperations() {
            return operations.range(Long.MIN_VALUE, true, Long.MAX_VALUE, true);
        }

        @NotNull
        @Override
        public Collection<? extends BalanceReport> readReports() {
            return reports.range(Long.MIN_VALUE, true, Long.MAX_VALUE, true);
        }

        @NotNull
        @Override
        public Collection<? extends ReferencePeriod> readPeriods() {
            return periods;
        }
    }

    private static class RulesPersistenceImpl implements PeriodRulesPersistence<RuleTemplateView, RuleTemplateInput> {
        private String id;
        private RuleTemplateInput input;

        @Override
        public void save(@NotNull RuleTemplateView view, @NotNull RuleTemplateInput input) {
            this.id = view.id();
            this.input = input;
        }

        @Nullable
        @Override
        public String savedId() {
            return id;
        }

        @Nullable
        @Override
        public RuleTemplateInput savedInput() {
            return input;
        }
    }

    private static class TestInput implements RuleTemplateInput {
    }

    private static final class RecurrenceViewImpl implements RecurrenceView {
        private final String id;

        RecurrenceViewImpl(String id) {
            this.id = id;
        }

        @NotNull
        @Override
        public String id() {
            return id;
        }
    }

    private static final class RecurrenceInputImpl implements RecurrenceInput {

        private final int count;

        RecurrenceInputImpl(int count) {
            this.count = count;
        }

        @Override
        public int operationsCount() {
            return count;
        }
    }

    private static class RecurrenceRuleImpl implements RuleTemplate<RecurrenceView, RecurrenceInput, TimestampIterator> {

        private final RecurrenceViewImpl view;
        private final TimestampIterator iterator;

        private RecurrenceRuleImpl(String id, TimestampIterator iterator) {
            this.view = new RecurrenceViewImpl(id);
            this.iterator = iterator;
        }

        @NotNull
        @Override
        public RecurrenceViewImpl display() {
            return view;
        }

        @NotNull
        @Override
        public TimestampIterator toRule(RecurrenceInput data) {
            return iterator;
        }
    }
}
