package budget.impl.entity;

import java.util.function.Consumer;

/**
 * Helper class for holding a consumed entity
 *
 * @author Kirill Berezin
 */
public class HoldingConsumer<T> implements Consumer<T> {
    private T obj;

    @Override
    public void accept(T t) {
        obj = t;
    }

    public T held() {
        return obj;
    }
}
