package budget.impl.api;

import budget.api.action.EraseRecordAction;
import budget.model.book.RecordBook;

import java.util.Arrays;
import java.util.Collection;

/**
 * Default erase action implementation
 *
 * @since 29.04.2017
 */
class EraseRecordActionImpl implements EraseRecordAction {
    private final Collection<RecordBook> books;

    EraseRecordActionImpl(RecordBook... books) {
        this.books = Arrays.asList(books);
    }

    @Override
    public void erase(long time) {
        books.forEach(b -> b.eraseRecord(time));
    }
}
