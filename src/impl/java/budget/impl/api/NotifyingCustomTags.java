package budget.impl.api;

import budget.api.CustomTags;
import budget.model.UpdateListener;
import org.jetbrains.annotations.NotNull;

/**
 * Notifying custom tags decorator
 *
 * @author Kirill Berezin
 */

public class NotifyingCustomTags implements CustomTags {
    private final UpdateListener updateListener;
    private final CustomTags customTags;

    public NotifyingCustomTags(@NotNull UpdateListener updateListener, @NotNull CustomTags customTags) {
        this.updateListener = updateListener;
        this.customTags = customTags;
    }

    @Override
    public void tagWithCustomName(long time, @NotNull String name) {
        customTags.tagWithCustomName(time, name);
        updateListener.notifyUpdated();
    }
}
