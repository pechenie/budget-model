package budget.impl.api;

import budget.api.action.LogOperationAction;
import budget.impl.model.entity.BalanceReportImpl;
import budget.impl.model.entity.OperationImpl;
import budget.model.Schedule;
import budget.model.book.BalanceRecordBook;
import budget.model.book.OperationRecordBook;
import org.jetbrains.annotations.NotNull;

/**
 * Default operation logging action. Picks a free space on a timeline for both operation and balance report records using schedule.
 * Records are guaranteed to be placed sequentially
 *
 * @since 29.04.2017
 */
class LogOperationActionImpl implements LogOperationAction {
    private final OperationRecordBook operationBook;
    private final BalanceRecordBook balanceBook;
    private final Schedule schedule;

    LogOperationActionImpl(@NotNull OperationRecordBook operationBook, @NotNull BalanceRecordBook balanceBook, @NotNull Schedule schedule) {
        this.operationBook = operationBook;
        this.balanceBook = balanceBook;
        this.schedule = schedule;
    }

    @Override
    public long writeWithBalance(long time, double operationCost, double remainingCash) {
        while (schedule.busy(time) || schedule.busy(time + 1)) {
            time++;
        }

        if (!operationBook.execute(new OperationImpl(time, operationCost))) {
            throw new IllegalStateException("Inconsistent schedule!");
        }
        if (!balanceBook.report(new BalanceReportImpl(time + 1, remainingCash))) {
            throw new IllegalStateException("Inconsistent schedule!");
        }
        return time;
    }
}
