package budget.impl.api;

import budget.api.RegularOperations;
import budget.api.entity.RecurrenceInput;
import budget.api.entity.RuleTemplate;
import budget.api.entity.RuleView;
import budget.impl.entity.HoldingConsumer;
import budget.impl.model.entity.OperationImpl;
import budget.impl.model.operation.PlannedOperationSeriesImpl;
import budget.impl.view.operation.RegularOperationItemViewImpl;
import budget.impl.view.operation.RegularOperationsViewImpl;
import budget.model.Operation;
import budget.model.operation.PlannedOperationSeries;
import budget.model.operation.PlannedOperations;
import budget.model.time.TimestampIterator;
import budget.view.TimedCost;
import budget.view.operation.RegularOperationItemView;
import budget.view.operation.RegularOperationsView;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.Comparator;
import java.util.stream.Collectors;

/**
 * Base implementation of the regular operations. Set of templates to be provided by a subclass
 *
 * @author Kirill Berezin
 */

public abstract class RegularOperationsImpl<View extends RuleView, RegOpsInp extends RecurrenceInput>
        implements RegularOperations<View, RegOpsInp> {
    private final PlannedOperations plannedOperations;

    protected RegularOperationsImpl(PlannedOperations plannedOperations) {
        this.plannedOperations = plannedOperations;
    }

    @Override
    public abstract Collection<? extends RuleTemplate<View, RegOpsInp, TimestampIterator>> occurrenceTemplates();

    @NotNull
    @Override
    public RegularOperationsView displayAll() {
        return displayWithin(Long.MAX_VALUE);
    }

    @NotNull
    @Override
    public RegularOperationsView displayWithin(long time) {
        return new RegularOperationsViewImpl(plannedOperations.plannedSeries().stream()
                .map(this::viewFromSeries)
                .filter(v -> v.next().time() <= time)
                .sorted(Comparator.comparingLong(v -> v.next().time()))
                .collect(Collectors.toList()));
    }

    private RegularOperationItemView viewFromSeries(PlannedOperationSeries series) {
        HoldingConsumer<Operation> holdingConsumer = new HoldingConsumer<>();
        series.processSeries(holdingConsumer);
        Operation next = holdingConsumer.held();
        return new RegularOperationItemViewImpl(series.name(), new TimedCostImpl(next.time(), next.cost()), series.size());
    }


    @Override
    public void planSeries(@NotNull String name, double cost, long startTime, @NotNull View view, @NotNull RegOpsInp input) {
        occurrenceTemplates().stream().filter(t -> t.display().id().equals(view.id())).findAny().ifPresent(t ->
                plannedOperations.plan(new PlannedOperationSeriesImpl(name, new OperationImpl(startTime, cost),
                        t.toRule(input), input.operationsCount()))
        );

    }

    @Override
    public void deleteSeries(@NotNull String name) {
        plannedOperations.delete(name);
    }

    private static final class TimedCostImpl implements TimedCost {
        private final long time;
        private final double cost;

        private TimedCostImpl(long time, double cost) {
            this.time = time;
            this.cost = cost;
        }

        @Override
        public long time() {
            return time;
        }

        @Override
        public double cost() {
            return cost;
        }
    }
}
