package budget.impl.api;

import budget.api.PeriodRules;
import budget.api.entity.RuleTemplate;
import budget.api.entity.RuleView;
import budget.impl.model.entity.ReferencePeriodImpl;
import budget.impl.model.future.ReferencePeriodsFactory;
import budget.model.ReferencePeriod;
import budget.model.book.PeriodBook;
import budget.model.source.ReferencePeriods;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Base class for default period rules. Rule is a {@link ReferencePeriodsFactory}
 * @author Kirill Berezin
 */
public abstract class FactoryPeriodRules<View extends RuleView, Input> implements PeriodRules<View, Input> {
    private final ReferencePeriods referencePeriods;
    private final PeriodBook periodBook;

    public FactoryPeriodRules(ReferencePeriods referencePeriods, PeriodBook periodBook) {
        this.referencePeriods = referencePeriods;
        this.periodBook = periodBook;
    }

    @Override
    public abstract List<? extends RuleTemplate<View, Input, ReferencePeriodsFactory>> templates();

    @Override
    public void applyNewRuleSince(long time, boolean immediate, View view, Input input) {
        List<ReferencePeriod> newPeriods = new ArrayList<>();

        ReferencePeriod intersectingPeriod = referencePeriods.periodAtTime(time);
        if (intersectingPeriod != null) {
            //we are in the middle of some period
            if (immediate) {
                //replacement
                newPeriods.add(new ReferencePeriodImpl(intersectingPeriod.start(), time, intersectingPeriod.availableMargin()));
            } else {
                time = intersectingPeriod.end();
            }
        }

        long startingTime = time;
        RuleTemplate<View, Input, ReferencePeriodsFactory> found = null;
        for (RuleTemplate<View, Input, ReferencePeriodsFactory> viewInputReferencePeriodsFactoryRuleTemplate : templates()) {
            if (viewInputReferencePeriodsFactoryRuleTemplate.display().id().equals(view.id())) {
                found = viewInputReferencePeriodsFactoryRuleTemplate;
                break;
            }
        }

        if (found == null) {
            throw new IllegalArgumentException("Unknown view with id = " + view.id());
        }
        newPeriods.addAll(found.toRule(input).calculateWithStartingTime(startingTime));
        if (!periodBook.rewrite(newPeriods)) {
            //this is a client responsibility to check correctness
            throw new IllegalStateException("Incorrect rules application");
        }
    }

    @Override
    public void updatePeriodMargin(long time, double margin) {
        ReferencePeriod period = referencePeriods.periodAtTime(time);
        if (period != null) {
            if (!periodBook.rewrite(Collections.singletonList(new ReferencePeriodImpl(period.start(), period.end(), margin)))) {
                //shouldn't normally happen
                throw new IllegalStateException("Couldn't update margin at " + time);
            }
        }
    }
}
