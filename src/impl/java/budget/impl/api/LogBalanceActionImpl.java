package budget.impl.api;

import budget.api.action.LogBalanceAction;
import budget.impl.model.entity.BalanceReportImpl;
import budget.model.Schedule;
import budget.model.book.BalanceRecordBook;
import org.jetbrains.annotations.NotNull;

/**
 * Default balance reporting action implementation. Picks a closest free space on a timeline using a schedule
 *
 * @since 29.04.2017
 */
class LogBalanceActionImpl implements LogBalanceAction {
    private final BalanceRecordBook book;
    private final Schedule schedule;

    LogBalanceActionImpl(@NotNull BalanceRecordBook book, @NotNull Schedule schedule) {
        this.book = book;
        this.schedule = schedule;
    }

    @Override
    public long write(long time, double cash) {
        while (schedule.busy(time)) {
            time++;
        }

        if (!book.report(new BalanceReportImpl(time, cash))) {
            throw new IllegalStateException("Inconsistent schedule!");
        }
        return time;
    }
}
