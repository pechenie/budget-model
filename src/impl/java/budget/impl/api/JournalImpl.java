package budget.impl.api;

import budget.api.Journal;
import budget.api.action.EraseRecordAction;
import budget.api.action.LogBalanceAction;
import budget.api.action.LogOperationAction;
import budget.impl.model.BookSchedule;
import budget.impl.model.metrics.TimedMetricsImpl;
import budget.impl.view.TimedBalanceWithMarginImpl;
import budget.impl.view.journal.BalanceReportViewImpl;
import budget.impl.view.journal.JournalViewImpl;
import budget.impl.view.journal.OperationViewImpl;
import budget.impl.view.journal.PeriodGroupViewImpl;
import budget.model.BalanceReport;
import budget.model.Operation;
import budget.model.ReferencePeriod;
import budget.model.TimedMetrics;
import budget.model.book.BalanceRecordBook;
import budget.model.book.OperationRecordBook;
import budget.model.source.BalanceReports;
import budget.model.source.Operations;
import budget.model.source.ReferencePeriods;
import budget.view.TimedBalanceWithMargin;
import budget.view.journal.GroupItem;
import budget.view.journal.JournalView;
import budget.view.journal.PeriodGroupView;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Default journal implementation
 *
 * @since 27.04.2017
 */
public class JournalImpl implements Journal {
    private final ReferencePeriods referencePeriods;
    private final BalanceRecordBook balanceBook;
    private final OperationRecordBook operationBook;
    private final TimedMetrics timedMetrics;
    private final BookSchedule schedule;

    public JournalImpl(BalanceReports reports, Operations operations, ReferencePeriods referencePeriods,
                       BalanceRecordBook balanceBook, OperationRecordBook operationBook) {
        this.referencePeriods = referencePeriods;

        this.balanceBook = balanceBook;
        this.operationBook = operationBook;
        this.schedule = new BookSchedule(balanceBook, operationBook);

        this.timedMetrics = new TimedMetricsImpl(operations, reports, referencePeriods);
    }

    @NotNull
    @Override
    public JournalView display() {
        CollectingVisitor visitor = new CollectingVisitor();

        ReferencePeriod range = referencePeriods.unite();
        timedMetrics.traverse(visitor, range.start(), range.end());

        return new JournalViewImpl(visitor.periods);
    }

    //intentionally procedural
    private static final class CollectingVisitor implements TimedMetrics.EntryVisitor {
        private final List<PeriodGroupView> periods = new ArrayList<>();

        //period state
        private TimedBalanceWithMargin onStart;
        private List<GroupItem> items;

        @Override
        public void onPeriodStart(ReferencePeriod period, double balance, double margin) {
            items = new ArrayList<>();
            onStart = new TimedBalanceWithMarginImpl(balance, period.start(), margin);
        }

        @Override
        public void onBalanceReport(BalanceReport report, double margin) {
            items.add(new BalanceReportViewImpl(new TimedBalanceWithMarginImpl(report.amount(), report.time(), margin)));
        }

        @Override
        public void onOperation(Operation operation, double estimatedBalance, double margin) {
            items.add(new OperationViewImpl(operation, estimatedBalance, margin));
        }

        @Override
        public void onPeriodEnd(ReferencePeriod period, double balance, double margin) {
            periods.add(new PeriodGroupViewImpl(period, items, onStart, new TimedBalanceWithMarginImpl(balance, period.end(), margin)));
            items = null;
            onStart = null;
        }
    }

    //ACTIONS
    @NotNull
    @Override
    public LogBalanceAction logBalance() {
        return new LogBalanceActionImpl(balanceBook, schedule);
    }

    @NotNull
    @Override
    public LogOperationAction logOperation() {
        return new LogOperationActionImpl(operationBook, balanceBook, schedule);
    }

    @NotNull
    @Override
    public EraseRecordAction eraseRecord() {
        return new EraseRecordActionImpl(operationBook, balanceBook);
    }
}
