package budget.impl.api;

import budget.api.CustomTags;
import budget.impl.model.RecordTagDomain;
import budget.model.tag.Tags;
import org.jetbrains.annotations.NotNull;

/**
 * Default custom tags implementation
 *
 * @author Kirill Berezin
 */

public class CustomTagsImpl implements CustomTags {
    private final Tags tags;
    private final RecordTagDomain recordTagDomain;

    public CustomTagsImpl(@NotNull Tags tags, @NotNull RecordTagDomain recordTagDomain) {
        this.tags = tags;
        this.recordTagDomain = recordTagDomain;
    }

    @Override
    public void tagWithCustomName(long time, @NotNull String name) {
        tags.update(tags.tagBy(time).withEntry(recordTagDomain.customTitleKey(), name));
    }
}
