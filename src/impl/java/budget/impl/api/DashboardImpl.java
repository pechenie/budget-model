package budget.impl.api;

import budget.api.Dashboard;
import budget.impl.model.metrics.TimedMetricsImpl;
import budget.impl.view.TimedBalanceWithMarginImpl;
import budget.model.BalanceReport;
import budget.model.ReferencePeriod;
import budget.model.TimedMetrics;
import budget.model.source.BalanceReports;
import budget.model.source.Operations;
import budget.model.source.ReferencePeriods;
import budget.view.TimedBalance;
import org.jetbrains.annotations.NotNull;

import java.util.function.Predicate;

/**
 * Default Dashboard implementation
 *
 * @since 15.04.2017
 */
public final class DashboardImpl implements Dashboard {

    @NotNull
    private final TimedMetrics timedMetrics;
    private final ReferencePeriods referencePeriods;
    private final BalanceReports balanceReports;

    public DashboardImpl(Operations operations, BalanceReports balanceReports, ReferencePeriods referencePeriods) {
        this.balanceReports = balanceReports;
        this.timedMetrics = new TimedMetricsImpl(operations, balanceReports, referencePeriods);
        this.referencePeriods = referencePeriods;
    }

    @Override
    public double usableMarginAtTime(long time) {
        return timedMetrics.marginForTimeInPeriod(time, referencePeriods.periodAtTime(time));
    }

    @Override
    public double minUsableMarginSinceTime(long time) {
        return timedMetrics.minUsableMarginInPeriod(time, referencePeriods.periodAtTime(time));
    }

    @NotNull
    @Override
    public TimedBalance lastReportedBalanceForTime(long time) {
        BalanceReport report = balanceReports.lastReportAtTime(time, true);
        return new TimedBalanceWithMarginImpl(report.amount(), report.time(), Double.NaN);
    }

    @Override
    public double estimatedBalanceAtTime(long time) {
        return timedMetrics.estimatedBalanceAtTime(time);
    }

    @NotNull
    @Override
    public TimedBalance minBalanceSinceLastReport() {
        long until = referencePeriods.unite().end();
        return timedMetrics.minBalanceAtRange(balanceReports.lastReportAtTime(until, true).time(), until);
    }

    @Override
    public TimedBalance checkBalanceSinceLastReport(Predicate<Double> criteria) {
        long until = referencePeriods.unite().end();
        BalanceReport balanceReport = balanceReports.lastReportAtTime(until, true);
        TimedBalance timedBalance = timedMetrics.firstBalanceWithCriteria(balanceReport.time(), until, criteria);
        if (timedBalance != null) {
            return timedBalance;
        }
        return null;
    }

    @Override
    public long timeTillPeriodEnd(long time) {
        ReferencePeriod referencePeriod = referencePeriods.periodAtTime(time);
        if (referencePeriod == null) {
            return 0L;
        }
        return referencePeriod.end() - time;
    }
}
