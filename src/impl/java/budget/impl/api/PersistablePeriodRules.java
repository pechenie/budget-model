package budget.impl.api;

import budget.api.PeriodRules;
import budget.api.entity.RuleTemplate;
import budget.api.entity.RuleView;
import budget.impl.model.persistence.PeriodRulesPersistence;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Period rules decorator, which persists last applied rules to provided persistence implementation
 *
 * @author Kirill Berezin
 */

public class PersistablePeriodRules<RV extends RuleView, RI> implements PeriodRules<RV, RI> {
    private final PeriodRules<RV, RI> decorated;
    private final PeriodRulesPersistence<RV, RI> persistence;

    public PersistablePeriodRules(@NotNull PeriodRulesPersistence<RV, RI> persistence, @NotNull PeriodRules<RV, RI> decorated) {
        this.decorated = decorated;
        this.persistence = persistence;
    }

    @Override
    public List<? extends RuleTemplate<RV, RI, ?>> templates() {
        return decorated.templates();
    }

    @Override
    public void applyNewRuleSince(long time, boolean immediate, RV view, RI ri) {
        decorated.applyNewRuleSince(time, immediate, view, ri);
        persistence.save(view, ri);
    }

    @Override
    public void updatePeriodMargin(long time, double margin) {
        decorated.updatePeriodMargin(time, margin);
    }
}
