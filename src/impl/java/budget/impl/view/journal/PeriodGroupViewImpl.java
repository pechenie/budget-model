package budget.impl.view.journal;

import budget.model.ReferencePeriod;
import budget.view.TimedBalanceWithMargin;
import budget.view.journal.GroupItem;
import budget.view.journal.PeriodGroupView;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Simple period group view implementation
 *
 * @since 27.04.2017
 */
public final class PeriodGroupViewImpl implements PeriodGroupView {
    private final ReferencePeriod period;
    private final List<GroupItem> items;
    private final TimedBalanceWithMargin startBalance;
    private final TimedBalanceWithMargin endBalance;

    public PeriodGroupViewImpl(@NotNull ReferencePeriod period, @NotNull List<GroupItem> items,
                               @NotNull TimedBalanceWithMargin startBalance, @NotNull TimedBalanceWithMargin endBalance) {
        this.period = period;
        this.items = items;
        this.startBalance = startBalance;
        this.endBalance = endBalance;
    }

    @Override
    public int itemsCount() {
        return items.size();
    }

    @NotNull
    @Override
    public GroupItem itemAt(int index) {
        return items.get(index);
    }

    @NotNull
    @Override
    public ReferencePeriod period() {
        return period;
    }

    @NotNull
    @Override
    public TimedBalanceWithMargin startBalance() {
        return startBalance;
    }

    @NotNull
    @Override
    public TimedBalanceWithMargin endBalance() {
        return endBalance;
    }


}
