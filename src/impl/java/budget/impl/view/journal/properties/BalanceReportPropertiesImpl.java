package budget.impl.view.journal.properties;

import budget.impl.model.RecordTagDomain;
import budget.model.tag.Tag;
import budget.view.journal.properties.BalanceReportProperties;
import org.jetbrains.annotations.NotNull;

/**
 * Balance report properties implementation based on tags
 *
 * @author Kirill Berezin
 */

class BalanceReportPropertiesImpl implements BalanceReportProperties {
    private final String name;

    BalanceReportPropertiesImpl(Tag tag, RecordTagDomain recordTagDomain) {
        this.name = tag.valueFor(recordTagDomain.customTitleKey());
    }

    @NotNull
    @Override
    public String name() {
        return name;
    }
}
