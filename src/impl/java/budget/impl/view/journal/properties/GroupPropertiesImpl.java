package budget.impl.view.journal.properties;

import budget.impl.model.RecordTagDomain;
import budget.impl.model.operation.PlannedOperationsTagDomain;
import budget.model.tag.Tag;
import budget.model.tag.Tags;
import budget.view.journal.BalanceReportView;
import budget.view.journal.GroupItemVisitor;
import budget.view.journal.OperationView;
import budget.view.journal.PeriodGroupView;
import budget.view.journal.properties.GroupProperties;
import budget.view.journal.properties.ItemProperties;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Group properties implementation based on tags
 * @author Kirill Berezin
 */

public class GroupPropertiesImpl implements GroupProperties {
    private final List<ItemProperties> itemProperties;
    private final Presence presence;

    GroupPropertiesImpl(@NotNull PeriodGroupView groupView, @NotNull Presence presence, Tags tags,
                        @NotNull RecordTagDomain recordTagDomain, @NotNull PlannedOperationsTagDomain plannedOpsTagDomain,
                        @NotNull Map<String, Long> chainStartTimes) {
        this.presence = presence;

        List<ItemProperties> itemProperties = new ArrayList<>(groupView.itemsCount());
        for (int i = 0; i < groupView.itemsCount(); ++i) {
            itemProperties.add(groupView.itemAt(i).visitBy(new GroupItemVisitor<ItemProperties>() {
                @Override
                public ItemProperties onOperation(@NotNull OperationView item) {
                    Tag operationTag = tags.tagBy(item.state().time());
                    String seriesKey = plannedOpsTagDomain.seriesNameKey();
                    if (operationTag.hasEntry(seriesKey)) {
                        return new PlannedOperationPropertiesImpl(chainStartTimes.computeIfAbsent(operationTag.valueFor(seriesKey),
                                k -> /*this is the first item in chain*/ item.state().time()), operationTag, plannedOpsTagDomain);
                    }
                    return new ExecutedOperationPropertiesImpl(operationTag, recordTagDomain);
                }

                @Override
                public ItemProperties onBalanceReport(@NotNull BalanceReportView item) {
                    return new BalanceReportPropertiesImpl(tags.tagBy(item.state().time()), recordTagDomain);
                }
            }));
        }
        this.itemProperties = itemProperties;
    }

    @NotNull
    @Override
    public Presence presence() {
        return presence;
    }

    @NotNull
    @Override
    public ItemProperties itemPropertiesAt(int index) {
        return itemProperties.get(index);
    }

    @Override
    public int itemsCount() {
        return itemProperties.size();
    }
}
