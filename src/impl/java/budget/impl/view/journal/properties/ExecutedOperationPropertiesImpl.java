package budget.impl.view.journal.properties;

import budget.impl.model.RecordTagDomain;
import budget.model.tag.Tag;
import budget.view.journal.properties.ExecutedOperationProperties;
import org.jetbrains.annotations.NotNull;

/**
 * Executed operation properties implementation based on tags
 *
 * @author Kirill Berezin
 */

class ExecutedOperationPropertiesImpl implements ExecutedOperationProperties {
    private final String name;

    ExecutedOperationPropertiesImpl(Tag tag, RecordTagDomain recordTagDomain) {
        this.name = tag.valueFor(recordTagDomain.customTitleKey());
    }

    @NotNull
    @Override
    public String name() {
        return name;
    }
}
