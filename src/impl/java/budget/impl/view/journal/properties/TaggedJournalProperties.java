package budget.impl.view.journal.properties;

import budget.impl.model.RecordTagDomain;
import budget.impl.model.operation.PlannedOperationsTagDomain;
import budget.model.tag.Tags;
import budget.view.journal.JournalView;
import budget.view.journal.properties.GroupProperties;
import budget.view.journal.properties.JournalProperties;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Tags based journal properties implementation
 *
 * @author Kirill Berezin
 */

public final class TaggedJournalProperties implements JournalProperties {
    private final List<GroupProperties> groupProperties;

    public TaggedJournalProperties(@NotNull JournalView view, @NotNull Tags tags, @NotNull RecordTagDomain recordTagDomain,
                                   @NotNull PlannedOperationsTagDomain plannedOpsTagDomain, long now) {
        int todayIndex = view.groupIndexAtTime(now);

        HashMap<String, Long> chainStartTimes = new HashMap<>();
        List<GroupProperties> groupProperties = new ArrayList<>(view.groupsCount());
        for (int i = 0; i < view.groupsCount(); ++i) {
            groupProperties.add(new GroupPropertiesImpl(view.groupAt(i), presenceBy(i, todayIndex),
                    tags, recordTagDomain, plannedOpsTagDomain, chainStartTimes));
        }
        this.groupProperties = groupProperties;
    }

    private GroupProperties.Presence presenceBy(int index, int todayIndex) {
        return index < todayIndex ? GroupProperties.Presence.PAST :
                index > todayIndex ? GroupProperties.Presence.FUTURE :
                        GroupProperties.Presence.PRESENT;
    }

    @Override
    public int groupsCount() {
        return groupProperties.size();
    }

    @NotNull
    @Override
    public GroupProperties groupPropertiesAt(int index) {
        return groupProperties.get(index);
    }
}
