package budget.impl.view.journal.properties;

import budget.impl.model.operation.PlannedOperationsTagDomain;
import budget.model.tag.Tag;
import budget.view.journal.properties.PlannedOperationProperties;
import org.jetbrains.annotations.NotNull;

/**
 * Planned operation properties implementation based on tags
 *
 * @author Kirill Berezin
 */

class PlannedOperationPropertiesImpl implements PlannedOperationProperties {
    private final String seriesName;
    private final long chainStartTime;

    PlannedOperationPropertiesImpl(long chainStartTime, Tag tag, PlannedOperationsTagDomain plannedOpsTagDomain) {
        this.seriesName = tag.valueFor(plannedOpsTagDomain.seriesNameKey());
        this.chainStartTime = chainStartTime;
    }

    @Override
    public long chainStartTime() {
        return chainStartTime;
    }

    @NotNull
    @Override
    public String name() {
        return seriesName;
    }
}
