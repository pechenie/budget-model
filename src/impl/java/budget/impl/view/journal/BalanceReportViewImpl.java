package budget.impl.view.journal;

import budget.view.TimedBalanceWithMargin;
import budget.view.journal.BalanceReportView;
import org.jetbrains.annotations.NotNull;

/**
 * Simple balance report view implementation
 *
 * @since 28.04.2017
 */
public final class BalanceReportViewImpl implements BalanceReportView {
    private final TimedBalanceWithMargin balance;

    public BalanceReportViewImpl(@NotNull TimedBalanceWithMargin balance) {
        this.balance = balance;
    }

    @Override
    public TimedBalanceWithMargin state() {
        return balance;
    }
}
