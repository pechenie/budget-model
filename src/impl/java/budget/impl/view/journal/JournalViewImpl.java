package budget.impl.view.journal;

import budget.impl.view.search.GroupViewSearch;
import budget.view.journal.JournalView;
import budget.view.journal.PeriodGroupView;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Simple journal view implementation
 *
 * @since 27.04.2017
 */
public final class JournalViewImpl implements JournalView {
    private final List<PeriodGroupView> groups;

    public JournalViewImpl(@NotNull List<PeriodGroupView> groups) {
        this.groups = groups;
    }

    @Override
    public int groupIndexAtTime(long time) {
        return new GroupViewSearch().findIndexOf(time, groups);
    }

    @Override
    public int groupsCount() {
        return groups.size();
    }

    @NotNull
    @Override
    public PeriodGroupView groupAt(int index) {
        return groups.get(index);
    }
}
