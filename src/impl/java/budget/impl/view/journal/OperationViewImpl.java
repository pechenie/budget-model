package budget.impl.view.journal;

import budget.impl.view.TimedBalanceWithMarginImpl;
import budget.model.Operation;
import budget.view.TimedBalanceWithMargin;
import budget.view.journal.OperationView;
import org.jetbrains.annotations.NotNull;

/**
 * Simple operation view implementation
 *
 * @since 28.04.2017
 */
public final class OperationViewImpl implements OperationView {
    private final Operation operation;
    private final TimedBalanceWithMargin balanceWithMargin;

    public OperationViewImpl(@NotNull Operation operation, double estimatedBalance, double usableMargin) {
        this.operation = operation;
        this.balanceWithMargin = new TimedBalanceWithMarginImpl(estimatedBalance, operation.time(), usableMargin);
    }

    @Override
    public double cost() {
        return operation.cost();
    }

    @Override
    public TimedBalanceWithMargin state() {
        return balanceWithMargin;
    }
}
