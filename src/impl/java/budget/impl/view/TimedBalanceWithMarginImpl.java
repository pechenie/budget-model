package budget.impl.view;

import budget.view.TimedBalanceWithMargin;

/**
 * Simple timed balance with margin implementation
 *
 * @since 27.04.2017
 */
public final class TimedBalanceWithMarginImpl implements TimedBalanceWithMargin {
    private final long time;
    private final double balance;
    private final double margin;

    public TimedBalanceWithMarginImpl(double balance, long time, double margin) {
        this.time = time;
        this.balance = balance;
        this.margin = margin;
    }

    @Override
    public long time() {
        return time;
    }

    @Override
    public double balance() {
        return balance;
    }

    @Override
    public double margin() {
        return margin;
    }
}
