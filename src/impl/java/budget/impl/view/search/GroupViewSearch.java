package budget.impl.view.search;

import budget.impl.model.search.BinarySearch;
import budget.model.ReferencePeriod;
import budget.view.journal.PeriodGroupView;

/**
 * Binary search implementation for period group views search by time
 *
 * @author Kirill Berezin
 */
public class GroupViewSearch extends BinarySearch<PeriodGroupView, Long> {
    @Override
    protected int comparison(Long key, PeriodGroupView entity) {
        ReferencePeriod.Relation relation = entity.period().relatesTo(key);
        if (relation == ReferencePeriod.Relation.PERIOD_IS_BEFORE) {
            return 1;
        } else if (relation == ReferencePeriod.Relation.PERIOD_IS_AFTER) {
            return -1;
        }
        return 0;
    }
}
