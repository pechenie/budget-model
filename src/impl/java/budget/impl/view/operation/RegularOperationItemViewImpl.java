package budget.impl.view.operation;

import budget.view.TimedCost;
import budget.view.operation.RegularOperationItemView;
import org.jetbrains.annotations.NotNull;

/**
 * Simple regular operation item view implementation
 *
 * @author Kirill Berezin
 */

public class RegularOperationItemViewImpl implements RegularOperationItemView {
    private final String name;
    private final TimedCost nextOperation;
    private final int operationsLeft;

    public RegularOperationItemViewImpl(@NotNull String name, @NotNull TimedCost nextOperation, int operationsLeft) {
        this.name = name;
        this.nextOperation = nextOperation;
        this.operationsLeft = operationsLeft;
    }

    @NotNull
    @Override
    public String name() {
        return name;
    }

    @NotNull
    @Override
    public TimedCost next() {
        return nextOperation;
    }

    @Override
    public int operationsLeft() {
        return operationsLeft;
    }
}
