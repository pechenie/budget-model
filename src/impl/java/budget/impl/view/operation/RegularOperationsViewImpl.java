package budget.impl.view.operation;

import budget.view.operation.RegularOperationItemView;
import budget.view.operation.RegularOperationsView;
import org.jetbrains.annotations.Nullable;

import java.util.List;

/**
 * Simple regular operations view implementation
 *
 * @author Kirill Berezin
 */

public class RegularOperationsViewImpl implements RegularOperationsView {
    private final List<? extends RegularOperationItemView> views;

    public RegularOperationsViewImpl(List<? extends RegularOperationItemView> views) {
        this.views = views;
    }

    @Override
    public int itemsCount() {
        return views.size();
    }

    @Nullable
    @Override
    public RegularOperationItemView viewAt(int i) {
        if (i < 0 || i >= views.size()) {
            return null;
        }
        return views.get(i);
    }
}
