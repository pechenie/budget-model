package budget.impl.app;

import budget.api.CustomTags;
import budget.api.Dashboard;
import budget.api.Journal;
import budget.api.PeriodRules;
import budget.api.RegularOperations;
import budget.api.entity.RecurrenceInput;
import budget.api.entity.RuleTemplate;
import budget.api.entity.RuleView;
import budget.app.BudgetModel;
import budget.impl.api.CustomTagsImpl;
import budget.impl.api.DashboardImpl;
import budget.impl.api.FactoryPeriodRules;
import budget.impl.api.JournalImpl;
import budget.impl.api.NotifyingCustomTags;
import budget.impl.api.PersistablePeriodRules;
import budget.impl.api.RegularOperationsImpl;
import budget.impl.model.BalanceReportsImpl;
import budget.impl.model.BookSchedule;
import budget.impl.model.OperationsImpl;
import budget.impl.model.ReferencePeriodsImpl;
import budget.impl.model.future.ReferencePeriodsFactory;
import budget.impl.model.notifying.NotifyingBalanceBook;
import budget.impl.model.notifying.NotifyingOperationBook;
import budget.impl.model.notifying.NotifyingPeriodBook;
import budget.impl.model.operation.PlannedOperationsImpl;
import budget.impl.model.persistence.JournalPersistence;
import budget.impl.model.persistence.PeriodRulesPersistence;
import budget.impl.model.persistence.PersistableBalanceBook;
import budget.impl.model.persistence.PersistableOperationBook;
import budget.impl.model.persistence.PersistablePeriodBook;
import budget.impl.view.journal.properties.TaggedJournalProperties;
import budget.model.UpdateListener;
import budget.model.book.OperationRecordBook;
import budget.model.book.PeriodBook;
import budget.model.operation.PlannedOperations;
import budget.model.source.Operations;
import budget.model.source.ReferencePeriods;
import budget.model.tag.Tags;
import budget.model.time.TimestampIterator;
import budget.view.journal.JournalView;
import budget.view.journal.properties.JournalProperties;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * The ultimate budget model implementation.
 *
 * <p>Guarantees consistency of all participating entities.
 * Any objects provided by this model shall not be stored nor persisted manually.
 * Any existing instances of objects provided by model are not guaranteed to be consist after notification about model update.
 * It is not guaranteed that stale (if any) objects won't in turn affect the model.</p>
 *
 * <p>
 * To summarize everything: any interaction with the model within some execution context shall always start with a model's method call.
 * </p>
 *
 * <p>The model <b>is not</b> thread-safe</p>
 *
 * @author Kirill Berezin
 */
public class BudgetModelImpl<RegOpsRV extends RuleView, RegOpsInp extends RecurrenceInput, PeriodRV extends RuleView, PeriodRInp>
        implements BudgetModel<RegOpsRV, RegOpsInp, PeriodRV, PeriodRInp> {
    private final JournalPersistence journalPersistence;
    private final PeriodRulesPersistence<PeriodRV, PeriodRInp> rulesPersistence;
    private final Tags tags;

    private final BalanceReportsImpl reportsImpl;
    private final Operations operations;
    private final ReferencePeriodsImpl periodsImpl;
    private final OperationRecordBook operationBook;
    private final PlannedOperations plannedOperations;

    private final Journal journal;
    private final Dashboard dashboard;
    private final CustomTags customTags;
    private final RegularOperations<RegOpsRV, RegOpsInp> regularOperations;
    private final PeriodRules<PeriodRV, PeriodRInp> periodRules;
    private final Set<UpdateListener> listeners = new CopyOnWriteArraySet<>();

    public BudgetModelImpl(@NotNull JournalPersistence journalPersistence,
                           @NotNull PeriodRulesPersistence<PeriodRV, PeriodRInp> rulesPersistence,
                           @NotNull List<? extends RuleTemplate<PeriodRV, PeriodRInp, ReferencePeriodsFactory>> periods,
                           @NotNull List<? extends RuleTemplate<RegOpsRV, RegOpsInp, TimestampIterator>> occurrences,
                           @NotNull Tags tags) {
        //args
        this.journalPersistence = journalPersistence;
        this.rulesPersistence = rulesPersistence;
        this.tags = tags;

        //models
        this.reportsImpl = new BalanceReportsImpl(journalPersistence.readReports());
        OperationsImpl operationsImpl = new OperationsImpl(journalPersistence.readOperations());
        PlannedOperationsImpl plannedOperationsImpl = newPlannedOperations(operationsImpl);
        this.operations = operationsImpl;
        this.operationBook = plannedOperationsImpl;
        this.plannedOperations = plannedOperationsImpl;
        this.periodsImpl = new ReferencePeriodsImpl(journalPersistence.readPeriods());

        //api entities
        this.journal = newJournal();
        this.dashboard = newDashboard();
        this.regularOperations = newRegularOperations(occurrences);
        this.periodRules = newPeriodRules(periods);
        this.customTags = new NotifyingCustomTags(this::innerStateUpdated, new CustomTagsImpl(tags, new RecordTagDomainImpl()));
    }

    @NotNull
    @Override
    public Journal journal() {
        return journal;
    }

    @NotNull
    @Override
    public Dashboard dashboard() {
        return dashboard;
    }

    @NotNull
    @Override
    public RegularOperations<RegOpsRV, RegOpsInp> regularOperations() {
        return regularOperations;
    }

    @NotNull
    @Override
    public PeriodRules<PeriodRV, PeriodRInp> periodRules() {
        return periodRules;
    }

    @Override
    public CustomTags customTags() {
        return customTags;
    }

    @NotNull
    @Override
    public JournalProperties journalProperties(@NotNull JournalView view, long now) {
        return new TaggedJournalProperties(view, tags, new RecordTagDomainImpl(), new PlannedOperationsTagDomainImpl(), now);
    }

    @Override
    public void addListener(@NotNull UpdateListener updateListener) {
        listeners.add(updateListener);
        updateListener.notifyUpdated();
    }

    @Override
    public void removeListener(@NotNull UpdateListener updateListener) {
        listeners.remove(updateListener);
    }

    /*
    PRIVATE IMPLEMENTATION PART
     */

    private void innerStateUpdated() {
        listeners.forEach(UpdateListener::notifyUpdated);
    }

    private PeriodRules<PeriodRV, PeriodRInp> newPeriodRules(
            List<? extends RuleTemplate<PeriodRV, PeriodRInp, ReferencePeriodsFactory>> templates) {
        return new PersistablePeriodRules<>(
                rulesPersistence,
                new PeriodRulesImpl(
                        periodsImpl,
                        new NotifyingPeriodBook(
                                this::innerStateUpdated, new PersistablePeriodBook(journalPersistence, periodsImpl, periodsImpl)
                        ),
                        templates)
        );
    }

    private RegularOperations<RegOpsRV, RegOpsInp> newRegularOperations(
            List<? extends RuleTemplate<RegOpsRV, RegOpsInp, TimestampIterator>> templates) {
        return new AppRegularOperationsImpl(plannedOperations, templates);
    }

    @NotNull
    private PlannedOperationsImpl newPlannedOperations(OperationsImpl operationsImpl) {
        return new PlannedOperationsImpl(
                operationsImpl,
                new NotifyingOperationBook(
                        this::innerStateUpdated,
                        new PersistableOperationBook(journalPersistence, operationsImpl)
                ),
                new BookSchedule(operationsImpl, reportsImpl),
                new PlannedOperationsTagDomainImpl(),
                tags
        );
    }

    private Dashboard newDashboard() {
        return new DashboardImpl(operations, reportsImpl, periodsImpl);
    }

    private Journal newJournal() {
        return new JournalImpl(
                reportsImpl, operations, periodsImpl,
                new NotifyingBalanceBook(
                        this::innerStateUpdated,
                        new PersistableBalanceBook(journalPersistence, reportsImpl)
                ),
                operationBook
        );
    }

    private final class AppRegularOperationsImpl extends RegularOperationsImpl<RegOpsRV, RegOpsInp> {
        private final List<? extends RuleTemplate<RegOpsRV, RegOpsInp, TimestampIterator>> templates;

        private AppRegularOperationsImpl(PlannedOperations plannedOperations,
                                         List<? extends RuleTemplate<RegOpsRV, RegOpsInp, TimestampIterator>> t) {
            super(plannedOperations);
            templates = t;
        }

        @Override
        public Collection<? extends RuleTemplate<RegOpsRV, RegOpsInp, TimestampIterator>> occurrenceTemplates() {
            return templates;
        }
    }

    private final class PeriodRulesImpl extends FactoryPeriodRules<PeriodRV, PeriodRInp> {
        private final List<? extends RuleTemplate<PeriodRV, PeriodRInp, ReferencePeriodsFactory>> templates;

        private PeriodRulesImpl(ReferencePeriods referencePeriods, PeriodBook periodBook,
                                List<? extends RuleTemplate<PeriodRV, PeriodRInp, ReferencePeriodsFactory>> templates) {
            super(referencePeriods, periodBook);
            this.templates = templates;
        }

        @Override
        public List<? extends RuleTemplate<PeriodRV, PeriodRInp, ReferencePeriodsFactory>> templates() {
            return templates;
        }
    }
}
