package budget.impl.app;

import budget.impl.model.operation.PlannedOperationsTagDomain;
import org.jetbrains.annotations.NotNull;

/**
 * Default implementation of planned operations tag keys domain
 *
 * @author Kirill Berezin
 */
class PlannedOperationsTagDomainImpl implements PlannedOperationsTagDomain {
    @NotNull
    @Override
    public String previousOperationKey() {
        return "$prev.op.time";
    }

    @NotNull
    @Override
    public String nextOperationKey() {
        return "$next.op.time";
    }

    @NotNull
    @Override
    public String seriesNameKey() {
        return "$series.name";
    }
}
