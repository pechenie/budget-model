package budget.impl.app;

import budget.impl.model.RecordTagDomain;
import org.jetbrains.annotations.NotNull;

/**
 * Default implementation of records tag keys domain
 *
 * @author Kirill Berezin
 */
class RecordTagDomainImpl implements RecordTagDomain {
    @NotNull
    @Override
    public String customTitleKey() {
        return "record.custom.title";
    }
}
