package budget.impl.model.future;

import budget.model.ReferencePeriod;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * A factory of reference periods. Generates periods by implementation defined rules starting with some time.
 *
 * @since 29.04.2017
 */
public interface ReferencePeriodsFactory {
    /**
     * Generates periods by implementation defined rules starting with the time provided
     *
     * @param time time to start
     * @return a list of consistent periods
     */
    @NotNull
    List<? extends ReferencePeriod> calculateWithStartingTime(long time);
}
