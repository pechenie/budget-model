package budget.impl.model.future;

import budget.model.Operation;
import budget.model.source.Operations;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;

/**
 * Decorator over operations, merging together multiple instances of operations source
 *
 * @since 29.04.2017
 */
public final class MultipleSourceOperations implements Operations {
    private final Collection<? extends Operations> operations;

    public MultipleSourceOperations(@NotNull Operations... operations) {
        this(Arrays.asList(operations));
    }

    public MultipleSourceOperations(@NotNull Collection<? extends Operations> operations) {
        this.operations = operations;
    }

    @Nullable
    @Override
    public Operation operationAt(long time) {
        for (Operations o : operations) {
            Operation operation = o.operationAt(time);
            if (operation != null) {
                return operation;
            }
        }
        return null;
    }

    @Override
    public double operationSum(long timeFrom, long timeTo, boolean inclusive) {
        return operations.stream().reduce(0d, (sum, t) -> sum + t.operationSum(timeFrom, timeTo, inclusive), (d1, d2) -> d1 + d2);
    }

    @NotNull
    @Override
    public Collection<Operation> range(long from, boolean inclusiveFrom, long to, boolean inclusiveTo) {
        ArrayList<Operation> ranges = new ArrayList<>();
        for (Operations operation : operations) {
            ranges.addAll(operation.range(from, inclusiveFrom, to, inclusiveTo));
        }
        ranges.sort(Comparator.comparingLong(Operation::time));
        return ranges;
    }
}
