package budget.impl.model.time;

import budget.model.time.TimestampIterator;
import org.joda.time.LocalDate;

/**
 * Iterator which walks on days start times
 * @author Kirill Berezin
 */

public class DailyIterator implements TimestampIterator {
    private final int count;

    public DailyIterator(int count) {
        this.count = count;
    }

    @Override
    public long next(long current) {
        return new LocalDate(current).plusDays(count).toDateTimeAtStartOfDay().getMillis();
    }

    @Override
    public long alignedStart(long current) {
        return new LocalDate(current).toDateTimeAtStartOfDay().getMillis();
    }
}
