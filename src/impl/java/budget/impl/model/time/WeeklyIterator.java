package budget.impl.model.time;

import budget.model.time.TimestampIterator;
import org.joda.time.LocalDate;

/**
 * Iterator which adds a full week to previous value in order to get next value
 *
 * @author Kirill Berezin
 */

public class WeeklyIterator implements TimestampIterator {
    private final int count;

    public WeeklyIterator(int count) {
        this.count = count;
    }

    @Override
    public long next(long current) {
        return new LocalDate(current).plusWeeks(count).toDateTimeAtStartOfDay().getMillis();
    }

    @Override
    public long alignedStart(long current) {
        return new LocalDate(current).toDateTimeAtStartOfDay().getMillis();
    }
}
