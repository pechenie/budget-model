package budget.impl.model.time;

import budget.model.time.TimestampIterator;
import org.joda.time.LocalDate;

/**
 * Iterator which walks on months first days
 *
 * @author Kirill Berezin
 */

public class StartOfMonthIterator implements TimestampIterator {
    private final int count;

    public StartOfMonthIterator(int count) {
        this.count = count;
    }

    @Override
    public long next(long current) {
        return new LocalDate(current).plusMonths(count).withDayOfMonth(1).toDateTimeAtStartOfDay().getMillis();
    }

    @Override
    public long alignedStart(long current) {
        return new LocalDate(current).withDayOfMonth(1).toDateTimeAtStartOfDay().getMillis();
    }
}
