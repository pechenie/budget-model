package budget.impl.model.time;

import budget.model.time.TimestampIterator;
import org.joda.time.LocalDate;

/**
 * Iterator which produces months' ends
 *
 * @author Kirill Berezin
 */

public class LastDayOfMonthIterator implements TimestampIterator {
    @Override
    public long next(long current) {
        LocalDate currentDate = new LocalDate(current);
        LocalDate nextDate;
        if (currentDate.getDayOfMonth() < currentDate.dayOfMonth().getMaximumValue()) {
            nextDate = currentDate.withDayOfMonth(currentDate.dayOfMonth().getMaximumValue());
        } else {
            nextDate = currentDate.plusMonths(1);
            nextDate = nextDate.withDayOfMonth(nextDate.dayOfMonth().getMaximumValue());
        }
        return nextDate.toDateTimeAtStartOfDay().getMillis();
    }

    @Override
    public long alignedStart(long current) {
        LocalDate localDate = new LocalDate(current).minusMonths(1);
        return localDate.withDayOfMonth(localDate.dayOfMonth().getMaximumValue()).toDateTimeAtStartOfDay().getMillis();
    }
}
