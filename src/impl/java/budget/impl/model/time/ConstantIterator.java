package budget.impl.model.time;

import budget.model.time.TimestampIterator;

/**
 * Just simple naive constant iterator which appends some fixed delta to previous value
 *
 * @author Kirill Berezin
 */

public class ConstantIterator implements TimestampIterator {
    private final long delta;

    public ConstantIterator(long delta) {
        this.delta = delta;
    }

    @Override
    public long next(long current) {
        return current + delta;
    }

    @Override
    public long alignedStart(long current) {
        return current;
    }
}
