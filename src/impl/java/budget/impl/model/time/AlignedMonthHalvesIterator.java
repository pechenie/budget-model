package budget.impl.model.time;

import budget.model.time.TimestampIterator;
import org.joda.time.LocalDate;

/**
 * Timestamp iterator which walks over months halves (from 1st to 15th, from 16th to month's end)
 *
 * @author Kirill Berezin
 */

public class AlignedMonthHalvesIterator implements TimestampIterator {
    @Override
    public long next(long current) {
        LocalDate date = new LocalDate(current);
        if (date.getDayOfMonth() < 16) {
            date = date.withDayOfMonth(16);
        } else {
            date = date.plusMonths(1).withDayOfMonth(1);
        }
        return date.toDateTimeAtStartOfDay().getMillis();
    }

    @Override
    public long alignedStart(long current) {
        LocalDate date = new LocalDate(current);
        if (date.getDayOfMonth() >= 16) {
            date = date.withDayOfMonth(16);
        } else {
            date = date.withDayOfMonth(1);
        }
        return date.toDateTimeAtStartOfDay().getMillis();
    }
}
