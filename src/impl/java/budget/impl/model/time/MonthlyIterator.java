package budget.impl.model.time;

import budget.model.time.TimestampIterator;
import org.joda.time.LocalDate;

/**
 * Iterator which adds a full month to previous value in order to get next value
 *
 * @author Kirill Berezin
 */

public class MonthlyIterator implements TimestampIterator {
    private final int count;

    public MonthlyIterator(int count) {
        this.count = count;
    }

    @Override
    public long next(long current) {
        return new LocalDate(current).plusMonths(count).toDateTimeAtStartOfDay().getMillis();
    }

    @Override
    public long alignedStart(long current) {
        return new LocalDate(current).toDateTimeAtStartOfDay().getMillis();
    }
}
