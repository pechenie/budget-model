package budget.impl.model.time;

import budget.model.time.TimestampIterator;
import org.joda.time.LocalDate;

/**
 * Iterator which walks on Mondays
 *
 * @author Kirill Berezin
 */

public class StartOfWeekIterator implements TimestampIterator {
    private final int count;

    public StartOfWeekIterator(int count) {
        this.count = count;
    }

    @Override
    public long next(long current) {
        return new LocalDate(current).plusWeeks(count).withDayOfWeek(1).toDateTimeAtStartOfDay().getMillis();
    }

    @Override
    public long alignedStart(long current) {
        return new LocalDate(current).withDayOfWeek(1).toDateTimeAtStartOfDay().getMillis();
    }
}
