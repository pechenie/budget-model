package budget.impl.model.metrics;

import budget.model.BalanceReport;
import org.jetbrains.annotations.NotNull;

/**
 * Balance item representing balance report
 *
 * @author Kirill Berezin
 */
final class ReportBalanceItem implements BalanceItem {
    private final BalanceReport balanceReport;

    ReportBalanceItem(@NotNull BalanceReport balanceReport) {
        this.balanceReport = balanceReport;
    }

    @Override
    public long time() {
        return balanceReport.time();
    }

    @Override
    public <T> T visitBy(BalanceItemVisitor<T> visitor) {
        return visitor.onBalanceReport(balanceReport);
    }
}
