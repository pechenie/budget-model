package budget.impl.model.metrics;

/**
 * Low-level journal record
 *
 * @author Kirill Berezin
 */
interface BalanceItem {
    long time();

    <T> T visitBy(BalanceItemVisitor<T> visitor);
}
