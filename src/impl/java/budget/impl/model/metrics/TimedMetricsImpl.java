package budget.impl.model.metrics;

import budget.model.BalanceReport;
import budget.model.Operation;
import budget.model.ReferencePeriod;
import budget.model.TimedMetrics;
import budget.model.source.BalanceReports;
import budget.model.source.Operations;
import budget.model.source.ReferencePeriods;
import budget.view.TimedBalance;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.function.Predicate;

import static budget.model.ReferencePeriod.Relation.PERIOD_IS_BEFORE;

/**
 * Default timed metrics implementation. All operations are based on traversing (or folding) the low-lever journal.
 *
 * @since 28.04.2017
 */
public final class TimedMetricsImpl implements TimedMetrics {
    private final BalanceReports balanceReports;

    private final BalanceItems balances;

    public TimedMetricsImpl(@NotNull Operations operations, @NotNull BalanceReports balanceReports,
                            @NotNull ReferencePeriods referencePeriods) {
        this.balanceReports = balanceReports;
        this.balances = new BalanceItemsImpl(balanceReports, operations, referencePeriods);
    }

    @Override
    public double marginForTimeInPeriod(long time, @Nullable ReferencePeriod referencePeriod) {
        if (referencePeriod == null) {
            return estimatedBalanceAtTime(time);
        }
        boolean inclusive = referencePeriod.includes(time);

        long start = referencePeriod.start();
        BalanceReport lastReport = balanceReports.lastReportAtTime(start, false);
        RunningMetrics runningMetrics = new RunningMetrics(finalReportTime(), lastReport);
        return new FoldToLast(runningMetrics).reduce(runningMetrics,
                balances.balances(lastReport.time(), time, inclusive, referencePeriod.relatesTo(time) != PERIOD_IS_BEFORE)).usableMargin();
    }

    @Override
    public double minUsableMarginInPeriod(long time, @Nullable ReferencePeriod referencePeriod) {
        if (referencePeriod == null || !referencePeriod.includes(time)) {
            return 0d;
        }

        long start = referencePeriod.start();
        BalanceReport lastReport = balanceReports.lastReportAtTime(start, false);

        RunningMetrics runningMetrics = new RunningMetrics(finalReportTime(), lastReport);
        return new FoldToMinMargin(runningMetrics)
                .reduce(new FoldToLast(runningMetrics).reduce(runningMetrics, balances.balances(lastReport.time(), time, true)),
                        balances.balances(time, referencePeriod.end(), false)).usableMargin();
    }

    @Override
    public double estimatedBalanceAtTime(long time) {
        BalanceReport lastReport = balanceReports.lastReportAtTime(time, true);
        RunningMetrics runningMetrics = new RunningMetrics(finalReportTime(), lastReport);
        return new FoldToLast(runningMetrics).reduce(runningMetrics, balances.balances(lastReport.time(), time, true)).balance();
    }

    @NotNull
    @Override
    public TimedBalance minBalanceAtRange(long from, long to) {
        RunningMetrics runningMetrics = new RunningMetrics(finalReportTime(), estimatedBalanceAtTime(from), from);
        return new FoldToMinBalance(runningMetrics).reduce(runningMetrics, balances.balances(from, to, true)).toTimedBalance();
    }

    @Nullable
    @Override
    public TimedBalance firstBalanceWithCriteria(long from, long to, Predicate<Double> criteria) {
        RunningMetrics runningMetrics = new RunningMetrics(finalReportTime(), estimatedBalanceAtTime(from), from);
        Metrics result = new FoldUntilBalance(runningMetrics, criteria).reduce(runningMetrics, balances.balances(from, to, true));
        if (result != null) {
            return result.toTimedBalance();
        }
        return null;
    }

    @Override
    public void traverse(EntryVisitor visitor, long from, long to) {
        BalanceReport report = balanceReports.lastReportAtTime(from, true);
        RunningMetrics runningMetrics = new RunningMetrics(finalReportTime(), report);
        new FoldToLast(new Wrapper(runningMetrics, visitor)).reduce(runningMetrics, balances.balances(report.time(), to, false));
    }

    private long finalReportTime() {
        return balanceReports.lastReportAtTime(Long.MAX_VALUE, true).time();
    }

    private static final class Wrapper implements BalanceItemVisitor<Metrics> {
        private final BalanceItemVisitor<Metrics> decorated;
        private final EntryVisitor entryVisitor;

        private Wrapper(BalanceItemVisitor<Metrics> decorated, EntryVisitor entryVisitor) {
            this.decorated = decorated;
            this.entryVisitor = entryVisitor;
        }

        @Override
        public Metrics onPeriodStart(@NotNull ReferencePeriod period) {
            Metrics metrics = decorated.onPeriodStart(period);
            entryVisitor.onPeriodStart(period, metrics.balance(), metrics.usableMargin());
            return metrics;
        }

        @Override
        public Metrics onBalanceReport(@NotNull BalanceReport report) {
            Metrics metrics = decorated.onBalanceReport(report);
            entryVisitor.onBalanceReport(report, metrics.usableMargin());
            return metrics;
        }

        @Override
        public Metrics onOperation(@NotNull Operation operation) {
            Metrics metrics = decorated.onOperation(operation);
            entryVisitor.onOperation(operation, metrics.balance(), metrics.usableMargin());
            return metrics;
        }

        @Override
        public Metrics onPeriodEnd(@NotNull ReferencePeriod period) {
            Metrics metrics = decorated.onPeriodEnd(period);
            entryVisitor.onPeriodEnd(period, metrics.balance(), metrics.usableMargin());
            return metrics;
        }
    }
}
