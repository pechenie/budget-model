package budget.impl.model.metrics;

import budget.impl.view.TimedBalanceWithMarginImpl;
import budget.model.BalanceReport;
import budget.model.Operation;
import budget.model.ReferencePeriod;
import budget.view.TimedBalance;
import org.jetbrains.annotations.NotNull;

import static budget.model.ReferencePeriod.Relation.PERIOD_IS_BEFORE;

/**
 * @author Kirill Berezin
 */
final class RunningMetrics implements Metrics, BalanceItemVisitor<Metrics> {
    private static final double EPS = 0.000001;

    private final long finalReportTime;

    private double balance;
    private double usableMargin;
    private double cappedUsableMargin;
    private long time;

    private double maxMargin;
    private boolean excludeMarginFromBalance;

    RunningMetrics(long finalReportTime, double balance, long time) {
        this.finalReportTime = finalReportTime;
        //
        onBalanceReportImpl(balance, time);
    }

    RunningMetrics(long finalReportTime, @NotNull BalanceReport report) {
        this.finalReportTime = finalReportTime;
        //
        onBalanceReportImpl(report.amount(), report.time());
    }

    private RunningMetrics(RunningMetrics toCopy) {
        this.finalReportTime = toCopy.finalReportTime;
        //
        this.balance = toCopy.balance;
        this.cappedUsableMargin = toCopy.cappedUsableMargin;
        this.usableMargin = toCopy.usableMargin;
        this.maxMargin = toCopy.maxMargin;
        this.time = toCopy.time;
        this.excludeMarginFromBalance = toCopy.excludeMarginFromBalance;
    }

    @Override
    public Metrics copy() {
        return new RunningMetrics(this);
    }

    @Override
    public double usableMargin() {
        return cappedUsableMargin;
    }

    @Override
    public double balance() {
        return balance;
    }

    @Override
    public long time() {
        return time;
    }

    @Override
    public Metrics onPeriodStart(@NotNull ReferencePeriod period) {
        time = period.start();
        if (excludeMarginFromBalance && cappedUsableMargin > EPS && balance > EPS) {
            this.balance -= cappedUsableMargin;
        }
        this.maxMargin = period.availableMargin();
        recalculateUsableMargin(maxMargin - usableMargin);
        return this;
    }

    @Override
    public Metrics onBalanceReport(@NotNull BalanceReport report) {
        onBalanceReportImpl(report.amount(), report.time());
        return this;
    }

    private void onBalanceReportImpl(double amount, long time) {
        this.time = time;
        double marginDiff = amount - this.balance;
        this.balance = amount;
        recalculateUsableMargin(marginDiff);
    }

    @Override
    public Metrics onOperation(@NotNull Operation operation) {
        this.time = operation.time();
        this.balance += operation.cost();
        recalculateUsableMargin(0d);
        return this;
    }

    @Override
    public Metrics onPeriodEnd(@NotNull ReferencePeriod period) {
        this.time = period.end();
        this.excludeMarginFromBalance = period.relatesTo(finalReportTime) != PERIOD_IS_BEFORE;
        return this;
    }

    private void recalculateUsableMargin(double diff) {
        this.usableMargin = Math.min(usableMargin + diff, maxMargin);
        this.cappedUsableMargin = Math.min(Math.max(0, balance), this.usableMargin);
    }

    @Override
    public TimedBalance toTimedBalance() {
        return new TimedBalanceWithMarginImpl(balance, time, Double.NaN);
    }
}
