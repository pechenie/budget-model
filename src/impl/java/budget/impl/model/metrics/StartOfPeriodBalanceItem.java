package budget.impl.model.metrics;

import budget.model.ReferencePeriod;

/**
 * Balance item representing period's start
 *
 * @author Kirill Berezin
 */
final class StartOfPeriodBalanceItem implements BalanceItem {
    private final ReferencePeriod period;

    StartOfPeriodBalanceItem(ReferencePeriod period) {
        this.period = period;
    }

    @Override
    public long time() {
        return period.start();
    }

    @Override
    public <T> T visitBy(BalanceItemVisitor<T> visitor) {
        return visitor.onPeriodStart(period);
    }
}
