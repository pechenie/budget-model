package budget.impl.model.metrics;

import org.jetbrains.annotations.NotNull;

import java.util.Collection;

/**
 * Abstract left fold to minimum
 *
 * @author Kirill Berezin
 */
abstract class AbstractFoldToMin implements Fold<BalanceItem, Metrics> {
    private static final double EPS = 0.000000001;

    private final BalanceItemVisitor<Metrics> visitor;

    AbstractFoldToMin(BalanceItemVisitor<Metrics> visitor) {
        this.visitor = visitor;
    }

    @Override
    public final Metrics reduce(@NotNull Metrics identity, @NotNull Collection<BalanceItem> items) {
        Metrics min = identity.copy();
        for (BalanceItem rb : items) {
            min = min(min, rb.visitBy(visitor));
        }
        return min;
    }

    @NotNull
    private Metrics min(@NotNull Metrics min, @NotNull Metrics candidate) {
        if (value(min) < value(candidate) + EPS) {
            return min;
        }
        return candidate.copy();
    }

    abstract double value(@NotNull Metrics metrics);
}
