package budget.impl.model.metrics;

import org.jetbrains.annotations.NotNull;

import java.util.Collection;

/**
 * Reduction interface
 *
 * @author Kirill Berezin
 */
interface Fold<T, R> {
    R reduce(@NotNull R identity, @NotNull Collection<T> items);
}
