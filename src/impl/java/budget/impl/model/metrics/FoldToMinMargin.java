package budget.impl.model.metrics;

import org.jetbrains.annotations.NotNull;

/**
 * Fold to minimal margin implementation
 *
 * @author Kirill Berezin
 */
final class FoldToMinMargin extends AbstractFoldToMin {
    FoldToMinMargin(BalanceItemVisitor<Metrics> visitor) {
        super(visitor);
    }

    @Override
    double value(@NotNull Metrics metrics) {
        return metrics.usableMargin();
    }
}
