package budget.impl.model.metrics;

import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.function.Predicate;

/**
 * Abstract left fold until criteria is met
 *
 * @author Kirill Berezin
 */
abstract class AbstractFoldUntil implements Fold<BalanceItem, Metrics> {
    private final BalanceItemVisitor<Metrics> visitor;
    private final Predicate<Double> criteria;

    AbstractFoldUntil(BalanceItemVisitor<Metrics> visitor, Predicate<Double> criteria) {
        this.visitor = visitor;
        this.criteria = criteria;
    }

    @Override
    public Metrics reduce(@NotNull Metrics identity, @NotNull Collection<BalanceItem> items) {
        Metrics report = identity;
        if (criteria.test(value(report))) {
            return report;
        }

        for (BalanceItem rb : items) {
            report = rb.visitBy(visitor);
            if (criteria.test(value(report))) {
                return report;
            }
        }
        return null;
    }

    abstract double value(Metrics metrics);
}
