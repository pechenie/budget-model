package budget.impl.model.metrics;

import org.jetbrains.annotations.NotNull;

/**
 * Fold to minimal balance implementation
 *
 * @author Kirill Berezin
 */
final class FoldToMinBalance extends AbstractFoldToMin {
    FoldToMinBalance(BalanceItemVisitor<Metrics> visitor) {
        super(visitor);
    }

    @Override
    double value(@NotNull Metrics metrics) {
        return metrics.balance();
    }
}
