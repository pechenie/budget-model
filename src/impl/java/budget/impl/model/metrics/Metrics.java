package budget.impl.model.metrics;

import budget.view.TimedBalance;

/**
 * @author Kirill Berezin
 */
interface Metrics {
    double usableMargin();

    double balance();

    long time();

    TimedBalance toTimedBalance();

    Metrics copy();
}
