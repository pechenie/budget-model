package budget.impl.model.metrics;

import budget.model.ReferencePeriod;

/**
 * Balance item representing period's end
 *
 * @author Kirill Berezin
 */
final class EndOfPeriodBalanceItem implements BalanceItem {
    private final ReferencePeriod period;

    EndOfPeriodBalanceItem(ReferencePeriod period) {
        this.period = period;
    }

    @Override
    public long time() {
        return period.end();
    }

    @Override
    public <T> T visitBy(BalanceItemVisitor<T> visitor) {
        return visitor.onPeriodEnd(period);
    }
}
