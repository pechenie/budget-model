package budget.impl.model.metrics;

import budget.model.BalanceReport;
import budget.model.Operation;
import budget.model.ReferencePeriod;
import budget.model.source.BalanceReports;
import budget.model.source.Operations;
import budget.model.source.ReferencePeriods;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

import static budget.model.ReferencePeriod.Relation.PERIOD_IS_AFTER;

/**
 * Base implementation which collects (in order) all necessary entities from provided sources
 *
 * @author Kirill Berezin
 */
class BalanceItemsImpl implements BalanceItems {
    private final BalanceReports balanceReports;
    private final Operations operations;
    private final ReferencePeriods referencePeriods;

    BalanceItemsImpl(@NotNull BalanceReports balanceReports, @NotNull Operations operations,
                     @NotNull ReferencePeriods referencePeriods) {
        this.balanceReports = balanceReports;
        this.operations = operations;
        this.referencePeriods = referencePeriods;
    }

    @NotNull
    @Override
    public Collection<BalanceItem> balances(long from, long to, boolean itemsInclusive, boolean periodsInclusive) {
        List<BalanceItem> balanceItemList = new ArrayList<>();

        IteratorsMerger<BalanceReport, Operation, BalanceItem> merger = new IteratorsMerger<>(
                balanceReports.range(from, false, to, itemsInclusive).iterator(),
                operations.range(from, false, to, itemsInclusive).iterator(),
                ReportBalanceItem::new, OperationBalanceItem::new, Comparator.comparingLong(BalanceItem::time)
        );

        BalanceItem item;
        while ((item = merger.nextWhile(bi -> referencePeriods.unite().relatesTo(bi.time()) == PERIOD_IS_AFTER)) != null) {
            balanceItemList.add(item);
        }

        for (ReferencePeriod period : referencePeriods.range(from, to, periodsInclusive)) {
            if (period.start() >= from) {
                balanceItemList.add(new StartOfPeriodBalanceItem(period));
            }

            while ((item = merger.nextWhile(bi -> period.includes(bi.time()))) != null) {
                balanceItemList.add(item);
            }

            if (!period.includes(to)) {
                balanceItemList.add(new EndOfPeriodBalanceItem(period));
            }
        }

        while ((item = merger.nextWhile(bi -> true)) != null) {
            balanceItemList.add(item);
        }
        return balanceItemList;
    }

    private static final class IteratorsMerger<E1, E2, T> {
        private final Iterator<E1> iteratorOne;
        private final Iterator<E2> iteratorTwo;

        private final Function<E1, T> mapperOne;
        private final Function<E2, T> mapperTwo;

        private final Comparator<T> comparator;

        private T nextOne;
        private T nextTwo;

        private IteratorsMerger(Iterator<E1> iteratorOne, Iterator<E2> iteratorTwo, Function<E1, T> mapperOne, Function<E2, T> mapperTwo,
                                Comparator<T> comparator) {
            this.iteratorOne = iteratorOne;
            this.iteratorTwo = iteratorTwo;
            this.mapperOne = mapperOne;
            this.mapperTwo = mapperTwo;
            this.comparator = comparator;
        }

        @Nullable
        T nextWhile(Predicate<T> predicate) {
            if (nextOne == null && iteratorOne.hasNext()) {
                nextOne = mapperOne.apply(iteratorOne.next());
            }
            if (nextTwo == null && iteratorTwo.hasNext()) {
                nextTwo = mapperTwo.apply(iteratorTwo.next());
            }

            if (nextOne == null && nextTwo == null) {
                return null;
            } else if (nextOne == null) {
                return testTwoAndGet(predicate);
            } else if (nextTwo == null) {
                return testOneAndGet(predicate);
            } else {
                if (comparator.compare(nextOne, nextTwo) > 0) {
                    return testTwoAndGet(predicate);
                } else {
                    return testOneAndGet(predicate);
                }
            }
        }

        @Nullable
        private T testOneAndGet(Predicate<T> predicate) {
            if (predicate.test(nextOne)) {
                T result = nextOne;
                nextOne = null;
                return result;
            }
            return null;
        }

        @Nullable
        private T testTwoAndGet(Predicate<T> predicate) {
            if (predicate.test(nextTwo)) {
                T result = nextTwo;
                nextTwo = null;
                return result;
            }
            return null;
        }
    }
}
