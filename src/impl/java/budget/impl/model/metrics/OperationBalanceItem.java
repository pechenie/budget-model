package budget.impl.model.metrics;

import budget.model.Operation;
import org.jetbrains.annotations.NotNull;

/**
 * Balance item representing operation
 *
 * @author Kirill Berezin
 */
final class OperationBalanceItem implements BalanceItem {
    private final Operation operation;

    OperationBalanceItem(@NotNull Operation operation) {
        this.operation = operation;
    }

    @Override
    public long time() {
        return operation.time();
    }

    @Override
    public <T> T visitBy(BalanceItemVisitor<T> visitor) {
        return visitor.onOperation(operation);
    }
}
