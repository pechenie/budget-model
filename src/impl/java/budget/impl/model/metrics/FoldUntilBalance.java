package budget.impl.model.metrics;

import java.util.function.Predicate;

/**
 * Fold until balance criteria is met implementation
 *
 * @author Kirill Berezin
 */
final class FoldUntilBalance extends AbstractFoldUntil {

    FoldUntilBalance(BalanceItemVisitor<Metrics> visitor, Predicate<Double> criteria) {
        super(visitor, criteria);
    }

    @Override
    double value(Metrics metrics) {
        return metrics.balance();
    }
}
