package budget.impl.model.metrics;

import org.jetbrains.annotations.NotNull;

import java.util.Collection;

/**
 * Low-level balance items collector
 *
 * @author Kirill Berezin
 */
interface BalanceItems {
    @NotNull
    default Collection<BalanceItem> balances(long from, long to, boolean inclusive) {
        return balances(from, to, inclusive, inclusive);
    }

    @NotNull
    Collection<BalanceItem> balances(long from, long to, boolean itemsInclusive, boolean periodsInclusive);
}

