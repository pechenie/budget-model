package budget.impl.model.metrics;

import budget.model.BalanceReport;
import budget.model.Operation;
import budget.model.ReferencePeriod;
import org.jetbrains.annotations.NotNull;

/**
 * Visitor of balance items
 * @author Kirill Berezin
 */
interface BalanceItemVisitor<T> {
    /**
     * Called when period's start occurred
     *
     * @param period period
     * @return result
     */
    T onPeriodStart(@NotNull ReferencePeriod period);

    /**
     * Called when balance report occured
     *
     * @param report report
     * @return result
     */
    T onBalanceReport(@NotNull BalanceReport report);

    /**
     * Called when operation occurred
     * @param operation operation
     * @return result
     */
    T onOperation(@NotNull Operation operation);

    /**
     * Called when period's end occurred
     * @param period period
     * @return result
     */
    T onPeriodEnd(@NotNull ReferencePeriod period);
}
