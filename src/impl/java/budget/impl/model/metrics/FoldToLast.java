package budget.impl.model.metrics;

import org.jetbrains.annotations.NotNull;

import java.util.Collection;

/**
 * Basic fold left implementation
 *
 * @author Kirill Berezin
 */
final class FoldToLast implements Fold<BalanceItem, Metrics> {
    private final BalanceItemVisitor<Metrics> visitor;

    FoldToLast(BalanceItemVisitor<Metrics> visitor) {
        this.visitor = visitor;
    }

    @Override
    public Metrics reduce(@NotNull Metrics identity, @NotNull Collection<BalanceItem> items) {
        Metrics report = identity;
        for (BalanceItem rb : items) {
            report = rb.visitBy(visitor);
        }
        return report;
    }
}
