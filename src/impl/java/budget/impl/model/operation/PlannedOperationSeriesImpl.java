package budget.impl.model.operation;

import budget.impl.model.entity.OperationImpl;
import budget.model.Operation;
import budget.model.operation.PlannedOperationSeries;
import budget.model.time.TimestampIterator;
import org.jetbrains.annotations.NotNull;

import java.util.function.Consumer;
import java.util.stream.LongStream;

/**
 * Default planned series implementation.
 *
 * @author Kirill Berezin
 */

public final class PlannedOperationSeriesImpl implements PlannedOperationSeries {
    private final String name;
    private final Operation initial;
    private final TimestampIterator iterator;
    private final int operationsCount;

    /**
     * Series are created by providing a series name, intial operations, timestamp iterator and necessary operations count
     *
     * @param name             name of a series
     * @param initialOperation operation to start with
     * @param iterator         iterator to produce following operations times
     * @param operationsCount  required operations count
     */
    public PlannedOperationSeriesImpl(String name, Operation initialOperation, TimestampIterator iterator, int operationsCount) {
        this.name = name;
        this.initial = initialOperation;
        this.iterator = iterator;
        this.operationsCount = operationsCount;
    }

    @NotNull
    @Override
    public String name() {
        return name;
    }

    @Override
    public void processSeries(@NotNull Consumer<Operation> nextOperationConsumer) {
        LongStream.iterate(initial.time(), iterator::next).limit(operationsCount)
                .mapToObj(time -> new OperationImpl(time, initial.cost()))
                .forEach(nextOperationConsumer);
    }

    @Override
    public int size() {
        return operationsCount;
    }
}
