package budget.impl.model.operation;

import budget.impl.model.entity.OperationImpl;
import budget.model.Operation;
import budget.model.Schedule;
import budget.model.book.OperationRecordBook;
import budget.model.operation.PlannedOperationSeries;
import budget.model.operation.PlannedOperations;
import budget.model.source.Operations;
import budget.model.tag.Tag;
import budget.model.tag.Tags;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.HashMap;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Default planned operations implementation
 *
 * @author Kirill Berezin
 */

public final class PlannedOperationsImpl implements PlannedOperations, OperationRecordBook {
    @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
    private final static SortedSet<Long> EMPTY_SET = new TreeSet<>();

    private final Operations operations;
    private final OperationRecordBook book;
    private final Schedule schedule;
    private final PlannedOperationsTagDomain domain;
    private final Tags tags;

    private final HashMap<String, SortedSet<Long>> reverseIndex;

    public PlannedOperationsImpl(@NotNull Operations operations, @NotNull OperationRecordBook book, @NotNull Schedule schedule,
                                 @NotNull PlannedOperationsTagDomain domain, @NotNull Tags tags) {
        this.operations = operations;
        this.book = book;
        this.schedule = schedule;
        this.domain = domain;
        this.tags = tags;
        this.reverseIndex = buildIndex(tags, domain);
    }

    private HashMap<String, SortedSet<Long>> buildIndex(Tags tags, PlannedOperationsTagDomain domain) {
        HashMap<String, SortedSet<Long>> index = new HashMap<>();
        tags.tags().forEach(t -> {
            String seriesNameKey = domain.seriesNameKey();
            if (t.hasEntry(seriesNameKey)) {
                index(index, t.valueFor(seriesNameKey), t.key());
            }
        });
        return index;
    }

    private void index(HashMap<String, SortedSet<Long>> index, String seriesName, long time) {
        index.computeIfAbsent(seriesName, s -> new TreeSet<>()).add(time);
    }

    @NotNull
    @Override
    public Collection<? extends PlannedOperationSeries> plannedSeries() {
        return reverseIndex.entrySet().stream()
                .filter(entry -> entry.getValue().size() > 0)
                .map(entry -> new FirstOperationSeries(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());
    }

    @Override
    public boolean plan(@NotNull PlannedOperationSeries series) {
        if (reverseIndex.getOrDefault(series.name(), EMPTY_SET).size() > 0) {
            return false;
        }
        series.processSeries(new TaggingOperationConsumer(series.name()));
        return true;
    }

    @Override
    public void delete(@NotNull String name) {
        SortedSet<Long> set = reverseIndex.get(name);
        if (set != null) {
            set.forEach(time -> {
                book.eraseRecord(time);
                tags.remove(time);
            });
            reverseIndex.remove(name);
        }

    }

    @Override
    public boolean execute(@NotNull Operation operation) {
        return book.execute(operation);
    }

    @Override
    public boolean hasRecord(long time) {
        return book.hasRecord(time);
    }

    @Override
    public void eraseRecord(long time) {
        //delete tags first
        eraseTagAndLinks(time);
        //and only now delete record
        book.eraseRecord(time);
    }

    private void eraseTagAndLinks(long time) {
        Tag tag = tags.tagBy(time);
        if (tag.hasEntry(domain.seriesNameKey())) {
            String seriesName = tag.valueFor(domain.seriesNameKey());
            //can't be null
            reverseIndex.get(seriesName).remove(time);
            if (reverseIndex.get(seriesName).isEmpty()) {
                //no more items in the series
                reverseIndex.remove(seriesName);
            } else {
                //now fix linked list
                Tag prev = null, next = null;
                if (tag.hasEntry(domain.previousOperationKey())) {
                    prev = tags.tagBy(tag.longValueFor(domain.previousOperationKey()));
                }
                if (tag.hasEntry(domain.nextOperationKey())) {
                    next = tags.tagBy(tag.longValueFor(domain.nextOperationKey()));
                }

                if (prev != null) {
                    if (next == null) {
                        //it was the last operation, remove next pointer
                        prev = prev.removeEntry(domain.nextOperationKey());
                    } else {
                        prev = prev.withEntry(domain.nextOperationKey(), String.valueOf(next.key()));
                    }
                    tags.update(prev);
                }
                if (next != null) {
                    if (prev == null) {
                        //it was the first operation, remove prev pointer
                        next = next.removeEntry(domain.previousOperationKey());
                    } else {
                        next = next.withEntry(domain.previousOperationKey(), String.valueOf(prev.key()));
                    }
                    tags.update(next);
                }
            }
        }
        tags.remove(time);
    }

    private class TaggingOperationConsumer implements Consumer<Operation> {
        private final String seriesName;
        private Tag previousTag;

        private TaggingOperationConsumer(String seriesName) {
            this.seriesName = seriesName;
        }

        @Override
        public void accept(Operation operation) {
            long time = operation.time();
            while (schedule.busy(time)) {
                time++;
            }

            Tag currentTag = tags.tagBy(time);
            if (previousTag != null) {
                currentTag = currentTag.withEntry(domain.previousOperationKey(), String.valueOf(previousTag.key()));
                tags.update(previousTag = previousTag.withEntry(domain.nextOperationKey(), String.valueOf(time)));
            }
            tags.update(currentTag = currentTag.withEntry(domain.seriesNameKey(), seriesName));
            executeOperation(operation, time);

            previousTag = currentTag;
        }

        private void executeOperation(Operation operation, long time) {
            if (!book.execute(new OperationImpl(time, operation.cost()))) {
                throw new IllegalStateException("Schedule is inconsistent!");
            }
            index(reverseIndex, seriesName, time);
        }
    }

    private class FirstOperationSeries implements PlannedOperationSeries {
        private final String name;
        private final SortedSet<Long> times;

        private FirstOperationSeries(String name, SortedSet<Long> times) {
            this.name = name;
            this.times = times;
        }

        @NotNull
        @Override
        public String name() {
            return name;
        }

        @Override
        public void processSeries(@NotNull Consumer<Operation> nextOperationConsumer) {
            nextOperationConsumer.accept(operations.operationAt(times.first()));
        }

        @Override
        public int size() {
            return times.size();
        }
    }
}
