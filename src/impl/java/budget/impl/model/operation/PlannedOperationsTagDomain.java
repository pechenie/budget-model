package budget.impl.model.operation;

import org.jetbrains.annotations.NotNull;

/**
 * Planned operations tag keys domain
 * @author Kirill Berezin
 */

public interface PlannedOperationsTagDomain {
    /**
     * Key for fetching previous operation time
     *
     * @return key
     */
    @NotNull
    String previousOperationKey();

    /**
     * Key for fetching next operation time
     *
     * @return key
     */
    @NotNull
    String nextOperationKey();

    /**
     * Key for fetching series name
     * @return key
     */
    @NotNull
    String seriesNameKey();
}
