package budget.impl.model;

import budget.model.Schedule;
import budget.model.book.RecordBook;

import java.util.Arrays;
import java.util.Collection;

/**
 * Schedule implementation based on a set of books provided
 *
 * @since 29.04.2017
 */
public class BookSchedule implements Schedule {
    private final Collection<RecordBook> books;

    public BookSchedule(RecordBook... books) {
        this.books = Arrays.asList(books);
    }

    @Override
    public boolean busy(long time) {
        for (RecordBook book : books) {
            if (book.hasRecord(time)) {
                return true;
            }
        }
        return false;
    }
}
