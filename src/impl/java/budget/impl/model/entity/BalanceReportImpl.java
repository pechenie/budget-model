package budget.impl.model.entity;

import budget.model.BalanceReport;

/**
 * Default simple implementation of a balance report
 *
 * @since 20.04.2017
 */
public final class BalanceReportImpl implements BalanceReport {
    private final long time;
    private final double amount;

    public BalanceReportImpl(long time, double amount) {
        this.time = time;
        this.amount = amount;
    }

    @Override
    public long time() {
        return time;
    }

    @Override
    public double amount() {
        return amount;
    }
}
