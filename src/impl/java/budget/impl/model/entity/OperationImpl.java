package budget.impl.model.entity;

import budget.model.Operation;

/**
 * Default simple implementation of an operation

 * @since 20.04.2017
 */
public final class OperationImpl implements Operation {
    private final long time;
    private final double amount;

    public OperationImpl(long time, double amount) {
        this.time = time;
        this.amount = amount;
    }

    @Override
    public long time() {
        return time;
    }

    @Override
    public double cost() {
        return amount;
    }
}
