package budget.impl.model.entity;

import budget.model.ReferencePeriod;

/**
 * Default simple implementation of a reference period

 * @since 20.04.2017
 */
public final class ReferencePeriodImpl implements ReferencePeriod {
    private final long start;
    private final long end;
    private final double overheadCash;

    public ReferencePeriodImpl(long start, long end, double overheadCash) {
        this.start = start;
        this.end = end;
        this.overheadCash = overheadCash;
    }

    @Override
    public long start() {
        return start;
    }

    @Override
    public long end() {
        return end;
    }

    @Override
    public double availableMargin() {
        return overheadCash;
    }
}
