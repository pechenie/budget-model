package budget.impl.model.tag;

import budget.impl.model.persistence.TagsPersistence;
import budget.model.tag.Tag;
import budget.model.tag.Tags;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.HashMap;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Decorator over tags, which persists all updates to decorated tags in a storage
 *
 * @author Kirill Berezin
 */

public class PersistableTags implements Tags {
    private final TagsPersistence tagPersistence;
    private final Tags decorated;

    public PersistableTags(TagsPersistence tagPersistence) {
        this.tagPersistence = tagPersistence;
        this.decorated = new TagsImpl(new HashMap<>(tagPersistence.readTags().stream()
                .collect(Collectors.toMap(Tag::key, Function.identity()))));
    }

    @Override
    public void remove(long time) {
        decorated.remove(time);
        tagPersistence.remove(time);
    }

    @Override
    public void update(@NotNull Tag tag) {
        decorated.update(tag);
        if (tag.allKeys().size() > 0) {
            tagPersistence.store(tag);
        } else {
            tagPersistence.remove(tag.key());
        }
    }

    @NotNull
    @Override
    public Tag tagBy(long time) {
        return decorated.tagBy(time);
    }

    @NotNull
    @Override
    public Collection<? extends Tag> tags() {
        return decorated.tags();
    }
}
