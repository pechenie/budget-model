package budget.impl.model.tag;

import budget.model.tag.Tag;
import budget.model.tag.Tags;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;

/**
 * Default tags implementation. Based on a hash map
 * @author Kirill Berezin
 */

public class TagsImpl implements Tags {
    private final HashMap<Long, Tag> tags;

    public TagsImpl(HashMap<Long, Tag> tags) {
        this.tags = tags;
    }

    @Override
    public void remove(long time) {
        tags.remove(time);
    }

    @Override
    public void update(@NotNull Tag tag) {
        tags.put(tag.key(), tag);
    }

    @NotNull
    @Override
    public Tag tagBy(long time) {
        return tags.getOrDefault(time, new EmptyTag(time));
    }

    @NotNull
    @Override
    public Collection<? extends Tag> tags() {
        return tags.values();
    }

    private static final class EmptyTag implements Tag {
        private final long key;

        private EmptyTag(long key) {
            this.key = key;
        }

        @Override
        public long key() {
            return key;
        }

        @Override
        public boolean hasEntry(@NotNull String key) {
            return false;
        }

        @NotNull
        @Override
        public Tag withEntry(@NotNull String key, @NotNull String value) {
            return new TagImpl(this.key, new HashMap<>()).withEntry(key, value);
        }

        @NotNull
        @Override
        public Tag removeEntry(@NotNull String key) {
            return this;
        }

        @NotNull
        @Override
        public String valueFor(@NotNull String key) {
            return "";
        }

        @Override
        public Collection<String> allKeys() {
            return Collections.emptySet();
        }
    }
}
