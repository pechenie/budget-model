package budget.impl.model.tag;

import budget.model.tag.Tag;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Default tag implementation. Based on a hash map
 * @author Kirill Berezin
 */

public class TagImpl implements Tag {
    private final long key;
    private final HashMap<String, String> values;

    public TagImpl(long key, Map<String, String> values) {
        this.key = key;
        this.values = new HashMap<>(values);
    }

    @Override
    public long key() {
        return key;
    }

    @Override
    public boolean hasEntry(@NotNull String key) {
        return values.containsKey(key);
    }

    @NotNull
    @Override
    public Tag withEntry(@NotNull String key, @NotNull String value) {
        TagImpl newTag = new TagImpl(this.key, this.values);
        newTag.values.put(key, value);
        return newTag;
    }

    @NotNull
    @Override
    public Tag removeEntry(@NotNull String key) {
        if (hasEntry(key)) {
            TagImpl newTag = new TagImpl(this.key, this.values);
            newTag.values.remove(key);
            return newTag;
        }
        return this;
    }

    @NotNull
    @Override
    public String valueFor(@NotNull String key) {
        return values.getOrDefault(key, "");
    }

    @Override
    public Collection<String> allKeys() {
        return values.keySet();
    }
}
