package budget.impl.model.search;

import java.util.List;

/**
 * Basic interface for search an entity by a specified key
 *
 * @author Kirill Berezin
 */
public interface Search<E, K> {
    /**
     * Returns an index of an entity within a collection
     *
     * @param key      key to lookup
     * @param entities list of entities
     * @return an index of an entity which corresponds to a key, or 0 if all entities are "greater"
     * or (size - 1) if all entities are "lesser"
     */
    int findIndexOf(K key, List<? extends E> entities);
}
