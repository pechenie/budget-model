package budget.impl.model.search;

import java.util.List;

/**
 * Base binary search implementation of a search of an entity by a specified key
 *
 * @author Kirill Berezin
 */
public abstract class BinarySearch<E, K> implements Search<E, K> {
    @Override
    public int findIndexOf(K key, List<? extends E> entities) {
        int low = 0;
        int high = entities.size() - 1;

        while (low <= high) {
            int mid = (low + high) >>> 1;
            E midVal = entities.get(mid);
            int comparison = comparison(key, midVal);

            if (comparison > 0)
                low = mid + 1;
            else if (comparison < 0)
                high = mid - 1;
            else
                return mid;
        }
        return Math.max(0, Math.min(entities.size() - 1, low));
    }

    protected abstract int comparison(K key, E entity);
}
