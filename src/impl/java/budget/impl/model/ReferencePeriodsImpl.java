package budget.impl.model;

import budget.impl.model.entity.ReferencePeriodImpl;
import budget.model.ReferencePeriod;
import budget.model.book.PeriodBook;
import budget.model.source.ReferencePeriods;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * Default reference periods read-write book implementation. Based on a tree map
 *
 * @since 15.04.2017
 */
public final class ReferencePeriodsImpl implements ReferencePeriods, PeriodBook {
    private final TreeMap<Long, ReferencePeriod> referencePeriods;

    public ReferencePeriodsImpl(@NotNull Collection<? extends ReferencePeriod> referencePeriods) {
        this.referencePeriods = new TreeMap<>();
        if (!mergeIfConsistent(this.referencePeriods, referencePeriods)) {
            throw new IllegalArgumentException("Periods with gaps are considered invalid");
        }
    }

    @Override
    public ReferencePeriod periodAtTime(long time) {
        Map.Entry<Long, ReferencePeriod> entry = referencePeriods.floorEntry(time);
        if (entry != null && entry.getValue().includes(time)) {
            return entry.getValue();
        }
        return null;
    }

    @NotNull
    @Override
    public Collection<ReferencePeriod> periods() {
        return referencePeriods.values();
    }

    @NotNull
    @Override
    public Collection<ReferencePeriod> range(long from, long to, boolean inclusiveTo) {
        Long fromKey = floorKey(from);
        if (fromKey > to) {
            return Collections.emptySet();
        }
        return referencePeriods.tailMap(fromKey, true)
                .headMap(to, true).values().stream()
                .filter(period -> period.relatesTo(from) != ReferencePeriod.Relation.PERIOD_IS_BEFORE
                        && (inclusiveTo || !period.includes(to)))
                .collect(Collectors.toList());
    }

    private Long floorKey(long from) {
        Long floorKey = referencePeriods.floorKey(from);
        return floorKey == null ? from : floorKey;
    }

    @NotNull
    @Override
    public ReferencePeriod unite() {
        if (this.referencePeriods.isEmpty()) {
            return new ReferencePeriodImpl(Long.MIN_VALUE, Long.MIN_VALUE, Double.NaN);
        } else {
            return new ReferencePeriodImpl(referencePeriods.firstEntry().getValue().start(),
                    referencePeriods.lastEntry().getValue().end(), Double.NaN);
        }
    }

    @Override
    public boolean append(@NotNull ReferencePeriod newPeriod) {
        if (referencePeriods.isEmpty()) {
            referencePeriods.put(newPeriod.start(), newPeriod);
            return true;
        } else {
            return putIfNoGap(referencePeriods, newPeriod, referencePeriods.lastEntry().getValue());
        }
    }

    private boolean putIfNoGap(TreeMap<Long, ReferencePeriod> map, ReferencePeriod candidate, ReferencePeriod lastPeriod) {
        if (checkNoGapOrOverlap(lastPeriod, candidate)) {
            map.put(candidate.start(), candidate);
            return true;
        }
        return false;
    }

    private boolean checkNoGapOrOverlap(ReferencePeriod prev, ReferencePeriod next) {
        return prev.relatesTo(next.start()) == ReferencePeriod.Relation.PERIOD_IS_BEFORE &&
                (!prev.includes(prev.end()) && next.includes(prev.end())
                        || prev.includes(prev.end()) && next.includes(prev.end() + 1));
    }

    @Override
    public boolean rewrite(List<? extends ReferencePeriod> periods) {
        return mergeIfConsistent(referencePeriods, periods);
    }

    private boolean mergeIfConsistent(TreeMap<Long, ReferencePeriod> map, Collection<? extends ReferencePeriod> periods) {
        if (periods.isEmpty()) {
            return true;
        }

        List<? extends ReferencePeriod> sortedList = periods.stream()
                .sorted(Comparator.comparingLong(ReferencePeriod::start)).collect(Collectors.toList());
        TreeMap<Long, ReferencePeriod> newMap = new TreeMap<>();
        newMap.putAll(map.headMap(sortedList.get(0).start(), false));
        sortedList.forEach(p -> newMap.put(p.start(), p));
        newMap.putAll(map.tailMap(sortedList.get(sortedList.size() - 1).end(), true));

        ReferencePeriod prev = null;
        for (ReferencePeriod period : newMap.values()) {
            if (prev != null) {
                if (!checkNoGapOrOverlap(prev, period)) {
                    return false;
                }
            }
            prev = period;
        }

        map.clear();
        map.putAll(newMap);
        return true;
    }
}
