package budget.impl.model;

import budget.model.ReferencePeriod;
import org.jetbrains.annotations.NotNull;

/**
 * Reference period decorator defining open interval on both sides with the same interval bounds.
 *
 * @since 28.04.2017
 */
public final class OpenReferencePeriodDecorator implements ReferencePeriod {
    private final ReferencePeriod decorated;

    public OpenReferencePeriodDecorator(ReferencePeriod decorated) {
        this.decorated = decorated;
    }

    @Override
    public long start() {
        return decorated.start();
    }

    @Override
    public long end() {
        return decorated.end();
    }

    @NotNull
    @Override
    public Relation relatesTo(long time) {
        if (time <= start()) {
            return Relation.PERIOD_IS_AFTER;
        } else if (end() <= time) {
            return Relation.PERIOD_IS_BEFORE;
        } else {
            return Relation.PERIOD_INCLUDES;
        }
    }

    @Override
    public boolean includes(long time) {
        return time > start() && time < end();
    }

    @Override
    public double availableMargin() {
        return decorated.availableMargin();
    }
}
