package budget.impl.model.persistence;

import budget.model.Operation;
import budget.model.book.OperationRecordBook;
import org.jetbrains.annotations.NotNull;

/**
 * Operations book decorator, which stores any successful updates to a book
 *
 * @author Kirill Berezin
 */
public class PersistableOperationBook implements OperationRecordBook {
    private final JournalPersistence persistence;
    private final OperationRecordBook decorated;

    public PersistableOperationBook(JournalPersistence persistence, OperationRecordBook decorated) {
        this.persistence = persistence;
        this.decorated = decorated;
    }

    @Override
    public boolean execute(@NotNull Operation operation) {
        if (decorated.execute(operation)) {
            persistence.store(operation);
            return true;
        }
        return false;
    }

    @Override
    public boolean hasRecord(long time) {
        return decorated.hasRecord(time);
    }

    @Override
    public void eraseRecord(long time) {
        decorated.eraseRecord(time);
        persistence.remove(time);
    }
}
