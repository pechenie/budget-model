package budget.impl.model.persistence;

import budget.api.entity.RuleView;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Storage interface for period rules
 *
 * @author Kirill Berezin
 */

public interface PeriodRulesPersistence<View extends RuleView, Input> {
    /**
     * Saves selected period rule defined by it's view and input
     *
     * @param view  view of a rule
     * @param input input
     */
    void save(@NotNull View view, @NotNull Input input);

    /**
     * Returns saved rule id
     *
     * @return id
     */
    @Nullable
    String savedId();

    /**
     * Returns saved input
     * @return input
     */
    @Nullable
    Input savedInput();
}
