package budget.impl.model.persistence;

import budget.model.BalanceReport;
import budget.model.book.BalanceRecordBook;
import org.jetbrains.annotations.NotNull;

/**
 * Balance book decorator, which stores any successful updates to a book
 *
 * @author Kirill Berezin
 */

public class PersistableBalanceBook implements BalanceRecordBook {
    private final JournalPersistence persistence;
    private final BalanceRecordBook decorated;

    public PersistableBalanceBook(JournalPersistence persistence, BalanceRecordBook decorated) {
        this.persistence = persistence;
        this.decorated = decorated;
    }

    @Override
    public boolean report(@NotNull BalanceReport balanceReport) {
        if (decorated.report(balanceReport)) {
            persistence.store(balanceReport);
            return true;
        }
        return false;
    }

    @Override
    public boolean hasRecord(long time) {
        return decorated.hasRecord(time);
    }

    @Override
    public void eraseRecord(long time) {
        decorated.eraseRecord(time);
        persistence.remove(time);
    }
}
