package budget.impl.model.persistence;

import budget.model.BalanceReport;
import budget.model.Operation;
import budget.model.ReferencePeriod;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;

/**
 * Persistence for journal entities
 *
 * @author Kirill Berezin
 */

public interface JournalPersistence {
    /**
     * Saves operations into underlying storage. May be asynchronous
     *
     * @param operation operation to save
     */
    void store(@NotNull Operation operation);

    /**
     * Saves balance report into underlying storage. May be asynchronous
     * @param balanceReport balance report to save
     */
    void store(@NotNull BalanceReport balanceReport);

    /**
     * Replaces all stored reference periods with provided collection of periods
     * @param referencePeriods reference periods to store
     */
    void replaceAll(@NotNull Collection<? extends ReferencePeriod> referencePeriods);

    /**
     * Removes an operation or a balance report with the specified time
     * @param recordAtTime time of a record to remove
     */
    void remove(long recordAtTime);

    /**
     * Reads all operations from storage
     * @return stored operations
     */
    @NotNull
    Collection<? extends Operation> readOperations();

    /**
     * Reads all balance reports from storage
     * @return stored balance reports
     */
    @NotNull
    Collection<? extends BalanceReport> readReports();

    /**
     * Reads all reference periods from storage
     * @return stored reference periods
     */
    @NotNull
    Collection<? extends ReferencePeriod> readPeriods();
}
