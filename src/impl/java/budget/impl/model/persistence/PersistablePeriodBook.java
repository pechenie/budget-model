package budget.impl.model.persistence;

import budget.model.ReferencePeriod;
import budget.model.book.PeriodBook;
import budget.model.source.ReferencePeriods;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Period book decorator, which stores any successful updates to a book
 *
 * @author Kirill Berezin
 */

public class PersistablePeriodBook implements PeriodBook {
    private final JournalPersistence persistence;
    private final ReferencePeriods periods;
    private final PeriodBook decorated;

    public PersistablePeriodBook(@NotNull JournalPersistence persistence, @NotNull ReferencePeriods periods,
                                 @NotNull PeriodBook decorated) {
        this.persistence = persistence;
        this.periods = periods;
        this.decorated = decorated;
    }

    @Override
    public boolean rewrite(List<? extends ReferencePeriod> periods) {
        if (decorated.rewrite(periods)) {
            storeEverything();
            return true;
        }
        return false;
    }

    @Override
    public boolean append(@NotNull ReferencePeriod period) {
        if (decorated.append(period)) {
            storeEverything();
            return true;
        }
        return false;
    }

    private void storeEverything() {
        persistence.replaceAll(periods.periods());
    }
}
