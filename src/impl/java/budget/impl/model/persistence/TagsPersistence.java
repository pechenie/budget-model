package budget.impl.model.persistence;

import budget.model.tag.Tag;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;

/**
 * Storage interface for tags
 *
 * @author Kirill Berezin
 */
public interface TagsPersistence {
    /**
     * Saves a tag to underlying storage
     *
     * @param tag tag to save
     */
    void store(@NotNull Tag tag);

    /**
     * Removes a tag by it's key from underlying storage
     *
     * @param tagKey tag key to remove
     */
    void remove(long tagKey);

    /**
     * Reads all stored tags
     *
     * @return stored tags
     */
    @NotNull
    Collection<? extends Tag> readTags();
}
