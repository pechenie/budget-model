package budget.impl.model;

import budget.impl.model.entity.BalanceReportImpl;
import budget.model.BalanceReport;
import budget.model.book.BalanceRecordBook;
import budget.model.source.BalanceReports;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Default balance reports read-write book implementation. Based on a tree map
 *
 * @since 15.04.2017
 */
public final class BalanceReportsImpl implements BalanceReports, BalanceRecordBook {
    private final TreeMap<Long, BalanceReport> balanceReports;

    public BalanceReportsImpl() {
        this.balanceReports = new TreeMap<>();
    }

    public BalanceReportsImpl(@NotNull Collection<? extends BalanceReport> balanceReports) {
        this.balanceReports = new TreeMap<>(balanceReports.stream().collect(
                Collectors.toMap(BalanceReport::time, Function.identity())));
    }

    @NotNull
    @Override
    public BalanceReport lastReportAtTime(long time, boolean inclusive) {
        Map.Entry<Long, BalanceReport> entry = inclusive ? balanceReports.floorEntry(time) : balanceReports.lowerEntry(time);
        return entry == null ? new BalanceReportImpl(Long.MIN_VALUE, 0) : entry.getValue();
    }

    @NotNull
    @Override
    public Collection<BalanceReport> range(long from, boolean inclusiveFrom, long to, boolean inclusiveTo) {
        if (from > to) {
            return Collections.emptySet();
        }
        return balanceReports.subMap(from, inclusiveFrom, to, inclusiveTo).values();
    }

    //BOOK
    @Override
    public boolean hasRecord(long time) {
        return balanceReports.containsKey(time);
    }

    @Override
    public void eraseRecord(long time) {
        balanceReports.remove(time);
    }

    @Override
    public boolean report(@NotNull BalanceReport balanceReport) {
        if (balanceReports.containsKey(balanceReport.time())) {
            return false;
        }
        balanceReports.put(balanceReport.time(), balanceReport);
        return true;
    }
}
