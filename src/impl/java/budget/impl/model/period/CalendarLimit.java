package budget.impl.model.period;

import org.joda.time.LocalDate;

import java.util.function.Predicate;

/**
 * Calendar-based predicate implementation
 *
 * @author Kirill Berezin
 */

public class CalendarLimit implements Predicate<Long> {
    private final LocalDate limitDate;

    public CalendarLimit(LocalDate limitDate) {
        this.limitDate = limitDate;
    }

    @Override
    public boolean test(Long time) {
        return new LocalDate(time).compareTo(limitDate) > 0;
    }
}
