package budget.impl.model.period;

import budget.impl.model.entity.ReferencePeriodImpl;
import budget.impl.model.future.ReferencePeriodsFactory;
import budget.model.ReferencePeriod;
import budget.model.time.TimestampIterator;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

/**
 * Default implementation of a reference period factory
 * @author Kirill Berezin
 */

public class PeriodicReferencePeriodFactory implements ReferencePeriodsFactory {
    private final TimestampIterator timestampIterator;
    private final double usableMargin;
    private final Limiter periodLimiter;

    /**
     * Factory rules are defined based on a timestamp iterator and a period limiter (to stop producing more periods). Usable margin value
     * shall be provided as well
     *
     * @param timestampIterator iterator to produce next periods
     * @param usableMargin      usable margin to assign to periods
     * @param periodLimiter     limiter to stop production
     */
    public PeriodicReferencePeriodFactory(TimestampIterator timestampIterator, double usableMargin, Limiter periodLimiter) {
        this.timestampIterator = timestampIterator;
        this.usableMargin = usableMargin;
        this.periodLimiter = periodLimiter;
    }

    @NotNull
    @Override
    public List<? extends ReferencePeriod> calculateWithStartingTime(long time) {
        Predicate<Long> periodLimit = periodLimiter.limitWithStartTime(time);

        List<ReferencePeriod> periods = new ArrayList<>();
        while (!periodLimit.test(time)) {
            periods.add(new ReferencePeriodImpl(time, time = timestampIterator.next(time), usableMargin));
        }
        return periods;
    }
}
