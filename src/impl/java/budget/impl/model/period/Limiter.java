package budget.impl.model.period;

import java.util.function.Predicate;

/**
 * A rule for creating a limiting predicate with starting time
 * @author Kirill Berezin
 */

public interface Limiter {
    /**
     * Creates a predicate to set a limit on a timeline with the starting time provided
     *
     * @param from time to calculate limit from
     * @return a predicate
     */
    Predicate<Long> limitWithStartTime(long from);
}
