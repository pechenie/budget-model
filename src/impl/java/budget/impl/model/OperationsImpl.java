package budget.impl.model;

import budget.model.Operation;
import budget.model.book.OperationRecordBook;
import budget.model.source.Operations;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Default operations read-write book implementation. Based on a tree map
 *
 * @since 15.04.2017
 */
public final class OperationsImpl implements Operations, OperationRecordBook {
    @NotNull
    private final TreeMap<Long, Operation> operations;

    public OperationsImpl() {
        this.operations = new TreeMap<>();
    }

    public OperationsImpl(@NotNull Operation... operations) {
        this(Arrays.asList(operations));
    }

    public OperationsImpl(@NotNull Collection<? extends Operation> operations) {
        this.operations = new TreeMap<>(operations.stream().collect(Collectors.toMap(Operation::time, Function.identity())));
    }

    @Nullable
    @Override
    public Operation operationAt(long time) {
        return operations.get(time);
    }

    @Override
    public double operationSum(long timeFrom, long timeTo, boolean inclusive) {
        if (timeFrom > timeTo) {
            return 0;
        }
        return operations.subMap(timeFrom, true, timeTo, inclusive).values().stream()
                .reduce(0d, (sum, t) -> sum + t.cost(), (s1, s2) -> s1 + s2);
    }

    @NotNull
    @Override
    public Collection<Operation> range(long from, boolean inclusiveFrom, long to, boolean inclusiveTo) {
        if (from > to) {
            return Collections.emptySet();
        }
        return operations.subMap(from, inclusiveFrom, to, inclusiveTo).values();
    }

    //BOOK
    @Override
    public boolean hasRecord(long time) {
        return operations.containsKey(time);
    }

    @Override
    public void eraseRecord(long time) {
        operations.remove(time);
    }

    @Override
    public boolean execute(@NotNull Operation operation) {
        if (operations.containsKey(operation.time())) {
            return false;
        }
        operations.put(operation.time(), operation);
        return true;
    }
}
