package budget.impl.model;

import org.jetbrains.annotations.NotNull;

/**
 * Records' tags key domain
 * @author Kirill Berezin
 */

public interface RecordTagDomain {
    /**
     * Key for record's custom title
     *
     * @return key for getting title from tag
     */
    @NotNull
    String customTitleKey();
}
