package budget.impl.model.notifying;

import budget.model.BalanceReport;
import budget.model.UpdateListener;
import budget.model.book.BalanceRecordBook;
import org.jetbrains.annotations.NotNull;

/**
 * Notifying balance book decorator
 *
 * @author Kirill Berezin
 */

public class NotifyingBalanceBook implements BalanceRecordBook {
    private final UpdateListener listener;
    private final BalanceRecordBook decorated;

    public NotifyingBalanceBook(@NotNull UpdateListener listener, @NotNull BalanceRecordBook decorated) {
        this.listener = listener;
        this.decorated = decorated;
    }

    @Override
    public boolean report(@NotNull BalanceReport balanceReport) {
        if (decorated.report(balanceReport)) {
            listener.notifyUpdated();
            return true;
        }
        return false;
    }

    @Override
    public boolean hasRecord(long time) {
        return decorated.hasRecord(time);
    }

    @Override
    public void eraseRecord(long time) {
        decorated.eraseRecord(time);
        listener.notifyUpdated();
    }
}
