package budget.impl.model.notifying;

import budget.model.ReferencePeriod;
import budget.model.UpdateListener;
import budget.model.book.PeriodBook;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Notifying period book decorator
 *
 * @author Kirill Berezin
 */

public class NotifyingPeriodBook implements PeriodBook {
    private final UpdateListener listener;
    private final PeriodBook decorated;

    public NotifyingPeriodBook(@NotNull UpdateListener listener, @NotNull PeriodBook decorated) {
        this.listener = listener;
        this.decorated = decorated;
    }

    @Override
    public boolean rewrite(List<? extends ReferencePeriod> periods) {
        if (decorated.rewrite(periods)) {
            listener.notifyUpdated();
            return true;
        }
        return false;
    }

    @Override
    public boolean append(@NotNull ReferencePeriod period) {
        if (decorated.append(period)) {
            listener.notifyUpdated();
            return true;
        }
        return false;
    }
}
