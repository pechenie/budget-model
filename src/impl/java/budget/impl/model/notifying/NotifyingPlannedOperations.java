package budget.impl.model.notifying;

import budget.model.UpdateListener;
import budget.model.operation.PlannedOperationSeries;
import budget.model.operation.PlannedOperations;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;

/**
 * Notifying planned operations decorator
 *
 * @author Kirill Berezin
 */

public class NotifyingPlannedOperations implements PlannedOperations {
    private final UpdateListener listener;
    private final PlannedOperations decorated;

    public NotifyingPlannedOperations(@NotNull UpdateListener listener, @NotNull PlannedOperations decorated) {
        this.listener = listener;
        this.decorated = decorated;
    }

    @NotNull
    @Override
    public Collection<? extends PlannedOperationSeries> plannedSeries() {
        return decorated.plannedSeries();
    }

    @Override
    public boolean plan(@NotNull PlannedOperationSeries series) {
        if (decorated.plan(series)) {
            listener.notifyUpdated();
            return true;
        }
        return false;
    }

    @Override
    public void delete(@NotNull String name) {
        decorated.delete(name);
        listener.notifyUpdated();
    }
}
