package budget.impl.model.notifying;

import budget.model.Operation;
import budget.model.UpdateListener;
import budget.model.book.OperationRecordBook;
import org.jetbrains.annotations.NotNull;

/**
 * Notifying operations book decorator
 *
 * @author Kirill Berezin
 */

public class NotifyingOperationBook implements OperationRecordBook {
    private final UpdateListener listener;
    private final OperationRecordBook decorated;

    public NotifyingOperationBook(@NotNull UpdateListener listener, @NotNull OperationRecordBook decorated) {
        this.listener = listener;
        this.decorated = decorated;
    }

    @Override
    public boolean execute(@NotNull Operation operation) {
        if (decorated.execute(operation)) {
            listener.notifyUpdated();
            return true;
        }
        return false;
    }

    @Override
    public boolean hasRecord(long time) {
        return decorated.hasRecord(time);
    }

    @Override
    public void eraseRecord(long time) {
        decorated.eraseRecord(time);
        listener.notifyUpdated();
    }
}
